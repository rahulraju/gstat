define(['knockout'], function (ko) {
  function model (context) {      
    var self = this;
    var icons = {'area': 'demo-csc-area-',
                 'bar': 'demo-csc-bar-',
                 'combo': 'demo-csc-bar-',
                 'lineWithArea': 'demo-csc-area-'};
    var type = ko.observable();
    self.unstackedIcon = ko.observable();
    self.stackedIcon = ko.observable();

    type.subscribe(function (value) {
      var key = icons[value] ? icons[value] : icons['bar'];
      self.unstackedIcon('oj-icon ' + key + 'unstack');
      self.stackedIcon('oj-icon ' + key + 'stack');
    });

    var element = context.element;
    element.addEventListener('typeChanged', function(event) {
      type(event.detail.value);
    });

    context.props.then(function(properties) {
      type(properties['type']);
    });
  }
  return model;
})
