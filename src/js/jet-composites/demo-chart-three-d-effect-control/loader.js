define(['ojs/ojcore', 'text!./view.html', 'text!./component.json', 'css!./styles', 'ojs/ojcomposite', 'ojs/ojbutton', 'ojs/ojknockout'],
  function(oj, view, metadata) {
    oj.Composite.register('demo-chart-three-d-effect-control', {
      view: view, 
      metadata: JSON.parse(metadata)
    });
  }
);