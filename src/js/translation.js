require(['ojs/ojcore', 'knockout', 'jquery'],
    function(oj, ko) {


        // document.addEventListener('deviceready', function() {
        //     // Enable to debug issues.
        //     // window.plugins.OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});
        //     cordova.InAppBrowser.open('http://192.168.155.106:9502/analytics/saw.dll?PortalPages&NQUser=weblogic&NQPassword=welcome1', '_blank', 'hidden=yes');


        //     const push = PushNotification.init({
        //         android: {
        //             senderID: 658388970477
        //         },
        //         browser: {
        //             pushServiceURL: 'http://push.api.phonegap.com/v1/push'
        //         },
        //         ios: {
        //             alert: "true",
        //             badge: "true",
        //             sound: "true"
        //         },
        //         windows: {}
        //     });

        //     push.on('registration', (data) => {
        //         //alert(data.registrationId);
        //     });

        //     push.on('notification', (data) => {
        //         alert(data.message);
        //         //alert('notificationOpenedCallback: ' + JSON.stringify(data));
        //         // data.title,
        //         // data.count,
        //         // data.sound,
        //         // data.image,
        //         // data.additionalData
        //     });

        //     push.on('error', (e) => {
        //         alert('error: ' + e.message);
        //     });

        //     // Call syncHashedEmail anywhere in your app if you have the user's email.
        //     // This improves the effectiveness of OneSignal's "best-time" notification scheduling feature.
        //     // window.plugins.OneSignal.syncHashedEmail(userEmail);
        // }, false);

        this.profilePic = ko.observable('css/images/ahmed.jpg');
        this.firstName = ko.observable('Ahmed');
        this.lastName = ko.observable('El Ashqar');
        this.email = ko.observable('ahmed.ashqar@oracle.com');
        this.parameter = ko.observable();
        this.filtrationValues = ko.observable();
        this.metadata = ko.observable();

        this.text = ko.observable();
        
         this.getIndicatorText =  function(code) {  
            return  indicators[code];
        };
       this.indicators ={
            
        };
        
         this.getDynRptName = function(){
           return ko.observable(localStorage.getItem('isEnglish') === "true"? sessionStorage.getItem('SA_EN') :sessionStorage.getItem('SA_AR'));
//            return oj.Translations.getTranslatedString('dynamicReport');
        };

        function loadMenu() {
            this.text({
                langtxt: oj.Translations.getTranslatedString('lang'),
                themetxt: oj.Translations.getTranslatedString('theme'),
                fontSizetxt: oj.Translations.getTranslatedString('fontSize'),
                lang: ko.observable(localStorage.getItem('isEnglish') === "true" ?true:false),
                login: oj.Translations.getTranslatedString('login'),
                about: oj.Translations.getTranslatedString('about'),
                mainmenu: oj.Translations.getTranslatedString('homePage'),
                langSwitch: oj.Translations.getTranslatedString('langSwitch'),
                indicators: oj.Translations.getTranslatedString('indicators'),
                population: oj.Translations.getTranslatedString('population'),
                news: oj.Translations.getTranslatedString('news'),
                'stat-map': oj.Translations.getTranslatedString('map'),
                eservice: oj.Translations.getTranslatedString('eservice'),
                library: oj.Translations.getTranslatedString('library'),
                'build-report': oj.Translations.getTranslatedString('report'),
                dashboard: oj.Translations.getTranslatedString('dashboard'),
                gastat: oj.Translations.getTranslatedString('gastat'),
                editProfile: oj.Translations.getTranslatedString('editProfile'),
                settings: oj.Translations.getTranslatedString('settings'),
                rating: oj.Translations.getTranslatedString('rating'),
                signin: oj.Translations.getTranslatedString('signin'),
                dynamicReport: this.getDynRptName(),
                biDashboard: oj.Translations.getTranslatedString('biDashboard'),
                noresult: oj.Translations.getTranslatedString('noresult'),
                avgprices: oj.Translations.getTranslatedString('avgprices'),
                exports: oj.Translations.getTranslatedString('exports'),
                imports: oj.Translations.getTranslatedString('imports'),
                tradeExchange: oj.Translations.getTranslatedString('tradeExchange'),
                exportsBySection: oj.Translations.getTranslatedString('exportsBySection'),
                materialExports: oj.Translations.getTranslatedString('materialExports'),
                materialImports: oj.Translations.getTranslatedString('materialImports'),
                aboutGastatText: oj.Translations.getTranslatedString('aboutGastatText'),
                aboutGastatTextLabel: oj.Translations.getTranslatedString('aboutGastatTextLabel'),
                aboutGastatContact: oj.Translations.getTranslatedString('aboutGastatContact'),
                aboutGastatContactLabel: oj.Translations.getTranslatedString('aboutGastatContactLabel'),
                aboutGastatLocation: oj.Translations.getTranslatedString('aboutGastatLocation'),
                aboutGastatLocationLabel: oj.Translations.getTranslatedString('aboutGastatLocationLabel'),
                infographics: oj.Translations.getTranslatedString('infographics'),
                'indicator-pro': oj.Translations.getTranslatedString('dynamicReport')
            });
        };
        loadMenu();
        



        this.model = {

            langtxt: ko.observable(oj.Translations.getTranslatedString('lang')),
            themetxt: ko.observable(oj.Translations.getTranslatedString('theme')),
            fontSizetxt: ko.observable(oj.Translations.getTranslatedString('fontSize')),
            lang: ko.observable(localStorage.getItem('isEnglish') === "true" ?true:false),
            login: ko.observable(oj.Translations.getTranslatedString('login')),
            about: ko.observable(oj.Translations.getTranslatedString('about')),
            mainmenu: ko.observable(oj.Translations.getTranslatedString('homePage')),
            langSwitch: ko.observable(oj.Translations.getTranslatedString('langSwitch')),
            indicators: ko.observable(oj.Translations.getTranslatedString('indicators')),
            population: ko.observable(oj.Translations.getTranslatedString('population')),
            news: ko.observable(oj.Translations.getTranslatedString('news')),
            map: ko.observable(oj.Translations.getTranslatedString('map')),
            portal: ko.observable(oj.Translations.getTranslatedString('portal')),
            masdarSite: ko.observable(oj.Translations.getTranslatedString('masdarSite')),
            eservice: ko.observable(oj.Translations.getTranslatedString('eservice')),
            library: ko.observable(oj.Translations.getTranslatedString('library')),
            report: ko.observable(oj.Translations.getTranslatedString('report')),
            dashboard: ko.observable(oj.Translations.getTranslatedString('dashboard')),
            gastat: ko.observable(oj.Translations.getTranslatedString('gastat')),
            editProfile: ko.observable(oj.Translations.getTranslatedString('editProfile')),
            settings: ko.observable(oj.Translations.getTranslatedString('settings')),
            rating: ko.observable(oj.Translations.getTranslatedString('rating')),

            submit: ko.observable(oj.Translations.getTranslatedString('submit')),
            ratingLabel: ko.observable(oj.Translations.getTranslatedString('ratingLabel')),
            feedback: ko.observable(oj.Translations.getTranslatedString('feedback')),
            size: ko.observable(oj.Translations.getTranslatedString('size')),
            example: ko.observable(oj.Translations.getTranslatedString('example')),

            signin: ko.observable(oj.Translations.getTranslatedString('signin')),
            dynamicReport: ko.observable(oj.Translations.getTranslatedString('dynamicReport')),
            biDashboard: ko.observable(oj.Translations.getTranslatedString('biDashboard')),
            noresult: ko.observable(oj.Translations.getTranslatedString('noresult')),
            year: ko.observable(oj.Translations.getTranslatedString('year')),
            type: ko.observable(oj.Translations.getTranslatedString('type')),
            back: ko.observable(oj.Translations.getTranslatedString('back')),
            apply: ko.observable(oj.Translations.getTranslatedString('apply')),
            next: ko.observable(oj.Translations.getTranslatedString('next')),
            more: ko.observable(oj.Translations.getTranslatedString('more')),
            searchKeyword: ko.observable(oj.Translations.getTranslatedString('searchKeyword')),

            chart: ko.observable(oj.Translations.getTranslatedString('chart')),
            pie: ko.observable(oj.Translations.getTranslatedString('pie')),
            bar: ko.observable(oj.Translations.getTranslatedString('bar')),
            pyramid: ko.observable(oj.Translations.getTranslatedString('pyramid')),

            sbujectArea: ko.observable(oj.Translations.getTranslatedString('sbujectArea')),
            indicator: ko.observable(oj.Translations.getTranslatedString('indicator')),
            dimensions: ko.observable(oj.Translations.getTranslatedString('dimensions')),
            group: ko.observable(oj.Translations.getTranslatedString('group')),
            value: ko.observable(oj.Translations.getTranslatedString('value')),
            filtration: ko.observable(oj.Translations.getTranslatedString('filtration')),
            stepLabel: ko.observable(oj.Translations.getTranslatedString('stepLabel')),
            newsLabel: ko.observable(oj.Translations.getTranslatedString('newsLabel')),
            totalPopulation: ko.observable(oj.Translations.getTranslatedString('totalPopulation')),
            infographics: ko.observable(oj.Translations.getTranslatedString('infographics')),
            commercialRegister: ko.observable(oj.Translations.getTranslatedString('commercialRegister')),
            establishments: ko.observable(oj.Translations.getTranslatedString('establishments')),
            foreignTrade: ko.observable(oj.Translations.getTranslatedString('foreignTrade')),
            avgprices: ko.observable(oj.Translations.getTranslatedString('avgprices')),
            economicEstablishments: ko.observable(oj.Translations.getTranslatedString('economicEstablishments')),
            activeCommerce: ko.observable(oj.Translations.getTranslatedString('activeCommerce')),
            operatingExpenditure: ko.observable(oj.Translations.getTranslatedString('operatingExpenditure')),
            transactionCommercialRegisters: ko.observable(oj.Translations.getTranslatedString('transactionCommercialRegisters')),
            exports: ko.observable(oj.Translations.getTranslatedString('exports')),
            imports: ko.observable(oj.Translations.getTranslatedString('imports')),
            tradeExchange: ko.observable(oj.Translations.getTranslatedString('tradeExchange')),
            exportsBySection: ko.observable(oj.Translations.getTranslatedString('exportsBySection')),
            materialExports: ko.observable(oj.Translations.getTranslatedString('materialExports')),
            materialImports: ko.observable(oj.Translations.getTranslatedString('materialImports')),
            aboutGastatText: ko.observable(oj.Translations.getTranslatedString('aboutGastatText')),
            aboutGastatTextLabel: ko.observable(oj.Translations.getTranslatedString('aboutGastatTextLabel')),
            aboutGastatContact: ko.observable(oj.Translations.getTranslatedString('aboutGastatContact')),
            aboutGastatContactLabel: ko.observable(oj.Translations.getTranslatedString('aboutGastatContactLabel')),
            aboutGastatLocation: ko.observable(oj.Translations.getTranslatedString('aboutGastatLocation')),
            aboutGastatLocationLabel: ko.observable(oj.Translations.getTranslatedString('aboutGastatLocationLabel')),
            aboutMasdar: ko.observable(oj.Translations.getTranslatedString('aboutMasdar')),
            aboutMasdarLabel: ko.observable(oj.Translations.getTranslatedString('aboutMasdarLabel')),
            yrs2 : ko.observable(oj.Translations.getTranslatedString('2Yrs')),
            yrs3 : ko.observable(oj.Translations.getTranslatedString('3Yrs')),
            yrs5 : ko.observable(oj.Translations.getTranslatedString('5Yrs')),
            
            compare : ko.observable(oj.Translations.getTranslatedString('compare')),            
            guide : ko.observable(oj.Translations.getTranslatedString('guide')),            
            buildReport : ko.observable(oj.Translations.getTranslatedString('buildReport')),            
            favourite : ko.observable(oj.Translations.getTranslatedString('favourite')),
            reportName : ko.observable(oj.Translations.getTranslatedString('reportName')),
            
             saudi : ko.observable(oj.Translations.getTranslatedString('saudi')),         
             world : ko.observable(oj.Translations.getTranslatedString('world')),           
             indiView : ko.observable(oj.Translations.getTranslatedString('indiView')),            
             yearView : ko.observable(oj.Translations.getTranslatedString('yearView'))
 
            
        };

        this.text.resolve = function(key) {
            return oj.Translations.getTranslatedString(key);
        };



        this.langChange = function() {
            var oldLang =localStorage.getItem('isEnglish');
            localStorage.setItem('isEnglish',this.model.lang());
            if(oldLang !== (this.model.lang()+"")){
                if(window.location.href.includes('indicator-pro') || window.location.href.includes('population') || window.location.href.includes('dynamicReport')|| window.location.href.includes('infographics')|| window.location.href.includes('rating')|| window.location.href.includes('stat-map'))
            location.reload();
            else
            this.loadLangText();
    };
            //console.log("data: " + data);
           

//             newLang = 'en-US';
            //console.log("newLang: " + newLang);
           

        };
        
        self.loadLangText = function(){
            var newLang = localStorage.getItem('isEnglish') === "true" ?'en-US':'ar-EG';
//             switch (isEnglish) {
//                case 'false':
//                    newLang = 'ar-EG';
//                    break;
//                default:
//                    newLang = 'en-US';
//            }            
             oj.Config.setLocale(newLang,
                function() {
                    $('html').attr('lang', newLang);
                    if (newLang === 'ar-EG') {
                        $('html').attr('dir', 'rtl');
                        $('body').addClass('ar');
                    } else {
                        $('html').attr('dir', 'ltr');
                        $('body').removeClass('ar');
                    }

                    loadMenu();
                    //                            this.model.langtxt = ko.observable(oj.Translations.getTranslatedString('lang'));
                    //                            this.model.themetxt = ko.observable(oj.Translations.getTranslatedString('theme'));
                    //                            this.model.fontSizetxt = ko.observable(oj.Translations.getTranslatedString('fontSize'));
                    this.model.langtxt(oj.Translations.getTranslatedString('lang'));
                    this.model.themetxt(oj.Translations.getTranslatedString('theme'));
                    this.model.fontSizetxt(oj.Translations.getTranslatedString('fontSize'));
                    this.model.login(oj.Translations.getTranslatedString('login'));
                    this.model.about(oj.Translations.getTranslatedString('about'));
                    this.model.mainmenu(oj.Translations.getTranslatedString('homePage'));
                    this.model.langSwitch(oj.Translations.getTranslatedString('langSwitch'));
                    this.model.indicators(oj.Translations.getTranslatedString('indicators'));
                    this.model.population(oj.Translations.getTranslatedString('population'));
                    this.model.news(oj.Translations.getTranslatedString('news'));
                    this.model.map(oj.Translations.getTranslatedString('map'));
                    this.model.portal(oj.Translations.getTranslatedString('portal'));
                    this.model.masdarSite(oj.Translations.getTranslatedString('masdarSite'));
                    this.model.eservice(oj.Translations.getTranslatedString('eservice'));
                    this.model.library(oj.Translations.getTranslatedString('library'));
                    this.model.report(oj.Translations.getTranslatedString('report'));
                    this.model.dashboard(oj.Translations.getTranslatedString('dashboard'));
                    this.model.gastat(oj.Translations.getTranslatedString('gastat'));
                    this.model.editProfile(oj.Translations.getTranslatedString('editProfile'));
                    this.model.settings(oj.Translations.getTranslatedString('settings'));
                    this.model.rating(oj.Translations.getTranslatedString('rating'));
                    this.model.submit(oj.Translations.getTranslatedString('submit'));
                    this.model.ratingLabel(oj.Translations.getTranslatedString('ratingLabel'));
                    this.model.feedback(oj.Translations.getTranslatedString('feedback'));
                    this.model.infographics(oj.Translations.getTranslatedString('infographics'));
                    this.model.size(oj.Translations.getTranslatedString('size'));
                    this.model.example(oj.Translations.getTranslatedString('example'));
                    this.model.signin(oj.Translations.getTranslatedString('signin'));
                    this.model.dynamicReport(oj.Translations.getTranslatedString('dynamicReport'));
                    this.model.biDashboard(oj.Translations.getTranslatedString('biDashboard'));
                    this.model.noresult(oj.Translations.getTranslatedString('noresult'));
                    this.model.year(oj.Translations.getTranslatedString('year'));
                    this.model.type(oj.Translations.getTranslatedString('type'));
                    this.model.back(oj.Translations.getTranslatedString('back'));
                    this.model.apply(oj.Translations.getTranslatedString('apply'));
                    this.model.next(oj.Translations.getTranslatedString('next'));
                    this.model.more(oj.Translations.getTranslatedString('more'));
                    this.model.searchKeyword(oj.Translations.getTranslatedString('searchKeyword'));
                    this.model.chart(oj.Translations.getTranslatedString('chart'));
                    this.model.pie(oj.Translations.getTranslatedString('pie'));
                    this.model.bar(oj.Translations.getTranslatedString('bar'));
                    this.model.pyramid(oj.Translations.getTranslatedString('pyramid'));


                    this.model.totalPopulation(oj.Translations.getTranslatedString('totalPopulation'));

                    this.model.sbujectArea(oj.Translations.getTranslatedString('sbujectArea'));
                    this.model.indicator(oj.Translations.getTranslatedString('indicator'));
                    this.model.dimensions(oj.Translations.getTranslatedString('dimensions'));
                    this.model.group(oj.Translations.getTranslatedString('group'));
                    this.model.value(oj.Translations.getTranslatedString('value'));
                    this.model.filtration(oj.Translations.getTranslatedString('filtration'));
                    this.model.stepLabel(oj.Translations.getTranslatedString('stepLabel'));
                    this.model.newsLabel(oj.Translations.getTranslatedString('newsLabel'));

                    this.model.commercialRegister(oj.Translations.getTranslatedString('commercialRegister'));
                    this.model.establishments(oj.Translations.getTranslatedString('establishments'));
                    this.model.foreignTrade(oj.Translations.getTranslatedString('foreignTrade'));
                    this.model.economicEstablishments(oj.Translations.getTranslatedString('economicEstablishments'));
                    this.model.activeCommerce(oj.Translations.getTranslatedString('activeCommerce'));
                    this.model.operatingExpenditure(oj.Translations.getTranslatedString('operatingExpenditure'));
                    this.model.transactionCommercialRegisters(oj.Translations.getTranslatedString('transactionCommercialRegisters'));
                    this.model.avgprices(oj.Translations.getTranslatedString('avgprices'));
                    this.model.exports(oj.Translations.getTranslatedString('exports'));
                    this.model.imports(oj.Translations.getTranslatedString('imports'));
                    this.model.tradeExchange(oj.Translations.getTranslatedString('tradeExchange'));
                    this.model.exportsBySection(oj.Translations.getTranslatedString('exportsBySection'));
                    this.model.materialExports(oj.Translations.getTranslatedString('materialExports'));
                    this.model.materialImports(oj.Translations.getTranslatedString('materialImports'));
                    this.model.aboutGastatText(oj.Translations.getTranslatedString('aboutGastatText'));
                    this.model.aboutGastatTextLabel(oj.Translations.getTranslatedString('aboutGastatTextLabel'));
                    this.model.aboutGastatContact(oj.Translations.getTranslatedString('aboutGastatContact'));
                    this.model.aboutGastatContactLabel(oj.Translations.getTranslatedString('aboutGastatContactLabel'));
                    this.model.aboutGastatLocation(oj.Translations.getTranslatedString('aboutGastatLocation'));
                    this.model.aboutGastatLocationLabel(oj.Translations.getTranslatedString('aboutGastatLocationLabel'));
                    this.model.aboutMasdar(oj.Translations.getTranslatedString('aboutMasdar'));
                    this.model.aboutMasdarLabel(oj.Translations.getTranslatedString('aboutMasdarLabel'));
                    
                    this.model.yrs2(oj.Translations.getTranslatedString('2Yrs'));
                     this.model.yrs3(oj.Translations.getTranslatedString('3Yrs'));
                     this.model.yrs5(oj.Translations.getTranslatedString('5Yrs'));
                     
                     this.model.compare(oj.Translations.getTranslatedString('compare'));          
            this.model.guide(oj.Translations.getTranslatedString('guide'));            
            this.model.buildReport(oj.Translations.getTranslatedString('buildReport'));            
            this.model.favourite(oj.Translations.getTranslatedString('favourite'));
            this.model.reportName(oj.Translations.getTranslatedString('reportName'));
            


            this.model.saudi(oj.Translations.getTranslatedString('saudi'));          
            this.model.world(oj.Translations.getTranslatedString('world'));            
            this.model.indiView(oj.Translations.getTranslatedString('indiView'));            
            this.model.yearView(oj.Translations.getTranslatedString('yearView'));

            
                }
            );
            
        };
        this.loadLangText();

        //ko.applyBindings(model,document.getElementById("sampleDemo"));


    });