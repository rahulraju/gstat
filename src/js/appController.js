/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your application specific code will go here
 */
define(['ojs/ojcore', 'knockout', 'ojs/ojrouter', 'ojs/ojknockout', 'ojs/ojarraytabledatasource',
        'ojs/ojoffcanvas', 'translation', 'ojs/ojmodule', 'ojs/ojmoduleanimations'
    ],
    function(oj, ko) {
        function ControllerViewModel() {
            var self = this;

            // Media queries for repsonsive layouts
            var smQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.SM_ONLY);
            self.smScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(smQuery);
            var mdQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.MD_UP);
            self.mdScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(mdQuery);

            // Router setup
            self.router = oj.Router.rootInstance;
            self.router.configure({

                'tour': { label: 'Tour'},
                'tourContent': { label: 'Tour Content' },
                'tourLaunchPage': { label: 'Tour Launch Page' },
                'dashboard': { label: 'Dashboard' , isDefault: true },
                'biDashboard': { label: 'Chart' },
                'tradeExchange': { label: 'Trade Exchange' },
                'exports': { label: 'Exports' },
                'incidents': { label: 'Login' },
                'exportBySections': { label: 'Export By Sections' },
                'dynamicReport': { label: 'Dynamic Report' },
                'settings': { label: 'Settings' },
                'editProfile': { label: 'Edit Profile' },
                'langPage': { label: 'langPage' },
                'rating': { label: 'rating' },
                'about': { label: 'About' },
                'indicator-pro': { label: 'indicator-pro' },
                'indicators': { label: 'Indicators' },
                'news': { label: 'News' },
                'population': { label: 'Population' },
                'library': { label: 'Statistical Library' },
                'stat-map': { label: 'Statistical Map' },
                'build-report': { label: 'Build your report' },
                'infographics': { label: 'Infographics', value: 'infographics' }
            });
            oj.Router.defaults['urlAdapter'] = new oj.Router.urlParamAdapter();
            //langPage
            // Navigation setup
            var navData = [{
                    name: model.mainmenu,
                    id: 'dashboard',
                    iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-chart-icon-24'
                },
                {
                    name: model.login,
                    id: 'incidents',
                    iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-people-icon-24'
                },
                {
                    name: 'Chart',
                    id: 'biDashboard',
                    iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-fire-icon-24'
                },
                {
                    name: 'Edit Profile',
                    id: 'editProfile',
                    iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-info-icon-24'
                },
                {
                    name: 'Settings',
                    id: 'settings',
                    iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-gear-icon-16'
                },
                {
                    name: model.about,
                    id: 'about',
                    iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-info-icon-24'
                }



            ];
            self.navDataSource = new oj.ArrayTableDataSource(navData, { idAttribute: 'id' });

            // Drawer
            // Close offcanvas on medium and larger screens
            self.mdScreen.subscribe(function() {
                oj.OffcanvasUtils.close(self.drawerParams);
            });
            self.drawerParams = {
                displayMode: 'push',
                selector: '#navDrawer',
                content: '#pageContent'
            };
            // Called by navigation drawer toggle button and after selection of nav drawer item
            self.toggleDrawer = function() {
                    return oj.OffcanvasUtils.toggle(self.drawerParams);
                };
                // Add a close listener so we can move focus back to the toggle button when the drawer closes
            $("#navDrawer").on("ojclose", function() {
                $('#drawerToggleButton').focus();
            });


            self.goToDashboard = function() {
                self.router.go('dashboard');
            };



            // Header
            // Application Name used in Branding Area
            self.appName = ko.observable("Masdar");
            // User Info used in Global Navigation area
            self.userLogin = ko.observable("ahmed.al.ashqar@oracle.com");

            self.homeRouting = function() {
                oj.Router.rootInstance.go('dashboard');
            };

            // Footer
            function footerLink(name, id, linkTarget) {
                this.name = name;
                this.linkId = id;
                this.linkTarget = linkTarget;
            }
            self.footerLinks = ko.observableArray([
                new footerLink('About Oracle', 'aboutOracle', 'http://www.oracle.com/us/corporate/index.html#menu-about'),
                new footerLink('Contact Us', 'contactUs', 'http://www.oracle.com/us/corporate/contact/index.html'),
                new footerLink('Legal Notices', 'legalNotices', 'http://www.oracle.com/us/legal/index.html'),
                new footerLink('Terms Of Use', 'termsOfUse', 'http://www.oracle.com/us/legal/terms/index.html'),
                new footerLink('Your Privacy Rights', 'yourPrivacyRights', 'http://www.oracle.com/us/legal/privacy/index.html')
            ]);
        }

        return new ControllerViewModel();
    }
);