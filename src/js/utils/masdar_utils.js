/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *  The function used to hide and show the html elements for print operation.
 *  clsNms = css style className of the html elements comma seperated , which needs to be printed
 */

define(['knockout', 'ojs/ojcore', 'jquery', 'promise'],
        function (ko, oj, $) {
            var self = this;
            var env = 'TEST_ENV';

            self.sitesUrl = '';
            self.bipUrl = '';
            self.osbUrl = '';
            self.bipAuth='';
            self.osbAuth='';

            self.indLabel = {};

            self.loadEnv = function (env) {
                var envJson = {
                    "DEV_ENV": {
                        "SITE_URL": "http://192.168.172.15:7777/sites/Masdar/---/HomePage",
                        "BIP_URL": "http://192.168.172.4:9502/xmlpserver/services/rest/v1/reports/Mobile%2FDataModel%2F",
                        "OSB_URL": "http://192.168.172.7:7005/",
                        "BIP_AUTH": "Basic c3JpbmF0aC5wYXNoYW06Z29saXZlKSk=",
                        "OSB_AUTH": "Basic d2VibG9naWM6d2VsY29tZTE="
                    },
                    "TEST_ENV": {
                        "SITE_URL": "http://192.168.170.15:7777/sites/Masdar/---/HomePage",
                        "BIP_URL": "http://192.168.170.4:9502/xmlpserver/services/rest/v1/reports/Mobile%2FDataModel%2F",
                        "OSB_URL": "http://192.168.170.7:7005/",
                        "BIP_AUTH": "Basic c3JpbmF0aC5wYXNoYW06Z29saXZlKSk=",
                        "OSB_AUTH": "Basic d2VibG9naWM6d2VsY29tZTE="
                    },
                    "STAGE_ENV": {
                        "SITE_URL": "http://192.168.172.14:7005/sites/Masdar/---/HomePage",
                        "BIP_URL": "http://192.168.172.4:9502/xmlpserver/services/rest/v1/reports/Mobile%2FDataModel%2F",
                        "OSB_URL": "http://192.168.172.7:7005/",
                        "BIP_AUTH": "Basic c3JpbmF0aC5wYXNoYW06Z29saXZlKSk=",
                        "OSB_AUTH": "Basic d2VibG9naWM6d2VsY29tZTE="
                    },
                    "PROD_ENV": {
                        "SITE_URL": "http://192.168.172.14:7005/sites/Masdar/---/HomePage",
                        "BIP_URL": "http://192.168.172.4:9502/xmlpserver/services/rest/v1/reports/Mobile%2FDataModel%2F",
                        "OSB_URL": "http://192.168.172.7:7005/",
                        "BIP_AUTH": "Basic c3JpbmF0aC5wYXNoYW06Z29saXZlKSk=",
                        "OSB_AUTH": "Basic d2VibG9naWM6d2VsY29tZTE="
                    }
                };
                var envDetails = envJson[env];
                self.sitesUrl = envDetails.SITE_URL;
                self.bipUrl = envDetails.BIP_URL;
                self.osbUrl = envDetails.OSB_URL;

            };
            self.loadEnv(env);

            self.getContextUrl = function (url) {
                var contextUrl = url ? url : self.sitesUrl;
                if (contextUrl && contextUrl.includes('---')) {
                    if (model.lang())
                        contextUrl = contextUrl.replace('---', 'EN');
                    else
                        contextUrl = contextUrl.replace('---', 'AR');
                }
                return contextUrl;
            };

            self.getMasdarSites = function () {
                return self.getContextUrl();
            };

            self.doOfflineRestCall = function (jsonUrl, jasonObj, fnSuccess) {
                var result = localStorage.getItem(jasonObj);
                if (result)
                    fnSuccess(JSON.parse(result));
                else
                    alert('Network issue or service currently unavaiable');

            };

            self.doRestGet = function (jsonUrl, fnSuccess) {

            };


            self.getGraphData = function (resultArray, graph_type) {
//                if(!val)
//                val = getAjaxResp('http://localhost:8383/hybrid/css/results.css');
//                var resultArray = val.split("\n");
                var returnResult = {groupVal: [], seriesVal: []};
                var arrayJson = [];

                for (i = 0; i < resultArray.length; i++) {
                    var yearfound = 0;
                    for (j = 0; j < arrayJson.length; j++) {
                        if (arrayJson[j].year === resultArray[i].year) {
                            arrayJson[j].group.push(resultArray[i].group);
                            arrayJson[j].value.push(resultArray[i].value);
                            yearfound++;
                        }

                    }
                    if (yearfound < 1) {
//                         returnResult.groupVal.push(colVals[0]);
//                         returnResult.seriesVal.push({ name: colVals[1], items: [colVals[3]] });
                        arrayJson.push({year: resultArray[i].year,
                            group: [resultArray[i].group], value: [resultArray[i].value]});
                    }
                }





//                for (i = 1; i < resultArray.length; i++) {
//                    var colVals = resultArray[i].split(",");
//                    var yearfound = 0;
//                    for (j = 0; j < arrayJson.length; j++) {
//                        if (arrayJson[j].year === colVals[0]) {
//                            arrayJson[j].group.push(colVals[1]);
//                            arrayJson[j].value.push(colVals[3]);
//                            yearfound++;
//                        }
//
//                    }
//                    if (yearfound < 1 && colVals.length >1){
////                         returnResult.groupVal.push(colVals[0]);
////                         returnResult.seriesVal.push({ name: colVals[1], items: [colVals[3]] });
//                        arrayJson.push({year: colVals[0],
//                            group: [colVals[1]], value: [colVals[3]]});
//                    }
//                }








                for (k = 0; k < arrayJson.length; k++) {

                    returnResult.groupVal.push(arrayJson[k].year);
//                        returnResult.seriesVal.push
                    if (k === 0) {
                        for (h = 0; arrayJson[k].group.length > h; h++)
                            returnResult.seriesVal.push({name: arrayJson[k].group[h], items: [arrayJson[k].value[h]]});
                    } else {
                        for (h = 0; arrayJson[k].group.length > h; h++)
                            returnResult.seriesVal[h].items.push(arrayJson[k].value[h]);

                    }

                }
                return returnResult;

            };




            self.getBarGraph = function (resultArray, group_col) {
                var returnResult = {groupVal: [], seriesVal: []};
                var arrayJson = [];
                var groupName = group_col ? group_col : 'group';

                for (i = 0; i < resultArray.length; i++) {
                    var yearfound = 0;
                    for (j = 0; j < arrayJson.length; j++) {
                        if (arrayJson[j].YEAR === resultArray[i].YEAR) {

                            if (arrayJson[j].group.includes(resultArray[i][groupName])) {
                                arrayJson[j].VALUE[arrayJson[j].group.indexOf(resultArray[i][groupName])] =
                                        arrayJson[j].VALUE[arrayJson[j].group.indexOf(resultArray[i][groupName])] + Number(resultArray[i].VALUE);
                            } else {
                                arrayJson[j].group.push(resultArray[i][groupName]);
                                arrayJson[j].VALUE.push(Number(resultArray[i].VALUE));
                            }
                            yearfound++;
                        }

                    }
                    if (yearfound < 1) {
                        arrayJson.push({YEAR: resultArray[i].YEAR,
                            group: [resultArray[i][groupName]], VALUE: [Number(resultArray[i].VALUE)]});
                    }
                }





                for (k = 0; k < arrayJson.length; k++) {

                    returnResult.groupVal.push(arrayJson[k].YEAR);
                    if (k === 0) {
                        for (h = 0; arrayJson[k].group.length > h; h++)
                            returnResult.seriesVal.push({name: arrayJson[k].group[h], items: [arrayJson[k].VALUE[h]]});
                    } else {
                        for (h = 0; arrayJson[k].group.length > h; h++)
                            returnResult.seriesVal[h].items.push(arrayJson[k].VALUE[h]);

                    }

                }
                return returnResult;

            };


            self.groupData = function (pArray, groupType) {

                var rArray = [];




                for (i = 0; i < pArray.length; i++) {
                    var g = parseInt(pArray[i].group);
                    if (groupType === 'Q')
                        g = g < 4 ? 'Q1' : g < 7 ? 'Q2' : g < 10 ? 'Q3' : 'Q4';
                    else if (groupType === 'H')
                        g = g < 7 ? 'H1' : 'H2';
                    else if (groupType === 'Y')
                        g = g < 13 ? 'Annual Report' : pArray[i].year;
                    var item = {year: pArray[i].year, group: g, indicator: pArray[i].indicator, uom: pArray[i].uom, value: pArray[i].value};
                    rArray.push(item);
                }

                for (i = 0; i < rArray.length - 1; ) {
                    if (rArray[i].year === rArray[i + 1].year && rArray[i].group === rArray[i + 1].group)
                    {
                        rArray[i].value = rArray[i].value + rArray[i + 1].value;
                        rArray.splice(i + 1, 1);
                    } else
                    {
                        i++;
                    }
                }
                return rArray;



            };



            self.convertToJson = function (val, id) {
//                getGraphData(val);
                var resultArray = val.split("\n");
                var arrayJson = [];
                for (i = 9; i < resultArray.length - 3; i++) {
                    var colVals = (resultArray[i].replace('"', '')).split(",");
                    if (id)
                        arrayJson.push({
                            id: i,
                            year: colVals[0],
                            group: colVals[1],
                            indicator: colVals[2],
                            uom: colVals[3],
                            value: Number(colVals[4])
                        });
                    else
                        arrayJson.push({
                            year: colVals[0],
                            group: colVals[1],
                            indicator: colVals[2],
                            uom: colVals[3],
                            value: Number(colVals[4])
                        });

                }
//                alert(arrayJson);
                return arrayJson;

            };


            self.getKeyIndicatorJson = function (val) {
                if (!val)
                    val = localStorage.getItem("KEY_INDICATORS");
//              if(!val)
//                  val=getAjaxResp('/hybrid/js/properties/Indicators_Menu');
                localStorage.setItem("KEY_INDICATORS", val);
                return self.getIndicatorJson(val);
            };

            self.getPopIndicatorJson = function (val) {
                if (!val)
                    val = localStorage.getItem("POPULATION_INDICATORS");
//              if(!val)
//                  val=getAjaxResp('/hybrid/js/properties/Indicators_Menu');
                localStorage.setItem("POPULATION_INDICATORS", val);
                return self.getIndicatorJson(val);
            };


            self.getIndicatorJson = function (val) {
                var resultArray = val.split("\n");
                var arrayJson = [];


                for (i = 10; i < resultArray.length - 3; i++) {
                    var colVals = (resultArray[i].replace(/"/g, '')).split(",");

                    var item = {
                        in_code: colVals[1],
                        css: ko.observable(colVals[2]),
                        url: colVals[6],
                        name: ko.observable(model.lang() ? colVals[3] : colVals[4]),
                        meta: ko.observable(model.lang() ? colVals[7] : colVals[8]),
                        sa: colVals[10],
                        seq_no: colVals[5],
                        name_eng: colVals[3],
                        name_arb: colVals[4],
                        meta_eng: colVals[7],
                        meta_arb: colVals[8],
                        indicators: []
                    };

                    if (colVals.length > 10) {
                        item['group_code'] = colVals[11];
                        item['group_eng'] = colVals[12];
                        item['group_arb'] = colVals[13];
                    }

                    if (arrayJson.length < 1) {
                        arrayJson.push(item);
                    } else {
                        if (!item.sa) {
                            arrayJson.push(item);
                        } else {
                            for (j = 0; j < arrayJson.length; j++) {
                                if (arrayJson[j].in_code === item.sa) {
                                    arrayJson[j].indicators.push(item);
                                    break;
                                }

                            }
                        }
                    }


                }
//                alert(arrayJson);
                return arrayJson;

            };

            self.bipParamBuilder = function (params) {
                var data = '{--Boundary \nContent-Type: application/json \nContent-Disposition: form-data; name=ReportRequest\n\n{"parameterNameValues": {"listOfParamNameValues":  [{"name":"P_INDICATOR","values":"Exports"},{"name":"P_COMP_YEAR","values":"4"}]}}\n--Boundary--}';

                var paramList = params ? {parameterNameValues: {listOfParamNameValues: params}} : {};

                var data = '{--Boundary \nContent-Type: application/json \nContent-Disposition: form-data; name=ReportRequest\n\n' + JSON.stringify(paramList) + '\n--Boundary--}';

                return data;
            };

            self.getBipJson = function (val, paramcnt) {
                var resultArray = val.split("\n");
                var arrayJson = [];
                paramcnt = paramcnt ? paramcnt : 0;
                var j = paramcnt ? 9 : 8;
                var colHeader = [];
                for (i = j; i < resultArray.length - 3; i++) {
                    var colVals = (resultArray[i].replace(/"/g, '')).split(",");
                    var item = {};
                    if (i === j)
                        colHeader = colVals;
                    for (k = paramcnt; i !== j && k < colVals.length; k++) {
                        item[colHeader[k]] = colVals[k];
                        if (i !== j)
                            item['ID'] = i;
                    }
                    if (i !== j)
                        arrayJson.push(item);
                }
                return arrayJson;

            };

            self.postbipCall = function (report, params, fnSuccess) {
//                alert(getAjaxResp('/hybrid/js/properties/Indicators_Menu'));
//alert(indicators);

                var jasonObj = bipParamBuilder(params);
                var p_url = self.bipUrl + report + '/run'

                $.ajax({
                    url: p_url,
                    headers: {
                        'Authorization': 'Basic c3JpbmF0aC5wYXNoYW06Z29saXZlKSk=',
                        'Content-Type': 'multipart/form-data; boundary="Boundary"'
                    },
                    method: 'POST',
//                    contentType: 'text/plain',
                    data: jasonObj,
                    success: function (data) {
                        console.log('succes: ' + data);
                        fnSuccess(data);
                    }
                });
            }


            self.doBIPCall2 = function (params, fnSuccess) {

//                var resultTxt = getAjaxResp('http://localhost:8383/hybrid/css/results.css');
//                convertToJson(resultTxt);


//                var jasonObj = '{--Boundary \nContent-Type: application/json \nContent-Disposition: form-data; name=ReportRequest\n\n{"parameterNameValues": {"listOfParamNameValues":  [{"name":"P_INDICATOR","values":"Exports"},{"name":"P_COMP_YEAR","values":"4"}]}}\n--Boundary--}';
                var jasonObj = bipParamBuilder(params);

                $.ajax({
                    url: 'http://192.168.154.137:9502/xmlpserver/services/rest/v1/reports/Mobile%2FDataModel%2FDynamicReport_RPt/run',
                    headers: {
                        'Authorization': 'Basic c3JpbmF0aC5wYXNoYW06Z29saXZlKSk=',
                        'Content-Type': 'multipart/form-data; boundary="Boundary"'
                    },
                    method: 'POST',
//                    contentType: 'text/plain',
                    data: jasonObj,
                    success: function (data) {
                        console.log('succes: ' + data);
                        fnSuccess(data);
                    }
                });
            };



            self.doBIPCall = function () {
//                alert(getAjaxResp('http://localhost:8383/hybrid/css/results.css'));

//                var jasonObj = JSON.stringify(jasonObj);
                $.ajax({
                    url: 'http://192.168.154.137:9502/xmlpserver/services/rest/v1/reports/Mobile%2FDataModel%2FDynamicReport_RPt',
                    headers: {
                        'Authorization': 'Basic c3JpbmF0aC5wYXNoYW06Z29saXZlKSk='
                    },
                    method: 'get',
//                    dataType: 'json',
                    contentType: 'application/json',
//                    data: jasonObj,
                    success: function (data) {
                        console.log('succes: ' + data);
                    }
                });
            };

            self.doRestCall = function (jsonUrl, jasonObj, Opertype, fnSuccess) {
                console.log("Start doRestCall " + Opertype);
                jasonObj = JSON.stringify(jasonObj);

                var obi = localStorage.getItem(jasonObj);
                $.ajax({
                    headers: {
                        'Authorization': 'Basic d2VibG9naWM6d2VsY29tZTE='
                    },
                    url: jsonUrl,
                    nocache: true,
                    data: jasonObj, // Params being sentz
                    type: Opertype, // Or get
                    contentType: 'application/json',
                    dataType: 'json',

                    success: function (output) {
                        //.. do what you like 

                        localStorage.setItem(jasonObj, JSON.stringify(output));

                        if (fnSuccess)
                            fnSuccess(output);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        console.log('Fail: ' + ajaxOptions);
                        console.log('Fail: ' + xhr);
                        if (xhr.status === 200 || xhr.status === 202)
                            if (fnSuccess)
                                fnSuccess();
//                        doOfflineRestCall(jsonUrl, jasonObj, fnSuccess);
                    },
                    timeout: 2000 //sets timeout to 2 seconds
                });
            };


            self.clubCanvas = function (childs, parent) {
                var canvas = document.getElementById(parent);
                var context = canvas.getContext("2d");
                var height = 0;
                var width = 0;
                for (i = 0; i < childs.length; i++) {
                    var can = document.getElementById(childs[i]);
                    if (can.width > width)
                        width = can.width;
                    height = height + can.height;
                }
                canvas.height = height + 10;
                canvas.width = width + 10;
                context.fillStyle = '#fff';
                context.fillRect(0, 0, width + 10, height + 10);

                for (i = 0, height = 0; i < childs.length; i++) {
                    var can = document.getElementById(childs[i]);
                    context.fillStyle = '#fff';
                    context.drawImage(can, 0 + 5, height + 5);
                    height = height + can.height;
                }
                return canvas;
            };

            self.wrapText = function (can, text, lineHeight) {
                var canvas = document.getElementById(can);
                var context = canvas.getContext("2d");
                if (!lineHeight)
                    lineHeight = 15;
                var words = text.split(' ');
                var line = '';
                var maxWidth = canvas.width;

                var x = 0;
                var lines = [''];
                for (var n = 0; n < words.length; n++) {
                    var testLine = line + words[n] + ' ';
                    var metrics = context.measureText(testLine);
                    var testWidth = metrics.width * lineHeight / 10;
                    if ((testWidth > maxWidth && n > 0) || words[n] === '<br>') {
                        // context.fillText(line, x, y);
                        lines.push(line);
                        if (words[n] !== '<br>')
                            line = words[n] + ' ';
                        else
                            line = '';
                    } else {
                        line = testLine;
                    }
                }
                lines.push(line);
                //  context.fillText(line, x, y);
                // alert(lines)
                canvas.height = lines.length * lineHeight;
                context.font = lineHeight + "px Neo Sans Arabic Arial";
                for (n = 0; n < lines.length; n++) {
//alert(lines[n]);
                    context.fillText(lines[n], 0, n * lineHeight);
                }

            };



            self.findItemFromResult = function (allData, col, value, item) {
                var returnValue;
                if (value === '***' || value === '$$$')
                    returnValue = [];
                ko.utils.arrayForEach(allData, function (rec) {

                    if (rec[col] && value && (value === '$$$' || value === '***' || rec[col] === value.toString())) {
                        if (item && value !== '***') {
                            returnValue = rec[item];
                        } else {
                            if (value === '***') {
                                if (item) {
                                    var childJson = rec[col];
                                    childJson[item] = rec[item];

                                    returnValue.push(childJson);
                                } else
                                    returnValue.push(rec[col]);

                            } else {
                                if (value === '$$$')
                                    returnValue.push(rec[col]);
                                else
                                    returnValue = rec;
                            }
                        }
                    }

                });
                return returnValue;
            };




            self.getLovFromResult = function (col, result) {
                return  getLovPair(null, col, result);
            };

            self.getLovFromArray = function (res) {
                var result = [];
                for (i = 0; i < res.length; i++) {
                    result.push({label: res[i], value: res[i]});
                }
                return result;
            };

            self.getTotal = function (allData, col) {
                var colArray = typeof col === "string" ? [col] : col;
                var totalArray = [];
                ko.utils.arrayForEach(allData, function (item) {
                    for (i = 0; (i < colArray.length && item[colArray[i]]); i++)
                    {
                        if (totalArray[i])
                            totalArray[i] = totalArray[i] + Number(item[colArray[i]]);
                        else
                            totalArray[i] = Number(item[colArray[i]]);
                    }
                });
                return typeof col === "string" ? totalArray[0] : totalArray;
            };



            self.getLovPair = function (labelProperty, valueProperty, allData) {
                var result = [];
                var valArray = [];
                ko.utils.arrayForEach(allData, function (item) {
                    if (!item.hasOwnProperty("active") || item.active === 'Y') {
                        var jsonRecVal = null;
                        if (valueProperty.indexOf('.') > 0) {
                            if (item[valueProperty.split('.')[0]])
                                jsonRecVal = item[valueProperty.split('.')[0]][valueProperty.split('.')[1]];
                        } else {
                            if (item[valueProperty] && item[valueProperty] !== null)
                                jsonRecVal = item[valueProperty].toString();
                        }

                        var jsonRecLbl = null;
                        if (labelProperty) {
                            if (labelProperty.indexOf('.') > 0) {
                                if (item[labelProperty.split('.')[0]])
                                    jsonRecLbl = item[labelProperty.split('.')[0]][labelProperty.split('.')[1]];
                            } else {
                                if (item[labelProperty] && item[labelProperty] !== null)
                                    jsonRecLbl = item[labelProperty].toString();
                            }
                        }

                        if (!valArray.includes(jsonRecVal)) {
                            valArray[valArray.length] = jsonRecVal;
                            if (labelProperty)
                                result.push({label: jsonRecLbl, value: jsonRecVal});
                            else
                                result.push({label: jsonRecVal, value: jsonRecVal});
                        }
                    }
                });
                return result;
            };



            self.getAjaxResp = function (remote_url) {
                return $.ajax({
                    type: "GET",
                    url: remote_url,
                    async: false
                }).responseText;
            };

            self.doFilterSearch = function (Keys, Vals, list, strict, chooseAny) {
                var model = list.slice(0);
                for (i = 0; i < Vals.length; i++) {
                    if (Vals[i] === null || Vals[i].toString().trim().length === 0)
                        Keys.splice((i + Keys.length - Vals.length), 1);
                }
                for (i = 0; i < Vals.length; i++) {
                    if (Vals[i] === null || Vals[i].toString().trim().length === 0) {
                        Vals.splice(i, 1);
                        i--;
                    }
                }

                //
                if (Vals.length < 1 || Keys.length < 1)
                    return list;
                //                  console.log(Keys);console.log(Vals);console.log(model);
                var temp = ko.utils.arrayFilter(model,
                        function (rec) {
                            var retVal = true;
                            var rowRecVal = '';
                            if (chooseAny)
                                retVal = false;
                            for (i = 0; i < Vals.length; i++) {
                                rowRecVal = null;
                                filterVal = Vals[i].toString().trim().toLowerCase();
                                if (Keys[i].indexOf('.') > 0) {
                                    jsonRecVal = null;
                                    if (rec[Keys[i].split('.')[0]])
                                        jsonRecVal = rec[Keys[i].split('.')[0]][Keys[i].split('.')[1]];
                                    if (jsonRecVal !== null)
                                        rowRecVal = jsonRecVal.toString().toLowerCase();
                                } else {
                                    if (Keys[i] !== null && rec[Keys[i]] && rec[Keys[i]] !== null)
                                        rowRecVal = rec[Keys[i]].toString().toLowerCase();
                                }

                                //                                 console.log(rowRecVal);console.log(filterVal);
                                if (chooseAny) {
                                    if ((retVal === true) || (
                                            (Vals[i].toString().trim().length > 0 && rowRecVal !== null &&
                                                    ((strict && rowRecVal === filterVal) ||
                                                            (!strict && rowRecVal.indexOf(filterVal) > -1))))) {
                                        retVal = true;
                                    } else {
                                        retVal = false;
                                    }
                                } else if ((retVal === true) && (
                                        (Vals[i].toString().trim().length > 0 && rowRecVal !== null &&
                                                ((strict && rowRecVal === filterVal) ||
                                                        (!strict && rowRecVal.indexOf(filterVal) > -1))))) {
                                    retVal = true;
                                } else {
                                    retVal = false;
                                }
                            }

                            return retVal;
                        });
                return temp;
            };


            self.autoCorrect = function (arrayStr, str) {
                if (str.length < 5)
                    return str;
                str = str.toLowerCase();
                matchCnt = 0;
                for (k = 0; k < arrayStr.length; k++) {
                    arrayStr[k] = arrayStr[k].toLowerCase();
                    if (arrayStr[k].includes(str))
                        matchCnt++;
                }
                if (matchCnt > 0)
                    return str;
                maxfirCnt = 0;
                maxFir = 0;
                for (i = 1; i < str.length + 1; i++)
                {
                    for (k = 0; k < arrayStr.length; k++) {
                        if (arrayStr[k].length > i && str.substr(0, i) === arrayStr[k].substr(0, i)) {
                            if (i > maxfirCnt) {
                                maxfirCnt = i;
                                maxFir = k;
                            }
                        }
                    }

                }


                maxCarCnt = 0;
                maxCar = 0;
                for (k = 0; k < arrayStr.length; k++)
                {
                    var cnt = 0;
                    for (i = 0; i < str.length; i++) {
                        if (arrayStr[k].includes(str.charAt(i))) {
                            cnt++;
                            if (arrayStr[k].length > cnt && cnt > maxCarCnt) {
                                maxCarCnt = cnt;
                                maxCar = k;
                            }
                        }
                    }

                }

                var returnVal = 'str';
                if (maxfirCnt > 0) {
                    if (maxfirCnt > 3)
                        returnVal = arrayStr[maxFir];
                    else
                    {
                        if (maxCarCnt >= maxfirCnt + 4)
                            returnVal = arrayStr[maxCar];
                        else
                            returnVal = arrayStr[maxFir].substr(0, maxfirCnt);
                    }
                }
                if (returnVal.includes(str))
                    return str;
                else
                    return returnVal;
            };
        });

