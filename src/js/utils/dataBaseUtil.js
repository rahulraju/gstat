/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define(['jquery', 'knockout', 'utils/queryUtil'],
    function($) {

        function dataBaseUtil() {
            var self = this;
            var dataBaseName = 'Masdar_Fav_Reports.db';
            var sampleDB = null;

            // A function to open DB connection, pass the DB name, location
            self.openDBConnection = function() {
                sampleDB = window.sqlitePlugin.openDatabase({ name: dataBaseName, location: 'default', createFromLocation: 1 }, function(db) {
                    console.log('Open database Sucess ');
                }, function(error) {
                    console.log('Open database ERROR: ' + JSON.stringify(error));
                });
                //Set the foreign keys pragma
                sampleDB.executeSql('PRAGMA foreign_keys = ON');
            };

            // A function to close DB connection
            self.closeDBConnection = function() {
                sampleDB.close(function() {
                    console.log("DB closed!");
                }, function(error) {
                    console.log("Error closing DB:" + error.message);
                });
            };

            // A function to get the data from DB by passing query, parameters
            self.queryData = function(query, bindVariablesArray) {
                console.log('Query for execution is : ' + query);
                console.log('Bind Variables for querying data : ' + bindVariablesArray);
                var dfr = $.Deferred();
                if (!sampleDB) {
                    self.openDBConnection();
                }
                sampleDB.transaction(function(tx) {
                    tx.executeSql(query, bindVariablesArray,
                        function(tx, result) {
                            dfr.resolve(result);
                        },
                        function(tx, error) {
                            //   notifyUser('Error while fetching data !', 'warning');
                            console.error('Error when executing the query : ' + JSON.stringify(error));
                        }
                    );
                });
                return dfr.promise();
            };

            // A function to insert the data from DB by passing query, parameters
            self.insertData = function(query, valuesArray) {
                console.log('Query for Insertions is : ' + query);
                console.log('Bind Variables for inserting data : ' + valuesArray);
                var dfr = $.Deferred();
                if (!sampleDB) {
                    self.openDBConnection();
                }
                sampleDB.transaction(function(tx) {
                    tx.executeSql(query, valuesArray,
                        function(tx, result) {
                            dfr.resolve(result);
                        },
                        function(tx, error) {
                            //    notifyUser('Error while inserting data !', 'warning');
                            console.error('Error when executing the query : ' + JSON.stringify(error));
                        }
                    );
                });
                return dfr.promise();
            };

        };
        return new dataBaseUtil();
    }
);