/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


define(['knockout', 'jquery'],
    function() {

        function queryUtil() {
            var self = this;


            self.insertReport = "INSERT INTO FAVOURITE_REPORTS (REPORT_NAME, SUBJECTAREA_ID, INDICATOR_ID, FILTER) VALUES (?, ?, ?, ?)";

            self.getReportNames = "SELECT REPORT_ID, REPORT_NAME FROM FAVOURITE_REPORTS";

            self.fetchReport = "SELECT SUBJECTAREA_ID, INDICATOR_ID, FILTER FROM FAVOURITE_REPORTS WHERE REPORT_ID = ?";

        };

        return new queryUtil();
    }
);