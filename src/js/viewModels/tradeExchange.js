/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * tradeExchange module
 */
define(['ojs/ojcore', 'knockout', 'ojs/ojchart', 'ojs/ojmodel', 'ojs/ojbutton'
], function (oj, ko) {
    /**
     * The view model for the main content view template
     */
    function tradeExchangeContentViewModel() {
        var self = this;
        self.stackValue = ko.observable('off');
        self.orientationValue = ko.observable('vertical');
        self.barSeriesValue = ko.observableArray();
        self.barGroupsValue = ko.observableArray();
        self.buttonClick = function (event, ui) {

            var Report = oj.Collection.extend({
                url: "http://192.168.1.136:7101/BIRestService/resources/gastat/getTradeExchange"
            });
            var membersColl = new Report();

            membersColl.fetch({
                success: function (model, response, options) {
                    var barSeries = [{name: "Exports", items: response['exports']},
                        {name: "Imports", items: response['imports']}];

                    var barGroups = response['country'];
                    self.barSeriesValue(barSeries);
                    self.barGroupsValue(barGroups);
                },
                error: function (model, xhr, options) {
                    alert("employee.fetch error - " + model);
                }});

            console.log("Create Clicked ");
        };

//
//        /* chart data */
//        var barSeries = [{name: "Series 1", items: [42, 34]},
//            {name: "Series 2", items: [55, 30]},
//            {name: "Series 3", items: [36, 50]},
//            {name: "Series 4", items: [22, 46]},
//            {name: "Series 5", items: [22, 46]}];
//
//        var barGroups = ["Group A", "Group B"];
//
//        self.barSeriesValue = ko.observableArray(barSeries);
//        self.barGroupsValue = ko.observableArray(barGroups);
    }

    return tradeExchangeContentViewModel;
});
