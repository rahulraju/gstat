/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your about ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojknockout', 'ojs/ojbutton',
    'ojs/ojaccordion', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBc0ZCWALgkRu6pcRh3KaH1yCS5UCXrObw'],
        function (oj, ko, $) {

            function AboutViewModel() {
                var self = this;

                $(function () {

                    //  $("#accordion").accordion({  active: false,collapsible: true});


                    function initMap() {
                        var myCenter = new google.maps.LatLng(24.663435, 46.711033);
                        var adjustCenter = new google.maps.LatLng(24.677, 46.697);
                        var mapCanvas = document.getElementById("googleMap");
                        var mapOptions = {center: adjustCenter, zoom: 14};
                        var map = new google.maps.Map(mapCanvas, mapOptions);
                        var marker = new google.maps.Marker({position: myCenter});
                        marker.setMap(map);
                    }





                    google.maps.event.addDomListener(window, 'load', initMap());
                    // document.getElementById("div1").innerHTML = model.aboutGastatContact();
                });



                // Below are a subset of the ViewModel methods invoked by the ojModule binding
                // Please reference the ojModule jsDoc for additional available methods.

                /**
                 * Optional ViewModel method invoked when this ViewModel is about to be
                 * used for the View transition.  The application can put data fetch logic
                 * here that can return a Promise which will delay the handleAttached function
                 * call below until the Promise is resolved.
                 * @param {Object} info - An object with the following key-value pairs:
                 * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
                 * @param {Function} info.valueAccessor - The binding's value accessor.
                 * @return {Promise|undefined} - If the callback returns a Promise, the next phase (attaching DOM) will be delayed until
                 * the promise is resolved
                 */
                self.handleActivated = function (info) {
                    // Implement if needed
                };

                /**
                 * Optional ViewModel method invoked after the View is inserted into the
                 * document DOM.  The application can put logic that requires the DOM being
                 * attached here.
                 * @param {Object} info - An object with the following key-value pairs:
                 * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
                 * @param {Function} info.valueAccessor - The binding's value accessor.
                 * @param {boolean} info.fromCache - A boolean indicating whether the module was retrieved from cache.
                 */
                self.handleAttached = function (info) {
                    // Implement if needed
                };


                /**
                 * Optional ViewModel method invoked after the bindings are applied on this View. 
                 * If the current View is retrieved from cache, the bindings will not be re-applied
                 * and this callback will not be invoked.
                 * @param {Object} info - An object with the following key-value pairs:
                 * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
                 * @param {Function} info.valueAccessor - The binding's value accessor.
                 */
                self.handleBindingsApplied = function (info) {
                    // Implement if needed
                };

                /*
                 * Optional ViewModel method invoked after the View is removed from the
                 * document DOM.
                 * @param {Object} info - An object with the following key-value pairs:
                 * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
                 * @param {Function} info.valueAccessor - The binding's value accessor.
                 * @param {Array} info.cachedNodes - An Array containing cached nodes for the View if the cache is enabled.
                 */
                self.handleDetached = function (info) {
                    // Implement if needed
                };

                self.callMobileNumber = function () {
                    call('2323');
                };
                self.callMobile = function (number) {

                    var onSuccess = {};
                    var onError = {};

                    try {
                        window.plugins.CallNumber.callNumber(onSuccess, onError, number, true);
                    } catch (ex) {
                        alert("Please go to the mobile settings and give contacts permission of the Masadar App");
                    }

                };


            }
            ;







            function call(number) {
                try {
                    window.plugins.CallNumber.callNumber(onSuccess, onError, number, true);
                } catch (ex) {
                    alert("Please go to the mobile settings and give contacts permission of the Masadar App");
                }
            }
            ;

            function onSuccess(result) {
                alert("Success:" + result);
            }

            function onError(result) {
                alert("Error:" + result);
            }

            /*
             * Returns a constructor for the ViewModel so that the ViewModel is constructed
             * each time the view is displayed.  Return an instance of the ViewModel if
             * only one instance of the ViewModel is needed.
             */
            return new AboutViewModel();
        }

);
