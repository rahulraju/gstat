
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojbutton', 'ojs/ojmodel', 'ojs/ojchart'
            , 'ojs/ojinputtext', 'ojs/ojtoolbar', 'libs/orientation/loader', 'libs/stack/loader'],
        function (oj, ko, $)
        {

            //self.counter = 0;
            function ChartViewModel() {
                var self = this;
                /* toggle button variables */


//                self.stackValue = ko.observable('off');
//                self.orientationValue = ko.observable('vertical');
//
//                /* chart data */
//                var comboSeries = [{name: "Total Revenue", items: [74, 42, 50, 46,50,60,40], type: "bar"},
//                    {name: "Total Revenue (% of GDP)", items: 
//                            [{y:0.1, label:"10%", labelPosition:'auto'},
//                            {y:0.06, label:"6%", labelPosition:'auto'},
//                            {y:0.07, label:"7%", labelPosition:'auto'},
//                            {y:0.08, label:"8%", labelPosition:'auto'},
//                            {y:0.06, label:"6%", labelPosition:'auto'},
//                            {y:0.07, label:"7%", labelPosition:'auto'},
//                            {y:0.09, label:"9%", labelPosition:'auto'}], type: "line"}];
//
//                var comboGroups = ["2010", "2011", "2012", "2013","2014","2015","2016"];
//
//                this.comboSeriesValue = ko.observableArray(comboSeries);
//                this.comboGroupsValue = ko.observableArray(comboGroups);
//                this.yMax = 100;
//                var converterFactory = oj.Validation.converterFactory('number');
//                var converterOptions = {style: 'percent'};
//                this.y2Converter = converterFactory.createConverter(converterOptions);
//
//
//                
//
////                self.barSeriesValue = ko.observableArray();
////                self.barGroupsValue = ko.observableArray();
//                var barSeries = [{name: "Series 1", items: [42, 34]},
//                    {name: "Series 2", items: [55, 30]},
//                    {name: "Series 3", items: [36, 50]},
//                    {name: "Series 4", items: [22, 46]},
//                    {name: "Series 5", items: [22, 46]}];
//
//                var barGroups = ["Group A", "Group B"];
//
//                self.barSeriesValue = ko.observableArray(barSeries);
//                self.barGroupsValue = ko.observableArray(barGroups);
//                self.stackValue = ko.observable('off');
//                self.orientationValue = ko.observable('vertical');
                /* chart data */

 /* chart data */
                var comboGroups = ["2010", "2011", "2012", "2013","2014","2015","2016","2017"];
                var counts = [74, 42, 50, 46,50,60,40,70];

                // Function to create data for pareto graph.
                var createParetoData = function (counts) {

                    var sum = counts.reduce(function (a, b) {
                        return a + b;
                    });
                    var data = {'percents': [], 'sum': sum};
                    var cumPercent = 0;
                    for (var i = 0; i < counts.length; i++) {
                        cumPercent = counts[i] / sum;
                        data['percents'].push({y:cumPercent,label:Math.round(cumPercent*100)+"%"});
                    }
                    return data;
                };

                /* compute the percent series*/
                var data = createParetoData(counts);
                var series = [
                    {name: "Total Revenue", items: counts},
                    {name: "Total Revenue (% of GDP)", items: data["percents"], assignedToY2: "on"}
                ];

                this.comboSeriesValue = ko.observableArray(series);
                this.comboGroupsValue = ko.observableArray(comboGroups);
                this.yMax = data["sum"]/4;

                var converterFactory = oj.Validation.converterFactory('number');
                var converterOptions = {style: 'percent'};
                this.y2Converter = converterFactory.createConverter(converterOptions);
            }

            return ChartViewModel;
        });
