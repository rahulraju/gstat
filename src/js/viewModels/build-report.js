/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * build-report module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'utils/dataBaseUtil', 'utils/queryUtil', 'ojs/ojknockout', 'ojs/ojtrain', 'ojs/ojtable',
    'ojs/ojbutton', 'ojs/ojselectcombobox', 'ojs/ojchart', 'ojs/ojinputtext', 'ojs/ojlabel', 'ojs/ojarraydataprovider', 'ojs/ojdialog', 'ojs/ojinputtext', 'masdar_util'
],
        function (oj, ko, $, dbUtil, queryUtil) {

            function TrainData() {
                var self = this;
                self.hierIndicator = ko.observableArray([]);
                self.hierIndicator(getKeyIndicatorJson());

                self.selSector = ko.observable('FE');
                self.selBA = ko.observable('FT');
                self.selSA = ko.observable('');
                self.selIN = ko.observable('');
                self.selDim = ko.observable('');

                var reCompute = ko.observable('true');


                self.getSEC = ko.pureComputed(function () {
                    var retArray = [];
                    retArray.push({label: model.lang() ? 'Finance and Economics' : 'الماليه والاقتصاد', value: 'FE'});
                    if (reCompute() !== 'none')
                        return retArray;
                });


                self.getBA = ko.pureComputed(function () {
                    var subArray = [];
                    if (self.selSector()) {
                        ko.utils.arrayForEach(self.hierIndicator(), function (rec) {
                            subArray.push({label: model.lang() ? rec.name_eng : rec.name_arb, value: rec.in_code});
                        });
                    }
                    if (!self.selBA() && subArray.length > 0)
                        self.selBA(subArray[0].value);

                    return subArray;
                });


                self.getSA = ko.pureComputed(function () {
                    var indArray = [];
                    ko.utils.arrayForEach(self.hierIndicator(), function (rec) {
                        if (self.selBA() === rec.in_code) {
                            ko.utils.arrayForEach(rec.indicators, function (item) {
                                indArray.push({label: model.lang() ? item.name_eng : item.name_arb, value: item.in_code});
                            });
                        }
                    });

                    return indArray;
                });



                self.getIN = ko.pureComputed(function () {
                    var indArray = [];
                    ko.utils.arrayForEach(self.hierIndicator(), function (rec) {
                        if (self.selBA() === rec.in_code) {
                            ko.utils.arrayForEach(rec.indicators, function (item) {
                                if (self.selSA() === item.in_code) {
                                    var grps = item.group_code;
                                    var grpName = model.lang() ? item.group_eng : item.group_arb;
                                    if (grps) {
                                        grps = grps.split(";");
                                        grpName = grpName.split(";");
                                        for (i = 0; i < grps.length; i++) {
                                            indArray.push({label: grpName[i], value: grps[i]});
                                        }
                                    }


                                }

                            });
                        }
                    });

                    return indArray;
                });

                self.refreshTrain = function () {

                    if (self.compareValue()) {
                        self.dispFil();
                        ko.utils.arrayForEach(self.compareYear(), function (rec) {
                            ko.utils.arrayForEach(rec.children, function (item) {
                                if (item.value === self.compareValue())
                                    self.dispFil(item.label);
                            });
                        });
                    }

                    ko.utils.arrayForEach(self.hierIndicator(), function (rec) {
                        if (self.selBA() === rec.in_code) {
                            ko.utils.arrayForEach(rec.indicators, function (item) {
                                if (self.selSA() === item.in_code) {
                                    self.dispSA(model.lang() ? item.name_eng : item.name_arb);
                                    var grps = item.group_code;
                                    var grpName = model.lang() ? item.group_eng : item.group_arb;
                                    if (grps) {
                                        grps = grps.split(";");
                                        grpName = grpName.split(";");
                                        for (i = 0; i < grps.length; i++) {
                                            if (grps[i] === self.selIN())
                                                self.dispIn(grpName[i]);
                                        }
                                    }


                                }

                            });
                        }
                    });
                };


                self.isNext = ko.observable(false);
                self.isPrev = ko.pureComputed(function () {
                    if (self.selectedStepValue() === 'stp1')
                        return false;
                    return true;
                });

                self.trainNext = function (data, event) {
//                alert(self.selectedStepValue());
                    self.refreshTrain();
                    if (self.selectedStepValue() === 'stp1') {
                        self.selectedStepValue('stp2');
                        self.selSA(self.getSA()[0].value);
                    } else if (self.selectedStepValue() === 'stp2') {
                        if(!self.selIN() && self.getIN().length > 0)
                            self.selIN(self.getIN()[0].value);
                            
                        self.selectedStepValue('stp3');
                    } else if (self.selectedStepValue() === 'stp3') {
                        self.selectedStepValue('stp4');
                    } else if (self.selectedStepValue() === 'stp4') {
                        self.selectedStepValue('stp5');
                        callBIRestService();
                    }


                };

                self.trainPrev = function (data, event) {
                    alert(self.selectedStepValue());
                };


//                
                self.trainRefresh = function (data, event) {

                    if (!self.selBA() && self.getBA().length > 0)
                        self.selBA(self.getBA()[0].value);
                    if (!self.selSA() && self.getSA().length > 0)
                        self.selSA(self.getSA()[0].value);
                    if (self.getIN().length > 0 && !self.selIN())
                        self.selIN(self.getIN()[0].value);
                };
//                
//                 self.trainRefresh();


                self.compareYear = ko.observableArray([
                    {label: model.compare(),
                        children: [
                            {value: "2", label: model.yrs2()},
                            {value: "3", label: model.yrs3()},
                            {value: "5", label: model.yrs5()}
                        ]},
                    {label: model.year(),
                        children: [
                            {value: "2017", label: "2017"},
                            {value: "2016", label: "2016"},
                            {value: "2015", label: "2015"},
                            {value: "2014", label: "2014"},
                            {value: "2013", label: "2013"},
                            {value: "2012", label: "2012"},
                            {value: "2011", label: "2011"},
                            {value: "2010", label: "2010"},
                            {value: "2009", label: "2009"}

                        ]}
                ]);

                self.subjectAreas = [];

                self.subjectAreasOptions = ko.observable();
                self.indicators = ko.observable();
                self.indicatorsOptions = ko.observable([]);
                self.valueOptions = ko.observable([]);
                self.groupOptions = ko.observable([]);
                self.selectedIndicator = ko.observable();
                self.selectedGroup = ko.observable();
                self.selectedValue = ko.observable();
                self.filtration = ko.observableArray();
                self.selectedStp4 = ko.observable();

                self.compareValue = ko.observable('5');
                self.compareOptions = ko.observableArray();


                self.dispSA = ko.observable('');
                self.dispIn = ko.observable('');
                self.dispDim = ko.observable('');
                self.dispFil = ko.observable('');
                self.selectedFavouriteReport = ko.observable();
                self.favouriteReportName = ko.observable('');

                self.favGraph = ko.observable(false);
                self.favPath = ko.observable('');
                self.favRepSel = ko.observable('');
                self.favArray = ko.observableArray([]);
                self.dataprovider = new oj.ArrayDataProvider(self.favArray, {idAttribute: 'FavName'});

                self.favBack = function () {
                    self.favGraph(false);
                };

                self.columnArray = [{"headerText": model.reportName(),
                        "sortable": "enabled", "field": "FavName"},
                    {"headerText": "",
                        "sortable": "disabled",
                        "renderer": oj.KnockoutTemplateUtils.getRenderer("Fav_Name", true),
                    }
                ];

                self.currFav = function (data, event) {
//               alert(data);

                    self.favRepSel(data.detail.currentRow.rowKey);


                    console.log(getFavVal(data.detail.currentRow.rowKey));
                    self.favPath(getFavVal(data.detail.currentRow.rowKey).path);



                };

                self.doFav = function () {
                    self.favGraph(true);
                    favReportCall(getFavVal(self.favRepSel()), getFavVal(self.favRepSel(), 'compareYear'));

                };

                getFavVal = function (favNam, field) {
                    var FavList = localStorage.getItem('FavList');
                    if (FavList) {
                        FavList = JSON.parse(FavList);
                        FavList = FavList.favList;
                        for (i = 0; i < FavList.length; i++) {
                            if (FavList[i].favName === favNam) {
                                if (field)
                                    return FavList[i][field];
                                else
                                    return FavList[i].params;
                                break;
                            }
                        }
                        ;
                    }
                };


                self.saveFavouriteReport = function () {
//                var tempsubjectAreaId = self.indicators();
//
//                var tempselectedIndicatorId = self.selectedIndicator().indicatorId;
//
//                var filters = self.selectedIndicator().param.filter;

                    var p_indi_code = self.selIN() ? self.selIN() : self.selSA();
                    var params = [{name: "P_INDICATOR", values: p_indi_code}, {name: "P_YEAR", values: "1"}, {name: "P_COMP_YEAR", values: self.compareValue()}];
                    var newFav = {
                        favName: self.favouriteReportName(),
                        params: params,
                        compareYear: self.compareValue()
                    };
                    var FavList = localStorage.getItem('FavList');
                    if (!FavList)
                        FavList = '{ "favList" :[]}';
                    FavList = JSON.parse(FavList);
                    FavList = FavList.favList;
                    var k = -1;
                    for (i = 0; i < FavList.length; i++) {
                        if (FavList[i].favName === self.favouriteReportName()) {
                            k = i;
                        }
                    }
                    ;
                    if (k >= 0) {
                        FavList[k] = newFav;
                    } else
                        FavList.push(newFav);
                    FavList = {favList: FavList};

                    localStorage.setItem('FavList', JSON.stringify(FavList));
                    self.fetchReportNames();

//                dbUtil.insertData(queryUtil.insertReport, [self.favouriteReportName(), tempsubjectAreaId, tempselectedIndicatorId, JSON.stringify(filters)]).then(function(response) {
//                    console.log('Report has been saved into DB');
//                });

                    document.querySelector('#modalDialog1').close();
                };

                self.fetchReportNames = function () {
                    self.favouriteReports = ko.observable();
                    self.tempReportNames = ko.observableArray([]);
//                    self.favArray([]);
                    var FavList = localStorage.getItem('FavList');
                    var favArray = [];
                    var indicArray = [];
                    if (FavList) {
                        FavList = JSON.parse(FavList);
                        FavList = FavList.favList;
                        for (i = 0; i < FavList.length; i++) {
                            var item = {FavName: FavList[i].favName, Indi: getIndicatorName(FavList[i].params.path)};
                            favArray.push(item);
//                            indicArray.push(getIndicatorName(FavList[i].params.path));
                        }
                    }
                    self.favArray(favArray);
//            self.tempReportNames(favArray);


//                dbUtil.queryData(queryUtil.getReportNames, []).then(function(response) {
//                    if (response.rows.length > 0) {
//                        for (var i = 0; i < response.rows.length; i++) {
//                            var temp = {};
//                            temp.value = response.rows.item(i).REPORT_ID;
//                            temp.label = response.rows.item(i).REPORT_NAME;
//                            self.tempReportNames().push(temp);
//                        }
//                    }
//                });
                    self.favouriteReports = new oj.ArrayDataProvider(self.tempReportNames, {idAttribute: 'value'});

                };




                self.generateReport = function () {

                    console.log(self.filtration());
                    self.checkCompare();
                    for (var i = 0; i < self.filtration().length; i++) {
                        for (var j = 0; j < self.selectedIndicator().param.filter.length; j++) {
                            //console.log(parameter().filter[j].key + " - " + filtrationValues()[i].key);

                            self.selectedStp4(self.selectedIndicator().param);
                            if (self.selectedIndicator().param.filter[j].key == self.filtration()[i].key) {
                                if (self.filtration()[i].value.length == 1) {
                                    self.selectedIndicator().param.filter[j].value = self.filtration()[i].value[0];
                                } else {
                                    self.selectedIndicator().param.filter[j].value = self.filtration()[i].value;
                                }
                            }
                        }
                    }
                    console.log(self.selectedIndicator().metadata);
                    self.metadata(self.selectedIndicator().metadata);
                    callRestService();
                    hideall();
                    document.getElementById("5").style.display = "block";
                    this.selectedStepValue("stp5");
                };

                self.currentRawName = ko.observable('');
                self.savebuttonDisabled = ko.observable(true);

                self.saveRawValueChangeCallback = function (event) {
                    if (self.currentRawName()) {
                        self.savebuttonDisabled(false);
                    } else {
                        self.savebuttonDisabled(true);
                    }
                };


                self.filterSelectionVal = ko.pureComputed(function () {
                    var returnVal = 'No Selection';
                    if (self.indicators()) {

                        for (k = 0; k < self.subjectAreas.length; k++) {

                            if (self.subjectAreas[k]['subjectAreaId'] === self.indicators()) {
                                self.dispSA(self.subjectAreas[k]['name']());
                                returnVal = self.subjectAreas[k]['name']();
                            }
                        }
                    }
                    if (self.selectedIndicator()) {
                        self.dispIn(self.selectedIndicator()['name']());
                        returnVal = returnVal + ' >> ' + self.selectedIndicator()['name']();
                    }
                    if (self.selectedGroup()) {
                        var groupList = self.selectedIndicator()['dimensions']['group'];
                        for (k = 0; k < groupList.length; k++) {

                            if (groupList[k]['value'] === self.selectedGroup()) {
                                self.dispDim(groupList[k]['label']);
                                returnVal = returnVal = returnVal + ' >> ' + groupList[k]['label'];
                            }
                        }
                    }

                    if (self.selectedValue()) {
                        var valList = self.selectedIndicator()['dimensions']['value'];
                        for (k = 0; k < valList.length; k++) {

                            if (valList[k]['value'] === self.selectedValue()) {
                                self.dispDim(self.dispDim() + ' / ' + valList[k]['label']);
                                returnVal = returnVal = returnVal + ' >> ' + valList[k]['label'];
                            }
                        }

                        if (self.selectedStp4() || (self.filtration() && self.selectedIndicator()['param'])) {
                            self.dispFil('');
                            for (var i = 0; i < self.filtration().length; i++) {
                                for (var j = 0; j < self.selectedIndicator().param.filter.length; j++) {
                                    //console.log(parameter().filter[j].key + " - " + filtrationValues()[i].key);
                                    if (self.selectedIndicator().param.filter[j].key === self.filtration()[i].key) {
                                        if (self.filtration()[i].value.length === 1) {
                                            self.dispFil(self.dispFil() + ' / ' + self.filtration()[i].value[0]);
                                            returnVal = returnVal = returnVal + ' >> ' + self.filtration()[i].value[0];
                                        } else {
                                            self.dispFil(self.filtration()[i].value);
                                            returnVal = returnVal = returnVal + ' >> ' + self.filtration()[i].value;
                                        }
                                    }
                                }
                            }

                        }



                    }

                    return returnVal;

                });

                this.selectedStepValue = ko.observable('stp1');
                this.selectedStepLabel = ko.observable('Step One');

                this.stepArray =
                        ko.observableArray(
                                [{label: 'Step 1', id: 'stp1'},
                                    {label: 'Step 2', id: 'stp2'},
                                    {label: 'Step 3', id: 'stp3', disabled: true},
                                    {label: 'Step 4', id: 'stp4'},
                                    {label: 'Step 5', id: 'stp5'}
                                ]);

                this.stepArrayAr =
                        ko.observableArray(
                                [{label: 'خطوة 1', id: 'stp1'},
                                    {label: 'خطوة 2', id: 'stp2'},
                                    {label: 'خطوة 3', id: 'stp3'},
                                    {label: 'خطوة 4', id: 'stp4'},
                                    {label: 'خطوة 5', id: 'stp5'}
                                ]);

                function hideall() {
                    for (var i = 0; i < 5; i++) {
                        document.getElementById((i + 1) + "").style.display = "none";
                    }
                }

                self.setSubjectAreaOptions = function () {
                    var subjectAreasOptions = [];
                    for (var i = 0; i < self.subjectAreas.length; i++) {
                        subjectAreasOptions.push({value: self.subjectAreas[i].subjectAreaId, label: self.subjectAreas[i].name()})
                    }
                    self.subjectAreasOptions(subjectAreasOptions);
                    self.indicators('');
                    hideall();
                    document.getElementById("1").style.display = "block";
                    this.selectedStepValue("stp1");
                };

                $(document).ready(function () {
                    self.setSubjectAreaOptions();
                });


                self.selectSubjectArea = function () {
                    var indicatorsOptions = [];
                    //indicatorsOptions.push({});
                    //  console.log('Indicator Option:--->' + self.indicators());
                    console.log('Subject Area');
                    console.log(self.subjectAreas[self.indicators() - 1]);
                    var tempSubjectArea = self.subjectAreas[self.indicators() - 1];
                    console.log(tempSubjectArea);
                    console.log(tempSubjectArea.indicators[1].name());

                    for (var i = 0; i < tempSubjectArea.indicators.length; i++) {
                        indicatorsOptions.push({value: tempSubjectArea.indicators[i], label: tempSubjectArea.indicators[i].name()})

                    }
                    self.indicatorsOptions(indicatorsOptions);

                    self.selectedIndicator('');
                    self.filtration([]);
                    //document.getElementById("filter").innerHTML = "";
                    console.log(self.indicatorsOptions());
                    hideall();
                    document.getElementById("2").style.display = "block";
                    this.selectedStepValue("stp2");
                };

                self.selectIndicator = function () {
                    //console.log(self.selectedIndicator());
                    var groupOptions = [];
                    //indicatorsOptions.push({});
                    for (var i = 0; i < self.selectedIndicator().dimensions.group.length; i++) {
                        groupOptions.push(self.selectedIndicator().dimensions.group[i]);
                    }
                    var valueOptions = [];
                    //indicatorsOptions.push({});
                    for (var i = 0; i < self.selectedIndicator().dimensions.value.length; i++) {
                        valueOptions.push(self.selectedIndicator().dimensions.value[i]);
                    }


                    self.valueOptions(valueOptions);
                    self.groupOptions(groupOptions);
                    self.selectedGroup('');
                    self.selectedValue('');
                    self.selectedStp4('');
                    self.compareValue('');

                    self.filtration(self.selectedIndicator().filtrationValues);
                    if (self.selectedIndicator().compareValues)
                        self.compareOptions(getLovFromArray(self.selectedIndicator().compareValues['values']));
                    else
                        self.compareOptions([]);

                    //console.log(self.selectedIndicator().filtrationValues);
                    hideall();
                    document.getElementById("3").style.display = "block";
                    this.selectedStepValue("stp3");
                };

                self.selectDimensions = function () {
                    //                    console.log(self.selectedGroup());
                    //                    console.log(self.selectedValue());
                    hideall();
                    document.getElementById("4").style.display = "block";
                    this.selectedStepValue("stp4");
                    console.log(self.filtration());
                    // console.log(self.filtration().value);

                };



                this.updateLabelText = function (event) {
                    var train = document.getElementById("train");
                    self.selectedStepLabel(train.getStep(event.detail.value).label);
                    //self.selectedStepLabel(model.step() + ' - ' + event.detail.value);
//                     self.isPrev(true);
                    hideall();
                    document.getElementById(event.detail.value.substring(3)).style.display = "block";
                };

                self.stackValue = ko.observable('off');
                self.orientationValue = ko.observable('vertical');
                self.barSeriesValue = ko.observableArray();
                self.barGroupsValue = ko.observableArray();
                self.barSeriesValueFav = ko.observableArray();
                self.barGroupsValueFav = ko.observableArray();
                self.metadata = ko.observable();
                self.webServiceParam = '';

                self.selectedItem = ko.observable('home');


                function favReportCall(params, compareYear) {
//                console.log(self.selectedIndicator());
//                var doCompare = false;
//                if(!param && compareYear){
//                   param= getFavVal(self.favRepSel());
//                   param.filter[0].value =compareYear;
//                   doCompare= true;
//                };
                    var report = 'IndicatorReport_RPT';
//            var params = [{name: "P_INDICATOR", values: p_indi_code}, {name: "P_YEAR", values: "1"}, {name: "P_COMP_YEAR", values: self.compareValue()}];
                    postbipCall(report, params,
                            function (response) {

                                resultArray = getBipJson(response, 3);
                                var filterArray = resultArray;
//                        filterArray = groupData(filterArray, self.grpValue());
                                var groupCol = model.lang() ? 'G1_ENG' : 'G1_ARB';
//                        self.drillingOption("groupsOnly");
//                        if(p_indi_code.includes('MOF_')){
//                            groupCol='indicator';
//                            self.drillingOption('off');
//                            }
                                var result = getBarGraph(filterArray, groupCol);

                                self.barSeriesValueFav(result.seriesVal);
                                self.barGroupsValueFav(result.groupVal);
                                try {
                                    //callRestService();
                                    document.getElementById("barChartFav").refresh();
                                } catch (err) {
                                    console.log(err);
                                }
                            });


                }


                function callRestService(compare) {
                    console.log(self.selectedIndicator());


                    doRestCall('http://192.168.154.148:7001/BIRestService/resources/gastat/genericReport', self.selectedIndicator().param, 'post',
                            function (response) {

                                var barSeries = [{name: compare ? self.selectedIndicator().param.filter[0].value : self.selectedIndicator().param.label(), items: response['value']}];
                                if (compare) {
                                    barSeries.push(compare);
                                    self.selectedIndicator().param.filter[0].value = compare.name;
                                } else {
                                    if (self.compareValue() && self.compareValue().length > 1) {
                                        var oldYear = self.selectedIndicator().param.filter[0].value;
                                        self.selectedIndicator().param.filter[0].value = self.compareValue();
                                        callRestService({name: oldYear, items: response['value']});
                                    }
                                }
                                var barGroups = response['group'];
                                self.barSeriesValue(barSeries);
                                self.barGroupsValue(barGroups);
                                try {
                                    //callRestService();
                                    document.getElementById("barChart").refresh();
                                } catch (err) {
                                    console.log(err);
                                }
                            });


                    // $.ajax({
                    //     type: "POST",
                    //     url: "http://192.168.155.115:7001/BIRestService/resources/gastat/genericReport",
                    //     dataType: 'json',
                    //     data: JSON.stringify(self.selectedIndicator().param),
                    //     contentType: 'application/json',
                    //     success: function(response) {
                    //         var barSeries = [{ name: self.selectedIndicator().param.label(), items: response['value'] }];
                    //         var barGroups = response['group'];
                    //         self.barSeriesValue(barSeries);
                    //         self.barGroupsValue(barGroups);
                    //         try {
                    //             //callRestService();
                    //             document.getElementById("barChart").refresh();
                    //         } catch (err) {
                    //             console.log(err);
                    //         }
                    //     },
                    //     failure: function(jqXHR, textStatus, errorThrown) {
                    //         console.log(textStatus);
                    //     }
                    // });
                }
                ;

                self.openMetadataURL = function () {
                     window.open(getContextUrl());
//                    cordova.InAppBrowser.open('http://192.168.172.14:7005/sites/Masdar/EN/HomePage', '_self', 'location=no,hardwareback=no');

                };


                self.makeFavourite = function () {
                    document.querySelector('#modalDialog1').open();
                };

                self.checkCompare = function () {
                    if (event.detail.value)
                        self.compareValue(event.detail.value);
                    if (self.compareValue())
                        if (self.selectedIndicator().param)
                            if (self.compareValue() === self.selectedIndicator().param.filter[0].value) {
                                alert('Cannot compare same year');
                                self.compareValue('');
                            }

                };



                self.prepareCanvas = function () {
                    //context.fillRect(0, 0, canvas.width, canvas.height);
                    //                     
                    //                    context.fillRect(0, 0, 500, 500);
                    //                    context.fillStyle = "#FF0000";
                    //                    context.font = "30px Arial";
                    //                    context.fillText("Hello World",0,600);
                    //                    context.fillText("akjhs hda",0,660);





                    //     document.getElementById("canvasMeta").style.fontSize='6px';
                    var can1 = document.getElementById("can1");
                    var context = can1.getContext("2d");
                    context.clearRect(0, 0, can1.width, can1.height);
                    can1.height = 0;
                    //
                    var svg = $('#dynamicReport').find('svg')[0];
//                $('#dynamicReport').find('svg')[0];
                    ////                    //canvas.setAttribute("style","background-image: url(css/images/GaStat.png);");
                    //                     svg.setAttribute("width", "500px");
                    svg.setAttribute("height", "300px");
                    //////                    //console.log(svg.outerHTML);
                    canvg(can1, svg.outerHTML);
                    context.globalCompositeOperation = "destination-over";
                    context.fillStyle = '#fff'; // <- background color
                    var backgroundImage = new Image();
                    backgroundImage.src = 'css/images/masdar-watermark.png';
                    backgroundImage.setAttribute("syle", "background-position: center center;");
                    context.drawImage(backgroundImage, 0, 0);
                    document.getElementById("can2").width = can1.width;
                    document.getElementById("can3").width = can1.width;
                    wrapText('can3', self.metadata(), 16);
                    var text2 = '' + model.sbujectArea() + ':' + self.dispSA() + ' <br> ' + model.indicator() + ':' + self.dispIn() + ' <br> ' +
                            model.dimensions() + ':' + self.dispDim() + ' <br> ' + model.filtration() + ':' + self.dispFil();
                    wrapText('can2', text2, 16);
                    var canVAAS = clubCanvas(['can2', 'can1', 'can3'], 'can4');
                    console.log(canVAAS.toDataURL());


                    //                    html2canvas(document.getElementById("canvasMeta"),useWidth,useHeight).then(canvas => {
                    //                        
                    //
                    //                        var destCanvas = document.getElementById("can3");
                    //
                    //                        var destCtx = destCanvas.getContext("2d");
                    ////                            var svg = document.querySelector("svg");
                    ////                            canvg(destCanvas, svg.outerHTML);
                    ////                           destCtx.font = "6px Arial";
                    //                        destCanvas.height = canvas.height;
                    //                        destCanvas.width = 500;
                    //                        destCtx.drawImage(canvas, 0, 0);
                    //                        document.getElementById("canvasMeta").style.fontSize='';
                    //
                    //
                    //                    });


                    // document.getElementById("canvasDiv").style.fontSize='6px';
                    //                    html2canvas(document.getElementById("canvasDiv")).then(canvas => {      
                    //                         document.getElementById("canvasDiv").style.fontSize='';
                    //                        var destCanvas = document.getElementById("can2");
                    //                        destCanvas.width = $(window).width();
                    //                        destCanvas.height = canvas.height + 10;
                    //                        var destCtx = destCanvas.getContext("2d");
                    //                        destCtx.drawImage(destCanvas, 0, canvas.height);
                    //                        destCtx.drawImage(canvas, 0, 0);

                    //                        var can1 = document.getElementById("can1");
                    //                        var context = can1.getContext("2d");
                    //                        context.clearRect(0, 0, can1.width, can1.height);
                    //                        can1.height = 0;
                    ////
                    //                        var svg = document.querySelector("svg");
                    //////                    //canvas.setAttribute("style","background-image: url(css/images/GaStat.png);");
                    ////                     svg.setAttribute("width", "500px");
                    //                        svg.setAttribute("height", "300px");
                    ////////                    //console.log(svg.outerHTML);
                    //                        canvg(can1, svg.outerHTML);
                    //                        context.globalCompositeOperation = "destination-over";
                    //                        context.fillStyle = '#fff'; // <- background color
                    //                        var backgroundImage = new Image();
                    //                        backgroundImage.src = 'css/images/masdar-watermark.png';
                    //                        backgroundImage.setAttribute("syle", "background-position: center center;");
                    //                        context.drawImage(backgroundImage, 0, 0);



                    //                        var can4 = document.getElementById("can4");
                    //                        var can3 = document.getElementById("can3");
                    //
                    //                        can4.width = $(window).width();
                    //                        can4.height = destCanvas.height + can1.height + can3.height;
                    //                        var drawContext = can4.getContext("2d");
                    //                        drawContext.drawImage(destCanvas, 0, 0);
                    //                        drawContext.drawImage(can1, 0, destCanvas.height);
                    //                        drawContext.drawImage(can3, 0, destCanvas.height + can1.height);

                    //                    });
                };


                self.shareImg = function () {
                    self.prepareCanvas();

                    var canvas = document.getElementById("can4");

                    window.plugins.socialsharing.share(
                            self.selectedIndicator().param.label(),
                            self.selectedIndicator().param.label(), canvas.toDataURL(), null);

                    //document.removeChild(canvas)

                };
                self.saveImg = function () {
                    self.prepareCanvas();
                    var canvas = document.getElementById("can4");

                    var params = {data: canvas.toDataURL(), prefix: 'GaStat_', format: 'PNG', quality: 100, mediaScanner: true};
                    window.imageSaver.saveBase64Image(params,
                            function (filePath) {
                                alert('File saved on ' + filePath);
                            },
                            function (msg) {
                                alert(msg);
                            }
                    );

                };






                self.loadFavoriteReport = function () {
                    var id = self.selectedFavouriteReport();
                    dbUtil.queryData(queryUtil.fetchReport, [id]).then(function (response) {
                        var tempIndicatorId = response.rows.item(0).INDICATOR_ID;
                        var tempSAId = response.rows.item(0).SUBJECTAREA_ID;
                        var tempFilter = JSON.parse(response.rows.item(0).FILTER)


                        self.indicators(self.subjectAreas[tempSAId - 1].indicators);
                        self.selectedIndicator(self.subjectAreas[tempSAId - 1].indicators[tempIndicatorId - 1]);
                        self.filtration(self.selectedIndicator().filtrationValues);

                        self.dispSA = self.subjectAreas[tempSAId - 1].name;
                        self.dispIn = self.subjectAreas[tempSAId - 1].indicators[tempIndicatorId - 1].name;
                        self.dispDim = ko.observable('');
                        self.dispFil = ko.observable('');

                        for (var i = 0; i < self.filtration().length; i++) {
                            for (var j = 0; j < self.selectedIndicator().param.filter.length; j++) {


                                if (self.selectedIndicator().param.filter[j].key == self.filtration()[i].key) {

                                    if (self.filtration()[i].key == tempFilter[i].key) {
                                        self.selectedIndicator().param.filter[j].value = tempFilter[i].value;
                                    }
                                }
                            }
                        }
                        self.metadata(self.selectedIndicator().metadata);
                        callRestService();
                        hideall();
                        document.getElementById("5").style.display = "block";
                        self.selectedStepValue("stp5");

                    });

                };



                function callBIRestService() {
                    var p_indi_code = self.selIN() ? self.selIN() : self.selSA();
                    var report = 'IndicatorReport_RPT';
                    var params = [{name: "P_INDICATOR", values: p_indi_code}, {name: "P_YEAR", values: "1"}, {name: "P_COMP_YEAR", values: self.compareValue()}];
                    postbipCall(report, params,
                            function (response) {

                                resultArray = getBipJson(response, 3);
                                var filterArray = resultArray;
//                        filterArray = groupData(filterArray, self.grpValue());
                                var groupCol = model.lang() ? 'G1_ENG' : 'G1_ARB';
//                        self.drillingOption("groupsOnly");
//                        if(p_indi_code.includes('MOF_')){
//                            groupCol='indicator';
//                            self.drillingOption('off');
//                            }
                                var result = getBarGraph(filterArray, groupCol);

                                self.barSeriesValue(result.seriesVal);
                                self.barGroupsValue(result.groupVal);
                                try {
                                    //callRestService();
                                    document.getElementById("barChart").refresh();
                                } catch (err) {
                                    console.log(err);
                                }
                            });

                }

                self.chartDrillYear = function (event) {
                    var filterArray;
                    if (event) {
                        var detail = event.detail;
                        if (detail['group']) {
                            var drillSel = detail['group'];
                            if (drillSel.length > 2) {
                                self.drillY(drillSel);
                                filterArray = doFilterSearch(['YEAR'], [drillSel], resultArray);
                            }
                        }

                    }
                    try {
                        var groupCol = model.lang() ? 'G1_ENG' : 'G1_ARB';
                        var result = getBarGraph(filterArray, groupCol);
                        self.barSeriesValue(result.seriesVal);
                        self.barGroupsValue(result.groupVal);
                        self.drillingOption("off");
                        document.getElementById("barChart").refresh();
                    } catch (err) {
                        console.log(err);
                    }

                };



                self.closeFavouriteDialog = function () {
                    document.querySelector('#modalDialog1').close();
                };
                self.handleAttached = function () {
                    self.fetchReportNames();
                };

                getIndicatorName = function (path) {
                    return getContext(path, 'IN');
                };

                getContext = function (path, name) {
                    if (!path)
                        path = self.favPath();
                    for (j = 0; j < self.subjectAreas.length; j++) {
                        var indicators = self.subjectAreas[j].indicators;
                        for (h = 0; h < indicators.length; h++) {
                            if (indicators[h].param.path === path) {
                                console.log(self.subjectAreas[j].name());
                                console.log(indicators[h].name());
                                if (name === 'SA')
                                    return self.subjectAreas[j].name();
                                if (name === 'IN')
                                    return indicators[h].name();
                                var filterVal = '';
                                var filter = indicators[h].filtrationValues;
                                for (l = 0; l < filter.length; l++) {
                                    filterVal = filterVal + filter[l].name() + ' / ';
                                    console.log(filter[l].name());
                                }
                                if (name === 'FL')
                                    return filterVal;
                            }
                        }

                    }
                };



            }
            ;
            var trainModel = new TrainData();
            return TrainData;
        });