/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * news module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojknockout', 'ojs/ojaccordion', 'masdar_util'
], function (oj, ko, $) {
    /**
     * The view model for the main content view template
     */
    function newsContentViewModel() {
        var self = this;


        self.isEnglish = function (lang) {
            if (lang)
                return 'display: block;';
            else
                return 'display: none;';
        };

        self.isArabic = function (lang) {
            if (lang)
                return 'display: none;';
            else
                return 'display: block;';
        };

        self.RssFeeds = ko.observableArray([{'title': 'News Title', 'desc': 'News Details'}]);
        self.RssArFeeds = ko.observableArray([{'title': 'أخبار', 'desc': 'description'}]);
        
        self.expandNews = function(event,data){
//            console.log(event);
//             console.log(data);
             
//              event.preventDefault();
          if(event.detail.header){
             var desc =findItemFromResult(model.lang()?self.RssFeeds():self.RssArFeeds(),'title', event.detail.header.childNodes[1].data,'desc');
             document.getElementById(event.detail.content.id).innerHTML = desc;
         }
            
        };


        self.refresh = function (feed_url) {
            url_feed=feed_url;
            if(!feed_url)
                url_feed="https://www.stats.gov.sa/en/rss.xml";

            $.ajax({url:url_feed , success: function (result) {
                    if(typeof result === "string"){
                   var parser = new DOMParser();
                       result=  parser.parseFromString(result,"text/xml");
                    }
                          var xmlDoc = result;

                    var newsArray = xmlDoc.getElementsByTagName("item");
                    var newList = [];
                    for (i = 0; i < newsArray.length; i++) {
                        var title = newsArray[i].getElementsByTagName("title")[0].textContent;
                        var description = newsArray[i].getElementsByTagName("description")[0].textContent;
                        if(title && description && !description.includes('<img'))
                        newList.push({'title': title, 'desc': description});
                    };
                    
                    if(!feed_url)
                         self.RssFeeds(newList);
                     else
                        self.RssArFeeds(newList);


//                var reponseXML = result.substr(39);
//              console.log(reponseXML); 
//                console.log(result['rss']);  
//               console.log(result['channel']);  


                }});
        };
        
        self.refresh();
        self.refresh('https://www.stats.gov.sa/ar/rss.xml');

//        doRestCall('https://205.234.241.196/rss/all','', 'get',
//                    function(response) {
//                        console.log(response);
//                    });



    }

    //Accordion
    $(function ()
    {
//        ko.applyBindings(null, document.getElementById('news-list'));
    }
    );

    return newsContentViewModel;
});
