/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * indicator-pro module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojknockout', 'ojs/ojfilmstrip', 'ojs/ojlabel'
], function (oj, ko, $) {

    /**
     * The view model for the main content view template
     */
    function indicatorproContentViewModel() {
        var self = this;
        //Slider page code
        self.indicators = [
            {name: model.economicEstablishments,
                css: 'icon icon-graph',
                url: 'img/avg-pricing.jpg',
                param: {
                    path: '/shared/Mobile/Economic_Establishments',
                    filter: [{key: 'Column7', value:'2015'},
                        {key: 'Column11', value:'أبحاث الإعلان والسوق'}],
                    group: 'Column8',
                    value: 'Column31',
                    type: 'bar',
                    rows: '0',
                    label: model.economicEstablishments},
                filtrationValues: [
                    {
                        name: 'Year',
                        key: 'Column7',
                        value: '',
                        values: ['2010', '2011', '2012', '2013', '2014', '2015']
                    }
                ]
                
            },
            {name: model.exports,
                css: 'icon icon-globe',
                url: 'img/export.jpg',
                param: {
                    path: '/shared/Economic Statistics/International Trade and Balance of Payment/FT/OBIEE FT/Trade Exchange',
                    filter: [{key: 'Column3', value:'2016'},
                        {key: 'Column2', value:'Exports'}],
                    group: 'Column1',
                    value: 'Column4',
                    type: 'bar',
                    rows: '10',
                    label: model.exports}
            },
            {name: model.imports,
                css: 'icon icon-dashboard',
                url: 'img/import.jpg',
                param: {
                    path: '/shared/Economic Statistics/International Trade and Balance of Payment/FT/OBIEE FT/Trade Exchange',
                    filter: [{key:'Column3',value:'2016'},{key:'Column2',value:'Imports'}],
                    group: 'Column1',
                    value: 'Column4',
                    rows: '10',
                    type: 'bar',                    
                    label: model.imports
                },
                filtrationValues: [
                    {
                        name: 'Year',
                        key: 'Column3',
                        value: '',
                        values: ['2015', '2016']
                    }
                ]
            },
            {name: model.tradeExchange,
                css: 'icon icon-graph',
                url: 'img/trade-exchange.jpg',
                param: {
                    path: '/shared/Economic Statistics/International Trade and Balance of Payment/FT/Static/Published Report/التبادل التجاري حسب مجموعات الدول',
                    filter: [{key:'Column4',value:'2016'},{key:'Column2',value:'دول مجلس التعاون الخليجي'},{key:'Column3',value:'الصادرات'}],
                    group: 'Column1',
                    value: 'Column5',
                    type: 'bar',
                    rows: '0',
                    label: model.tradeExchange
                }
            },
            {name: model.exportsBySection,
                css: 'icon icon-graph',
                url: 'img/export_1.jpg',
                param: {
                    path: '/shared/Economic Statistics/International Trade and Balance of Payment/FT/Static/Published Report/الصادرات حسب الأقسام',
                    filter: [{key:'Column1',value:'2016'}],
                    group: 'Column2',
                    value: 'Column3',
                    type: 'pyramid',
                    rows: '0',
                    label: model.exportsBySection
                }
            },
            {name: model.materialExports,
                css: 'icon icon-graph',
                url: 'img/material-export.jpg',
                param: {
                    path: '/shared/Economic Statistics/International Trade and Balance of Payment/FT/Static/Published Report/الصادرات للأقسام حسب طبيعة المواد',
                    filter: [{key:'Column1',value:'2016'},{key:'Column3',value:'خام'}],
                    group: 'Column2',
                    value: 'Column4',
                    type: 'bar',
                    rows: '0',
                    label: model.materialExports
                }
            },
            {name: model.materialImports,
                css: 'icon icon-graph',
                url: 'img/material-import.jpg',
                param: {
                    path: '/shared/Economic Statistics/International Trade and Balance of Payment/FT/Static/Published Report/الواردات حسب استخدام المواد وطبيعة المواد',
                    filter: [{key:'Column1',value:'2016'},{key:'Column4',value:'رأسمالية'},{key:'Column3',value:'مصنعة'}],
                    group: 'Column2',
                    value: 'Column5',
                    type: 'bar',
                    rows: '0',
                    label: model.materialImports
                }
            }
        ];

        self.routing = function (data) {
            oj.Router.rootInstance.go('dynamicReport');
            parameter(data.param);
            filtrationValues(data.filtrationValues);
        };

        self.currDetailPage = ko.observable({"index": 0});

        handleDetailCreate = function ()
        {
            var filmStrip = document.getElementById('detailFilmStrip');
            var pagingModel = filmStrip.getPagingModel();
            pagingModel.on("beforePage", handleDetailBeforePage);
            handleDetailBeforePage();
        };

        handleDetailBeforePage = function (event)
        {

            var filmStrip = document.getElementById('detailFilmStrip');
            var pagingModel = filmStrip.getPagingModel();
            var pageCount = pagingModel.getPageCount();
            var pageIndex = event ? event.page : 0;

            //wrap in try..catch block because the detail filmstrip is 
            //initialized first and the master filmstrip is not yet 
            //available
            try
            {
                var masterFilmStrip = document.getElementById('masterFilmStrip');
                var masterPagingModel = masterFilmStrip.getPagingModel();
                var masterPage = masterPagingModel.getPage();
                var masterItemsPerPage = masterFilmStrip.getItemsPerPage();
                var newMasterPage =
                        Math.floor(pageIndex / masterItemsPerPage);
                //change master page in detail's beforePage event so that 
                //the master page change transitions at the same time as 
                //the detail page
                if (newMasterPage !== masterPage)
                    masterPagingModel.setPage(newMasterPage);
            } catch (e)
            {
                //do nothing
            }
        };

        masterClick = function (data, event)
        {
            //get the ko binding context for the item DOM element
            var context = ko.contextFor(event.target);
            //get the index of the item from the binding context
            var index = context.$index();
            //set currDetailPage in a timeout so that the page change 
            //transitions smoothly, otherwise it seems that there's some 
            //processing going on that interferes (maybe due to knockout)
            setTimeout(function () {
                self.currDetailPage({"index": index});
            }, 50);
        };

        getDetailItemInitialDisplay = function (index)
        {
            return index < 1 ? '' : 'none';
        };

        getMasterItemInitialDisplay = function (index)
        {
            return index < 3 ? '' : 'none';
        };

        getMasterItemLabelId = function (index)
        {
            return 'masterLabel' + index;
        };

        isMasterItemSelected = function (index)
        {
            return self.currDetailPage().index === index;
        };

        getMasterItemLabelledBy = function (index)
        {
            var labelledBy = getMasterItemLabelId(index);
            if (isMasterItemSelected(index))
                labelledBy += " masterItemSelectedLabel";
            return labelledBy;
        };

    }
    ;





    return indicatorproContentViewModel;

});