/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Indecators module
 */
define(['ojs/ojcore', 'knockout'
], function (oj, ko) {
    /**
     * The view model for the main content view template
     */
    function IndecatorsContentViewModel() {
        var self = this;
        self.items = ko.observableArray([
            {name: model.avgprices, css: 'icon icon-graph',
                url: 'dynamicReport',
                param: {
                    path: '/shared/Economic Statistics/Prices/MD/متوسط الاسعار ونسبةالتغير حسب السنة',
                    filter: '',
                    group: 'Column1',
                    value: 'Column7',
                    type: 'bar',
                    rows: '0',
                    label: model.avgprices}
            },
            {name: model.exports, css: 'icon icon-graph',
                url: 'dynamicReport',
                param: {
                    path: '/shared/Economic Statistics/International Trade and Balance of Payment/FT/OBIEE FT/Trade Exchange',
                    filter: 'Column3^2016~Column2^Exports',
                    group: 'Column1',
                    value: 'Column4',
                    type: 'bar',
                    rows: '10',
                    label: model.exports}
            },
            {
                name: model.imports,
                css: 'icon icon-graph',
                url: 'dynamicReport',
                param: {
                    path: '/shared/Economic Statistics/International Trade and Balance of Payment/FT/OBIEE FT/Trade Exchange',
                    filter: 'Column3^2016~Column2^Imports',
                    group: 'Column1',
                    value: 'Column4',
                    rows: '10',
                    type: 'bar',                    
                    label: model.imports
                }
            }, {
                name: model.tradeExchange,
                css: 'icon icon-graph',
                url: 'dynamicReport',
                param: {
                    path: '/shared/Economic Statistics/International Trade and Balance of Payment/FT/Static/Published Report/التبادل التجاري حسب مجموعات الدول',
                    filter: 'Column4^2016~Column2^دول مجلس التعاون الخليجي~Column3^الصادرات',
                    group: 'Column1',
                    value: 'Column5',
                    type: 'bar',
                    rows: '0',
                    label: model.tradeExchange
                }
            }, {
                name: model.exportsBySection,
                css: 'icon icon-graph',
                url: 'dynamicReport',
                param: {
                    path: '/shared/Economic Statistics/International Trade and Balance of Payment/FT/Static/Published Report/الصادرات حسب الأقسام',
                    filter: 'Column1^2016',
                    group: 'Column2',
                    value: 'Column3',
                    type: 'pyramid',
                    rows: '0',
                    label: model.exportsBySection
                }
            }, {
                name: model.materialExports,
                css: 'icon icon-graph',
                url: 'dynamicReport',
                param: {
                    path: '/shared/Economic Statistics/International Trade and Balance of Payment/FT/Static/Published Report/الصادرات للأقسام حسب طبيعة المواد',
                    filter: 'Column1^2016~Column3^خام',
                    group: 'Column2',
                    value: 'Column4',
                    type: 'bar',
                    rows: '0',
                    label: model.materialExports
                }
            }, {
                name: model.materialImports,
                css: 'icon icon-graph',
                url: 'dynamicReport',
                param: {
                    path: '/shared/Economic Statistics/International Trade and Balance of Payment/FT/Static/Published Report/الواردات حسب استخدام المواد وطبيعة المواد',
                    filter: 'Column1^2016~Column4^رأسمالية~Column3^مصنعة',
                    group: 'Column2',
                    value: 'Column5',
                    type: 'bar',
                    rows: '0',
                    label: model.materialImports
                }}
        ]);

        self.routing = function (data) {

            if (data.url && data.url != '#') {
                oj.Router.rootInstance.go(data.url);
                parameter(data.param);
            }
        };

    }

    return IndecatorsContentViewModel;
});
