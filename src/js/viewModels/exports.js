/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * biDashboard module
 */
define(['ojs/ojcore', 'knockout', 'ojs/ojchart', 'ojs/ojmodel', 'ojs/ojbutton'
], function (oj, ko) {
    /**
     * The view model for the main content view template
     */
    function biDashboardContentViewModel() {
        var self = this;
        self.stackValue = ko.observable('off');
        self.orientationValue = ko.observable('vertical');
        self.barSeriesValue = ko.observableArray();
        self.barGroupsValue = ko.observableArray();


            var Report = oj.Collection.extend({
                url: "http://192.168.1.136:7101/BIRestService/resources/gastat/getExports"
            });
            var membersColl = new Report();

            membersColl.fetch({
                success: function (model, response, options) {
                    var barSeries = [{name: "2016", items: response['value']}];

                    var barGroups = response['country'];
                    self.barSeriesValue(barSeries);
                    self.barGroupsValue(barGroups);
                },
                error: function (model, xhr, options) {
                    alert("employee.fetch error - " + model);
                }});
    }

    return biDashboardContentViewModel;
});
