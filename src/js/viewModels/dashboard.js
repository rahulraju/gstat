/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your dashboard ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery','text!../properties/Indicators_Menu', 'text!../properties/Indicators_Population',  'ojs/ojbutton', 'ojs/ojmodel', 'ojs/ojrouter',
        'ojs/ojpagingcontrol', 'ojs/ojarraypagingdatasource', 'ojs/ojinputtext',
        'translation', 'jquery-ui', 'jquery.autocorrect','ojs/ojselectcombobox', 'ojs/ojarraydataprovider', 'masdar_util'
    ],
    function(oj, ko, $,key_indi,pop_indi) {
        
        function DashboardViewModel() {
            var self = this;

            self.keyword = ko.observable('');
            self.refresh = true;

            var dashboards = [
                {
                    name: model.indicators,
                    css: 'icon icon-graph',
                    url: 'indicator-pro'},
                {
                    name: model.population,
                    css: 'icon icon-people',
                    url: 'population'
                },
                {
                    name: model.report,
                    css: 'icon icon-report',
                    url: 'build-report'
                },
                {
                    name: model.map,
                    css: 'icon icon-map',
                    url: 'stat-map'
                },
                {
                    name: model.library,
                    css: 'icon icon-library',
                    url: 'library'
                },
                {
                    name:  model.news,
                    css: 'icon icon-news',
                    url: 'news'
                },
                {
                    name: model.infographics,
                    css: 'icon icon-infographic',
                    url: 'infographics'
                },

                //{name: model.dashboard, css: 'icon icon-dashboard',
                {
                    name: model.masdarSite,
                    css: 'icon icon-world',
                    url: 'http://192.168.172.14:7005/sites/Masdar/---/HomePage'
                },
                {
                    name: model.gastat,
                    css: 'icon icon-gastat-logo',
                    url: 'about'
                }
            ];
            self.items = ko.observableArray();
            self.items(dashboards);

            self.loadOnClick = function() {
                //alert('loaded');
                loadTags();

            };


            function getSearchResult(keyword) {
                var result = [];
//                dashboards=self.homeMenu();
                console.log("keyword: " + keyword);
                if (!keyword || keyword === '') {
                    result = dashboards;
                } else {
                    for (var i = 0; i < dashboards.length; i++) {
                        if (dashboards[i].name().toUpperCase().includes(keyword)) {
                            result.push(dashboards[i]);
                        }
                        if (dashboards[i].hasOwnProperty('children')) {
                            for (var j = 0; j < dashboards[i].children.length; j++) {
                                if (dashboards[i].children[j].name().toUpperCase().includes(keyword)) {
                                    result.push(dashboards[i].children[j]);
                                }
                                if (dashboards[i].children[j].hasOwnProperty('indicators')) {
                                    for (var k = 0; k < dashboards[i].children[j].indicators.length; k++) {
                                        if (dashboards[i].children[j].indicators[k].name().toUpperCase().includes(keyword)) {
                                            result.push(dashboards[i].children[j].indicators[k]);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                return result;
            };

            self.search = function() {
                if (self.keyword()) {
                    var result = getSearchResult(self.keyword().trim().toUpperCase());
                    console.log("result: " + result.length);
                    if (result.length == 0) {
                        //$("#icon-list-container").append("<span style=\"color: white;float: 100px;\">No Results</span>");
                        $("#icon-list-container .no-result").show();
                    } else {
                        $("#icon-list-container .no-result").hide();
                    }
                    this.items(result);

                } else {
                    $("#icon-list-container .no-result").hide();
                    this.items(dashboards);
                }
            };
            
           

            self.routing = function(data) {
                 localStorage.removeItem('indicatorCode');
                if (data.url && data.url !== '#') {
                    if (data.url.includes("http")) {
                        window.open(getMasdarSites());
                    } else {
                        var routeUrl = data.url;
                        
                      if (routeUrl.includes("img")) {
                          localStorage.setItem('indicatorCode',data.in_code);   
                          sessionStorage.setItem('report_param',JSON.stringify(data));
                          if(data.sa)
                              routeUrl='dynamicReport';
                          else
                              routeUrl='indicator-pro';
                    
                    }
                    
                        oj.Router.rootInstance.go(routeUrl);
                        parameter(data);
//                        filtrationValues(data.filtrationValues);
//                        metadata(data.metadata);
                    }
                }
            };

            self.openURL = function() {

                //                    var dashboard = getRow(name);
                //                    console.log("openURL " + dashboard.name);


                cordova.InAppBrowser.open('http://192.168.100.19:9502/xmlpserver/MetadataBIPReports/BIP/IndicatorMetadataReportEng.xdo?id=weblogic&passwd=welcome1&_xpf=&_xpt=0&_xmode=2&_paramsP_DSD_ROW_WID=1&_paramsP_INDICATOR_RO_WID=3908&_xautorun=true', '_blank', 'location=no');

            };

            self.shareURL = function(name) {
                var dashboard = getRow(name);
                console.log("Share " + dashboard.name);
                window.plugins.socialsharing.share(dashboard.name, null, null, dashboard.url);

            };
           
             self.tags = ko.observableArray([]);
            self.loadTags =   function(){

                var availableTags = [];
                self.tags(availableTags);

                for (var i = 0; i < dashboards.length; i++) {

                    availableTags.push({label:dashboards[i].name() , value:dashboards[i].name()});
                    if (dashboards[i].hasOwnProperty('children')) {
                        for (var j = 0; j < dashboards[i].children.length; j++) {
                            sb =dashboards[i].children[j];
                            availableTags.push({label:dashboards[i].children[j].name(),value:dashboards[i].children[j].name()});
                            if (dashboards[i].children[j].hasOwnProperty('indicators')) {
                                for (var k = 0; k < dashboards[i].children[j].indicators.length; k++) {
                                    availableTags.push({label:dashboards[i].children[j].indicators[k].name(),value:dashboards[i].children[j].indicators[k].name()});
                                }
                            }
                        }
                    }
                };
                
                self.tags(availableTags);
                self.refresh = false;
            };
            
            self.isSearchText = ko.pureComputed(function(){
                
                if(self.keyword().length > 0)
                return true;
            else{     
                if(document.getElementById("searchHome"))
                self.search2();
                
                return false;  
            }
            } );
            
  self.getLangContext = ko.computed(function(){
//                self.keyword('');
         
     if(document.getElementById("searchHome")){
                dashboards[0]['children'] =getKeyIndicatorJson();
                 self.loadTags();
                var ary= self.items();
                for(j=0;j<ary.length;j++){
                    if(ary[j].name_arb)
                      ary[j].name(model.lang()?ary[j].name_eng:ary[j].name_arb)  
                }
////                 self.keyword("");
////                  self.search2();
            }
         
                if(model.lang() ==='true')
                    return 'English';
                else
                    return 'Arabic';
            });
            
   
        self.tagsDataProvider = new oj.ArrayDataProvider(self.tags, {idAttribute: 'label'});
        self.searchTriggered = ko.observable();
        self.searchTerm = ko.observable();
        self.searchTimeStamp = ko.observable();
        self.loadTags();
       self.afterkeydown = function(data,event){
            console.log(event.originalEvent);
//            if(newVal)
//               $('#searchHome :input').val(newVal); 
       console.log( findItemFromResult(this.tags(),'label','$$$'));
         var txt = $('#searchHome :input').val();  
         if(txt !== ''){
               $('#searchHome :input').val(autoCorrect(findItemFromResult(this.tags(),'label','$$$'),txt));  
//                $('#search-button').trigger( "click" );
//                $('#search-button').trigger( "dblclick" );
            }
          
            if($('#searchHome :input').val() && $('#searchHome :input').val().length >0 && event.originalEvent.code !== "Enter")
            $('#moreSearch').show();
        else{           
            $('#moreSearch').hide();
        }

            };
           
        self.loadIndicators = function(){
            
//            var jsonUrl ='http://192.168.154.140:7005/MasdarRating/insert';
//            var jasonObj ={
//  "MasdarRating" : [ {
//    "userId" : "NOUSER",
//    "rating" : "7",
//    "ratingComments" : "string",
//    "attribute1" : "string",
//    "attribute2" : "string",
//    "creationDate" : "2008-09-29T04:49:45",
//    "updatedDate" : "2014-09-19T02:18:33"
//    
//  } ]
//};
//
//jasonObj ={
//  "MasdarAuditLog" : [ {
//    "userId" : "Anonymous",
//    "indicator" : "string",
//    "viewCount" : 10,
//    "rating" : "string",
//    "ratingComments" : "string",
//    "attribute1" : "string",
//    "attribute2" : "string",
//    "creationDate" : "2008-09-29T04:49:45",
//    "updatedDate" : "2014-09-19T02:18:33"
//  } ]
//};
//
//jsonUrl ='http://192.168.154.140:7005/MasdarAudit/insert';
            
//            doRestCall(jsonUrl, jasonObj, 'post', function(data){alert(data);}) ;


       
        doRestCall('https://json.geoiplookup.io/ip','','get', function(data){
            sessionStorage.setItem('DeviceIp',data.ip);
        }) ;
        
            getPopIndicatorJson(pop_indi);
            dashboards[0]['children'] =getKeyIndicatorJson(key_indi);
             self.loadTags();
             
              postbipCall('MobileIndicator_RPT',null,function(data){
                var jsonArray= getKeyIndicatorJson(data);
                dashboards[0]['children'] =jsonArray;
                 self.loadTags();
            }
                     );
         };
            self.loadIndicators();
            
        self.search2 = function (event) {    
//          var eventTime = getCurrentTime();
//          var trigger = event.type;
//          var term;         
//          
//          if (trigger === "ojValueUpdated") {
//            // search triggered from input field
//            // getting the search term from the ojValueUpdated event
//            term = event['detail']['value'];
//            trigger += " event";
//          } else { 
//            // search triggered from end slot
//            // getting the value from the element to use as the search term.
//            term = document.getElementById("search").value;
//            trigger = "click on search button";
//          }
//          alert(term);
// if (self.keyword().length == 1) 
//                self.keyword(self.keyword()[0]);
//            else
//                self.keyword('');
            
           $('#moreSearch').hide();
           if (self.keyword()) {
               if(self.keyword().length ===0)
                   self.keyword('');
                    var result = getSearchResult(self.keyword().trim().toUpperCase());
                    console.log("result: " + result.length);
                    if (result.length === 0) {
                        //$("#icon-list-container").append("<span style=\"color: white;float: 100px;\">No Results</span>");
                        $("#icon-list-container .no-result").show();
                    } else {
                        $("#icon-list-container .no-result").hide();
                    }
                    self.items(result);

                } else {
                    $("#icon-list-container .no-result").hide();
                    self.items(dashboards);
                }
          
//          self.searchTriggered("Search triggered by: " + trigger);
//          self.searchTerm("Search term: " + term);
//          self.searchTimeStamp("Last search fired at: " + eventTime);
        };


//            $(document).ready(function() {
////                loadTags();
//            });





            // Below are a subset of the ViewModel methods invoked by the ojModule binding
            // Please reference the ojModule jsDoc for additional available methods.

            /**
             * Optional ViewModel method invoked when this ViewModel is about to be
             * used for the View transition.  The application can put data fetch logic
             * here that can return a Promise which will delay the handleAttached function
             * call below until the Promise is resolved.
             * @param {Object} info - An object with the following key-value pairs:
             * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
             * @param {Function} info.valueAccessor - The binding's value accessor.
             * @return {Promise|undefined} - If the callback returns a Promise, the next phase (attaching DOM) will be delayed until
             * the promise is resolved
             */
            self.handleActivated = function(info) {
                // Implement if needed
            };

            /**
             * Optional ViewModel method invoked after the View is inserted into the
             * document DOM.  The application can put logic that requires the DOM being
             * attached here.
             * @param {Object} info - An object with the following key-value pairs:
             * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
             * @param {Function} info.valueAccessor - The binding's value accessor.
             * @param {boolean} info.fromCache - A boolean indicating whether the module was retrieved from cache.
             */
            self.handleAttached = function(info) {
                localStorage.setItem('firstTimeUser', false);
                // Implement if needed
            };


            /**
             * Optional ViewModel method invoked after the bindings are applied on this View. 
             * If the current View is retrieved from cache, the bindings will not be re-applied
             * and this callback will not be invoked.
             * @param {Object} info - An object with the following key-value pairs:
             * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
             * @param {Function} info.valueAccessor - The binding's value accessor.
             */
            self.handleBindingsApplied = function(info) {
                // Implement if needed
            };

            /*
             * Optional ViewModel method invoked after the View is removed from the
             * document DOM.
             * @param {Object} info - An object with the following key-value pairs:
             * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
             * @param {Function} info.valueAccessor - The binding's value accessor.
             * @param {Array} info.cachedNodes - An Array containing cached nodes for the View if the cache is enabled.
             */
            self.handleDetached = function(info) {
                // Implement if needed
            };

        }
        
        
//         self.tags = [
//            { value: ".net", label: ".net" },
//            { value: "Accounting", label: "Accounting" },
//            { value: "ADE", label: "ADE" },
//            { value: "Adf", label: "Adf" },
//            { value: "Adfc", label: "Adfc" },
//            { value: "Adfm", label: "Adfm" }
//
//        ];
        
        
        function getCurrentTime() {
          var date = new Date();
          return date.getHours() + ":" + date.getMinutes() 
                  + ":" + date.getSeconds() + "." + date.getMilliseconds();
        }
        
        
        

        //            $(document).ready(function () {
        //                ready = 1;
        //                console.log("ready!!!");
        //            });

        /*
         * Returns a constructor for the ViewModel so that the ViewModel is constructed
         * each time the view is displayed.  Return an instance of the ViewModel if
         * only one instance of the ViewModel is needed.
         */
        return new DashboardViewModel();
    }
);