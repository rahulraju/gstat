/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * editProfile module
 */
define(['ojs/ojcore', 'knockout', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojavatar',
    'ojs/ojslider', 'ojs/ojselectcombobox', 'ojs/ojfilepicker', 'ojs/ojbutton'
], function (oj, ko) {
    /**
     * The view model for the main content view template
     */
    function editProfileContentViewModel() {
        var self = this;
        self.fileNames = ko.observableArray([]);

        self.imgURL = ko.observable(profilePic());
        self.firstName = ko.observable(firstName());
        self.lastName = ko.observable(lastName());
        self.email = ko.observable(email());
        self.selectListener = function (event) {
            var files = event.detail.files;
            for (var i = 0; i < files.length; i++) {
                self.fileNames.push(files[i].name);
                //alert(window.URL.createObjectURL(files[i]));
                
                self.imgURL(window.URL.createObjectURL(files[i]));

            }
        };

        self.submit = function () {
            email(self.email());
            lastName(self.lastName());
            firstName(self.firstName());
            profilePic(self.imgURL());
        };
    }

    return editProfileContentViewModel;
});
