/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * stat-map module
 */
define(['ojs/ojcore', 'knockout', 'jquery',
    'text!../properties/saudiArabia.json',
    'text!../properties/world_countries.json', 'text!../properties/SaudiMap_data.json','text!../properties/SaudiMap_data','ojs/ojarraytabledatasource', 'ojs/ojcheckboxset','masdar_util',
    'appController','ojs/ojbutton', 'ojs/ojthematicmap', 'ojs/ojlegend', 'ojs/ojselectcombobox', 'ojs/ojlabel', 'ojs/ojtable', 'ojs/ojarraydataprovider'],
        function (oj, ko, $, asia, world, saudiMapDataJson,saudiMapData2) {
            /**
             * The view model for the main content view template
             */
            function statmapContentViewModel() {

                var self = this;
                self.map = ko.observable('asia');
                self.areaData = ko.observableArray();
                self.mapProvider = ko.observable();
                self.region = ko.observable('');
                self.selYear =ko.observable('2017');
                self.selIn =ko.observable('');                
                self.ViewType=ko.observable('Indicator');


                updateMap(this.map());
//                              var suudData = getBipJson(saudiDatas);  
                var suudData = [{
        "REGION_ARB": "الرياض",
        "ID": 9,
        "REGION_ENG": "Al-Riyadh",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2017",
        "VALUE": "353733"
    }, {
        "REGION_ARB": "مكة المكرمة",
        "ID": 10,
        "REGION_ENG": "Makkah Al-Mokarramah",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2017",
        "VALUE": "266647"
    }, {
        "REGION_ARB": "المدينة المنورة",
        "ID": 11,
        "REGION_ENG": "Al-Madinah Al-Monawarah",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2017",
        "VALUE": "66544"
    }, {
        "REGION_ARB": "القصيم",
        "ID": 12,
        "REGION_ENG": "Al-Qaseem",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2017",
        "VALUE": "92063"
    }, {
        "REGION_ARB": "الشرقية",
        "ID": 13,
        "REGION_ENG": "Eastern Region",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2017",
        "VALUE": "192505"
    }, {
        "REGION_ARB": "عسير",
        "ID": 14,
        "REGION_ENG": "Aseer",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2017",
        "VALUE": "70139"
    }, {
        "REGION_ARB": "تبوك",
        "ID": 15,
        "REGION_ENG": "Tabouk",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2017",
        "VALUE": "29288"
    }, {
        "REGION_ARB": "حائل",
        "ID": 16,
        "REGION_ENG": "Hail",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2017",
        "VALUE": "30512"
    }, {
        "REGION_ARB": "الحدود الشمالية",
        "ID": 17,
        "REGION_ENG": "Northern Borders",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2017",
        "VALUE": "17305"
    }, {
        "REGION_ARB": "جازان",
        "ID": 18,
        "REGION_ENG": "Jazan",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2017",
        "VALUE": "34063"
    }, {
        "REGION_ARB": "نجران",
        "ID": 19,
        "REGION_ENG": "Najran",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2017",
        "VALUE": "25163"
    }, {
        "REGION_ARB": "الباحة",
        "ID": 20,
        "REGION_ENG": "Al-Baha",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2017",
        "VALUE": "12664"
    }, {
        "REGION_ARB": "الجوف",
        "ID": 21,
        "REGION_ENG": "Al-Jouf",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2017",
        "VALUE": "11666"
    }, {
        "REGION_ARB": "المجموع",
        "ID": 22,
        "REGION_ENG": "Total",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2017",
        "VALUE": "1202292"
    }, {
        "REGION_ARB": "الرياض",
        "ID": 23,
        "REGION_ENG": "Al-Riyadh",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2016",
        "VALUE": "353966"
    }, {
        "REGION_ARB": "مكة المكرمة",
        "ID": 24,
        "REGION_ENG": "Makkah Al-Mokarramah",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2016",
        "VALUE": "272042"
    }, {
        "REGION_ARB": "المدينة المنورة",
        "ID": 25,
        "REGION_ENG": "Al-Madinah Al-Monawarah",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2016",
        "VALUE": "68264"
    }, {
        "REGION_ARB": "القصيم",
        "ID": 26,
        "REGION_ENG": "Al-Qaseem",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2016",
        "VALUE": "86899"
    }, {
        "REGION_ARB": "الشرقية",
        "ID": 27,
        "REGION_ENG": "Eastern Region",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2016",
        "VALUE": "194066"
    }, {
        "REGION_ARB": "عسير",
        "ID": 28,
        "REGION_ENG": "Aseer",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2016",
        "VALUE": "71839"
    }, {
        "REGION_ARB": "تبوك",
        "ID": 29,
        "REGION_ENG": "Tabouk",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2016",
        "VALUE": "29229"
    }, {
        "REGION_ARB": "حائل",
        "ID": 30,
        "REGION_ENG": "Hail",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2016",
        "VALUE": "30281"
    }, {
        "REGION_ARB": "الحدود الشمالية",
        "ID": 31,
        "REGION_ENG": "Northern Borders",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2016",
        "VALUE": "17049"
    }, {
        "REGION_ARB": "جازان",
        "ID": 32,
        "REGION_ENG": "Jazan",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2016",
        "VALUE": "30857"
    }, {
        "REGION_ARB": "نجران",
        "ID": 33,
        "REGION_ENG": "Najran",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2016",
        "VALUE": "24372"
    }, {
        "REGION_ARB": "الباحة",
        "ID": 34,
        "REGION_ENG": "Al-Baha",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2016",
        "VALUE": "12836"
    }, {
        "REGION_ARB": "الجوف",
        "ID": 35,
        "REGION_ENG": "Al-Jouf",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2016",
        "VALUE": "12057"
    }, {
        "REGION_ARB": "المجموع",
        "ID": 36,
        "REGION_ENG": "Total",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2016",
        "VALUE": "1203757"
    }, {
        "REGION_ARB": "الرياض",
        "ID": 37,
        "REGION_ENG": "Al-Riyadh",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2015",
        "VALUE": "500898"
    }, {
        "REGION_ARB": "مكة المكرمة",
        "ID": 38,
        "REGION_ENG": "Makkah Al-Mokarramah",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2015",
        "VALUE": "397192"
    }, {
        "REGION_ARB": "المدينة المنورة",
        "ID": 39,
        "REGION_ENG": "Al-Madinah Al-Monawarah",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2015",
        "VALUE": "102057"
    }, {
        "REGION_ARB": "القصيم",
        "ID": 40,
        "REGION_ENG": "Al-Qaseem",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2015",
        "VALUE": "121167"
    }, {
        "REGION_ARB": "الشرقية",
        "ID": 41,
        "REGION_ENG": "Eastern Region",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2015",
        "VALUE": "294636"
    }, {
        "REGION_ARB": "عسير",
        "ID": 42,
        "REGION_ENG": "Aseer",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2015",
        "VALUE": "89064"
    }, {
        "REGION_ARB": "تبوك",
        "ID": 43,
        "REGION_ENG": "Tabouk",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2015",
        "VALUE": "42307"
    }, {
        "REGION_ARB": "حائل",
        "ID": 44,
        "REGION_ENG": "Hail",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2015",
        "VALUE": "56004"
    }, {
        "REGION_ARB": "الحدود الشمالية",
        "ID": 45,
        "REGION_ENG": "Northern Borders",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2015",
        "VALUE": "26632"
    }, {
        "REGION_ARB": "جازان",
        "ID": 46,
        "REGION_ENG": "Jazan",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2015",
        "VALUE": "35667"
    }, {
        "REGION_ARB": "نجران",
        "ID": 47,
        "REGION_ENG": "Najran",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2015",
        "VALUE": "34052"
    }, {
        "REGION_ARB": "الباحة",
        "ID": 48,
        "REGION_ENG": "Al-Baha",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2015",
        "VALUE": "18467"
    }, {
        "REGION_ARB": "الجوف",
        "ID": 49,
        "REGION_ENG": "Al-Jouf",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2015",
        "VALUE": "24326"
    }, {
        "REGION_ARB": "المجموع",
        "ID": 50,
        "REGION_ENG": "Total",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2015",
        "VALUE": "1742469"
    }, {
        "REGION_ARB": "الرياض",
        "ID": 51,
        "REGION_ENG": "Al-Riyadh",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2014",
        "VALUE": "444905"
    }, {
        "REGION_ARB": "مكة المكرمة",
        "ID": 52,
        "REGION_ENG": "Makkah Al-Mokarramah",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2014",
        "VALUE": "352475"
    }, {
        "REGION_ARB": "المدينة المنورة",
        "ID": 53,
        "REGION_ENG": "Al-Madinah Al-Monawarah",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2014",
        "VALUE": "87306"
    }, {
        "REGION_ARB": "القصيم",
        "ID": 54,
        "REGION_ENG": "Al-Qaseem",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2014",
        "VALUE": "98214"
    }, {
        "REGION_ARB": "الشرقية",
        "ID": 55,
        "REGION_ENG": "Eastern Region",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2014",
        "VALUE": "262694"
    }, {
        "REGION_ARB": "عسير",
        "ID": 56,
        "REGION_ENG": "Aseer",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2014",
        "VALUE": "75604"
    }, {
        "REGION_ARB": "تبوك",
        "ID": 57,
        "REGION_ENG": "Tabouk",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2014",
        "VALUE": "35769"
    }, {
        "REGION_ARB": "حائل",
        "ID": 58,
        "REGION_ENG": "Hail",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2014",
        "VALUE": "49584"
    }, {
        "REGION_ARB": "الحدود الشمالية",
        "ID": 59,
        "REGION_ENG": "Northern Borders",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2014",
        "VALUE": "22938"
    }, {
        "REGION_ARB": "جازان",
        "ID": 60,
        "REGION_ENG": "Jazan",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2014",
        "VALUE": "28845"
    }, {
        "REGION_ARB": "نجران",
        "ID": 61,
        "REGION_ENG": "Najran",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2014",
        "VALUE": "28556"
    }, {
        "REGION_ARB": "الباحة",
        "ID": 62,
        "REGION_ENG": "Al-Baha",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2014",
        "VALUE": "16073"
    }, {
        "REGION_ARB": "الجوف",
        "ID": 63,
        "REGION_ENG": "Al-Jouf",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2014",
        "VALUE": "21054"
    }, {
        "REGION_ARB": "المجموع",
        "ID": 64,
        "REGION_ENG": "Total",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2014",
        "VALUE": "1524017"
    }, {
        "REGION_ARB": "الرياض",
        "ID": 65,
        "REGION_ENG": "Al-Riyadh",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2013",
        "VALUE": "404336"
    }, {
        "REGION_ARB": "مكة المكرمة",
        "ID": 66,
        "REGION_ENG": "Makkah Al-Mokarramah",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2013",
        "VALUE": "317681"
    }, {
        "REGION_ARB": "المدينة المنورة",
        "ID": 67,
        "REGION_ENG": "Al-Madinah Al-Monawarah",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2013",
        "VALUE": "76502"
    }, {
        "REGION_ARB": "القصيم",
        "ID": 68,
        "REGION_ENG": "Al-Qaseem",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2013",
        "VALUE": "88775"
    }, {
        "REGION_ARB": "الشرقية",
        "ID": 69,
        "REGION_ENG": "Eastern Region",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2013",
        "VALUE": "238016"
    }, {
        "REGION_ARB": "عسير",
        "ID": 70,
        "REGION_ENG": "Aseer",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2013",
        "VALUE": "63908"
    }, {
        "REGION_ARB": "تبوك",
        "ID": 71,
        "REGION_ENG": "Tabouk",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2013",
        "VALUE": "31144"
    }, {
        "REGION_ARB": "حائل",
        "ID": 72,
        "REGION_ENG": "Hail",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2013",
        "VALUE": "46210"
    }, {
        "REGION_ARB": "الحدود الشمالية",
        "ID": 73,
        "REGION_ENG": "Northern Borders",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2013",
        "VALUE": "20579"
    }, {
        "REGION_ARB": "جازان",
        "ID": 74,
        "REGION_ENG": "Jazan",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2013",
        "VALUE": "24169"
    }, {
        "REGION_ARB": "نجران",
        "ID": 75,
        "REGION_ENG": "Najran",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2013",
        "VALUE": "25113"
    }, {
        "REGION_ARB": "الباحة",
        "ID": 76,
        "REGION_ENG": "Al-Baha",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2013",
        "VALUE": "13866"
    }, {
        "REGION_ARB": "الجوف",
        "ID": 77,
        "REGION_ENG": "Al-Jouf",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2013",
        "VALUE": "19369"
    }, {
        "REGION_ARB": "المجموع",
        "ID": 78,
        "REGION_ENG": "Total",
        "INDI_ARB": "السجلات التجارية القائمة",
        "INDI_ENG": "Commercial Registrations",
        "YEAR": "2013",
        "VALUE": "1369668"
    }, {
        "REGION_ARB": "الرياض",
        "ID": 79,
        "REGION_ENG": "Al-Riyadh",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2017",
        "VALUE": "127674"
    }, {
        "REGION_ARB": "مكة المكرمة",
        "ID": 80,
        "REGION_ENG": "Makkah Al-Mokarramah",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2017",
        "VALUE": "103688"
    }, {
        "REGION_ARB": "المدينة المنورة",
        "ID": 81,
        "REGION_ENG": "Al-Madinah Al-Monawarah",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2017",
        "VALUE": "29173"
    }, {
        "REGION_ARB": "القصيم",
        "ID": 82,
        "REGION_ENG": "Al-Qaseem",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2017",
        "VALUE": "34656"
    }, {
        "REGION_ARB": "الشرقية",
        "ID": 83,
        "REGION_ENG": "Eastern Region",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2017",
        "VALUE": "74887"
    }, {
        "REGION_ARB": "عسير",
        "ID": 84,
        "REGION_ENG": "Aseer",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2017",
        "VALUE": "29068"
    }, {
        "REGION_ARB": "تبوك",
        "ID": 85,
        "REGION_ENG": "Tabouk",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2017",
        "VALUE": "11137"
    }, {
        "REGION_ARB": "حائل",
        "ID": 86,
        "REGION_ENG": "Hail",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2017",
        "VALUE": "12326"
    }, {
        "REGION_ARB": "الحدود الشمالية",
        "ID": 87,
        "REGION_ENG": "Northern Borders",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2017",
        "VALUE": "4795"
    }, {
        "REGION_ARB": "جازان",
        "ID": 88,
        "REGION_ENG": "Jazan",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2017",
        "VALUE": "10066"
    }, {
        "REGION_ARB": "نجران",
        "ID": 89,
        "REGION_ENG": "Najran",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2017",
        "VALUE": "11852"
    }, {
        "REGION_ARB": "الباحة",
        "ID": 90,
        "REGION_ENG": "Al-Baha",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2017",
        "VALUE": "5608"
    }, {
        "REGION_ARB": "الجوف",
        "ID": 91,
        "REGION_ENG": "Al-Jouf",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2017",
        "VALUE": "7256"
    }, {
        "REGION_ARB": "المجموع",
        "ID": 92,
        "REGION_ENG": "Total",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2017",
        "VALUE": "462186"
    }, {
        "REGION_ARB": "الرياض",
        "ID": 93,
        "REGION_ENG": "Al-Riyadh",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2016",
        "VALUE": "124305"
    }, {
        "REGION_ARB": "مكة المكرمة",
        "ID": 94,
        "REGION_ENG": "Makkah Al-Mokarramah",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2016",
        "VALUE": "101208"
    }, {
        "REGION_ARB": "المدينة المنورة",
        "ID": 95,
        "REGION_ENG": "Al-Madinah Al-Monawarah",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2016",
        "VALUE": "28966"
    }, {
        "REGION_ARB": "القصيم",
        "ID": 96,
        "REGION_ENG": "Al-Qaseem",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2016",
        "VALUE": "34067"
    }, {
        "REGION_ARB": "الشرقية",
        "ID": 97,
        "REGION_ENG": "Eastern Region",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2016",
        "VALUE": "74267"
    }, {
        "REGION_ARB": "عسير",
        "ID": 98,
        "REGION_ENG": "Aseer",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2016",
        "VALUE": "29036"
    }, {
        "REGION_ARB": "تبوك",
        "ID": 99,
        "REGION_ENG": "Tabouk",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2016",
        "VALUE": "10925"
    }, {
        "REGION_ARB": "حائل",
        "ID": 100,
        "REGION_ENG": "Hail",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2016",
        "VALUE": "12038"
    }, {
        "REGION_ARB": "الحدود الشمالية",
        "ID": 101,
        "REGION_ENG": "Northern Borders",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2016",
        "VALUE": "4615"
    }, {
        "REGION_ARB": "جازان",
        "ID": 102,
        "REGION_ENG": "Jazan",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2016",
        "VALUE": "9613"
    }, {
        "REGION_ARB": "نجران",
        "ID": 103,
        "REGION_ENG": "Najran",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2016",
        "VALUE": "11459"
    }, {
        "REGION_ARB": "الباحة",
        "ID": 104,
        "REGION_ENG": "Al-Baha",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2016",
        "VALUE": "5644"
    }, {
        "REGION_ARB": "الجوف",
        "ID": 105,
        "REGION_ENG": "Al-Jouf",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2016",
        "VALUE": "7246"
    }, {
        "REGION_ARB": "المجموع",
        "ID": 106,
        "REGION_ENG": "Total",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2016",
        "VALUE": "453389"
    }, {
        "REGION_ARB": "الرياض",
        "ID": 107,
        "REGION_ENG": "Al-Riyadh",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2015",
        "VALUE": "115338"
    }, {
        "REGION_ARB": "مكة المكرمة",
        "ID": 108,
        "REGION_ENG": "Makkah Al-Mokarramah",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2015",
        "VALUE": "94794"
    }, {
        "REGION_ARB": "المدينة المنورة",
        "ID": 109,
        "REGION_ENG": "Al-Madinah Al-Monawarah",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2015",
        "VALUE": "26707"
    }, {
        "REGION_ARB": "القصيم",
        "ID": 110,
        "REGION_ENG": "Al-Qaseem",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2015",
        "VALUE": "30656"
    }, {
        "REGION_ARB": "الشرقية",
        "ID": 111,
        "REGION_ENG": "Eastern Region",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2015",
        "VALUE": "70454"
    }, {
        "REGION_ARB": "عسير",
        "ID": 112,
        "REGION_ENG": "Aseer",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2015",
        "VALUE": "27128"
    }, {
        "REGION_ARB": "تبوك",
        "ID": 113,
        "REGION_ENG": "Tabouk",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2015",
        "VALUE": "9778"
    }, {
        "REGION_ARB": "حائل",
        "ID": 114,
        "REGION_ENG": "Hail",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2015",
        "VALUE": "10887"
    }, {
        "REGION_ARB": "الحدود الشمالية",
        "ID": 115,
        "REGION_ENG": "Northern Borders",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2015",
        "VALUE": "4114"
    }, {
        "REGION_ARB": "جازان",
        "ID": 116,
        "REGION_ENG": "Jazan",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2015",
        "VALUE": "8664"
    }, {
        "REGION_ARB": "نجران",
        "ID": 117,
        "REGION_ENG": "Najran",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2015",
        "VALUE": "10513"
    }, {
        "REGION_ARB": "الباحة",
        "ID": 118,
        "REGION_ENG": "Al-Baha",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2015",
        "VALUE": "5262"
    }, {
        "REGION_ARB": "الجوف",
        "ID": 119,
        "REGION_ENG": "Al-Jouf",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2015",
        "VALUE": "6646"
    }, {
        "REGION_ARB": "المجموع",
        "ID": 120,
        "REGION_ENG": "Total",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2015",
        "VALUE": "420941"
    }, {
        "REGION_ARB": "الرياض",
        "ID": 121,
        "REGION_ENG": "Al-Riyadh",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2014",
        "VALUE": "91145"
    }, {
        "REGION_ARB": "مكة المكرمة",
        "ID": 122,
        "REGION_ENG": "Makkah Al-Mokarramah",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2014",
        "VALUE": "23797"
    }, {
        "REGION_ARB": "المدينة المنورة",
        "ID": 123,
        "REGION_ENG": "Al-Madinah Al-Monawarah",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2014",
        "VALUE": "19150"
    }, {
        "REGION_ARB": "القصيم",
        "ID": 124,
        "REGION_ENG": "Al-Qaseem",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2014",
        "VALUE": "28057"
    }, {
        "REGION_ARB": "الشرقية",
        "ID": 125,
        "REGION_ENG": "Eastern Region",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2014",
        "VALUE": "40439"
    }, {
        "REGION_ARB": "عسير",
        "ID": 126,
        "REGION_ENG": "Aseer",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2014",
        "VALUE": "20079"
    }, {
        "REGION_ARB": "تبوك",
        "ID": 127,
        "REGION_ENG": "Tabouk",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2014",
        "VALUE": "8923"
    }, {
        "REGION_ARB": "حائل",
        "ID": 128,
        "REGION_ENG": "Hail",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2014",
        "VALUE": "10599"
    }, {
        "REGION_ARB": "الحدود الشمالية",
        "ID": 129,
        "REGION_ENG": "Northern Borders",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2014",
        "VALUE": "3881"
    }, {
        "REGION_ARB": "جازان",
        "ID": 130,
        "REGION_ENG": "Jazan",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2014",
        "VALUE": "8131"
    }, {
        "REGION_ARB": "نجران",
        "ID": 131,
        "REGION_ENG": "Najran",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2014",
        "VALUE": "9832"
    }, {
        "REGION_ARB": "الباحة",
        "ID": 132,
        "REGION_ENG": "Al-Baha",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2014",
        "VALUE": "5010"
    }, {
        "REGION_ARB": "الجوف",
        "ID": 133,
        "REGION_ENG": "Al-Jouf",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2014",
        "VALUE": "6318"
    }, {
        "REGION_ARB": "المجموع",
        "ID": 134,
        "REGION_ENG": "Total",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2014",
        "VALUE": "275361"
    }, {
        "REGION_ARB": "الرياض",
        "ID": 135,
        "REGION_ENG": "Al-Riyadh",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2013",
        "VALUE": "110028"
    }, {
        "REGION_ARB": "مكة المكرمة",
        "ID": 136,
        "REGION_ENG": "Makkah Al-Mokarramah",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2013",
        "VALUE": "89280"
    }, {
        "REGION_ARB": "المدينة المنورة",
        "ID": 137,
        "REGION_ENG": "Al-Madinah Al-Monawarah",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2013",
        "VALUE": "25989"
    }, {
        "REGION_ARB": "القصيم",
        "ID": 138,
        "REGION_ENG": "Al-Qaseem",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2013",
        "VALUE": "34133"
    }, {
        "REGION_ARB": "الشرقية",
        "ID": 139,
        "REGION_ENG": "Eastern Region",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2013",
        "VALUE": "75938"
    }, {
        "REGION_ARB": "عسير",
        "ID": 140,
        "REGION_ENG": "Aseer",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2013",
        "VALUE": "25746"
    }, {
        "REGION_ARB": "تبوك",
        "ID": 141,
        "REGION_ENG": "Tabouk",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2013",
        "VALUE": "8935"
    }, {
        "REGION_ARB": "حائل",
        "ID": 142,
        "REGION_ENG": "Hail",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2013",
        "VALUE": "13101"
    }, {
        "REGION_ARB": "الحدود الشمالية",
        "ID": 143,
        "REGION_ENG": "Northern Borders",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2013",
        "VALUE": "4560"
    }, {
        "REGION_ARB": "جازان",
        "ID": 144,
        "REGION_ENG": "Jazan",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2013",
        "VALUE": "8257"
    }, {
        "REGION_ARB": "نجران",
        "ID": 145,
        "REGION_ENG": "Najran",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2013",
        "VALUE": "11458"
    }, {
        "REGION_ARB": "الباحة",
        "ID": 146,
        "REGION_ENG": "Al-Baha",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2013",
        "VALUE": "5472"
    }, {
        "REGION_ARB": "الجوف",
        "ID": 147,
        "REGION_ENG": "Al-Jouf",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2013",
        "VALUE": "6588"
    }, {
        "REGION_ARB": "المجموع",
        "ID": 148,
        "REGION_ENG": "Total",
        "INDI_ARB": "المنشآت الاقتصادية",
        "INDI_ENG": "Establishments",
        "YEAR": "2013",
        "VALUE": "419485"
    }, {
        "REGION_ARB": "الرياض",
        "ID": 149,
        "REGION_ENG": "Al-Riyadh",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2017",
        "VALUE": "19907"
    }, {
        "REGION_ARB": "مكة المكرمة",
        "ID": 150,
        "REGION_ENG": "Makkah Al-Mokarramah",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2017",
        "VALUE": "21083"
    }, {
        "REGION_ARB": "المدينة المنورة",
        "ID": 151,
        "REGION_ENG": "Al-Madinah Al-Monawarah",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2017",
        "VALUE": "5338"
    }, {
        "REGION_ARB": "القصيم",
        "ID": 152,
        "REGION_ENG": "Al-Qaseem",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2017",
        "VALUE": "6432"
    }, {
        "REGION_ARB": "الشرقية",
        "ID": 153,
        "REGION_ENG": "Eastern Region",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2017",
        "VALUE": "18660"
    }, {
        "REGION_ARB": "عسير",
        "ID": 154,
        "REGION_ENG": "Aseer",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2017",
        "VALUE": "4178"
    }, {
        "REGION_ARB": "تبوك",
        "ID": 155,
        "REGION_ENG": "Tabouk",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2017",
        "VALUE": "3204"
    }, {
        "REGION_ARB": "حائل",
        "ID": 156,
        "REGION_ENG": "Hail",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2017",
        "VALUE": "2426"
    }, {
        "REGION_ARB": "الحدود الشمالية",
        "ID": 157,
        "REGION_ENG": "Northern Borders",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2017",
        "VALUE": "1781"
    }, {
        "REGION_ARB": "جازان",
        "ID": 158,
        "REGION_ENG": "Jazan",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2017",
        "VALUE": "2171"
    }, {
        "REGION_ARB": "نجران",
        "ID": 159,
        "REGION_ENG": "Najran",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2017",
        "VALUE": "2509"
    }, {
        "REGION_ARB": "الباحة",
        "ID": 160,
        "REGION_ENG": "Al-Baha",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2017",
        "VALUE": "800"
    }, {
        "REGION_ARB": "الجوف",
        "ID": 161,
        "REGION_ENG": "Al-Jouf",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2017",
        "VALUE": "854"
    }, {
        "REGION_ARB": "المجموع",
        "ID": 162,
        "REGION_ENG": "Total",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2017",
        "VALUE": "89343"
    }, {
        "REGION_ARB": "الرياض",
        "ID": 163,
        "REGION_ENG": "Al-Riyadh",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2016",
        "VALUE": "10958"
    }, {
        "REGION_ARB": "مكة المكرمة",
        "ID": 164,
        "REGION_ENG": "Makkah Al-Mokarramah",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2016",
        "VALUE": "9931"
    }, {
        "REGION_ARB": "المدينة المنورة",
        "ID": 165,
        "REGION_ENG": "Al-Madinah Al-Monawarah",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2016",
        "VALUE": "2982"
    }, {
        "REGION_ARB": "القصيم",
        "ID": 166,
        "REGION_ENG": "Al-Qaseem",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2016",
        "VALUE": "3402"
    }, {
        "REGION_ARB": "الشرقية",
        "ID": 167,
        "REGION_ENG": "Eastern Region",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2016",
        "VALUE": "10389"
    }, {
        "REGION_ARB": "عسير",
        "ID": 168,
        "REGION_ENG": "Aseer",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2016",
        "VALUE": "1486"
    }, {
        "REGION_ARB": "تبوك",
        "ID": 169,
        "REGION_ENG": "Tabouk",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2016",
        "VALUE": "1361"
    }, {
        "REGION_ARB": "حائل",
        "ID": 170,
        "REGION_ENG": "Hail",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2016",
        "VALUE": "1893"
    }, {
        "REGION_ARB": "الحدود الشمالية",
        "ID": 171,
        "REGION_ENG": "Northern Borders",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2016",
        "VALUE": "1006"
    }, {
        "REGION_ARB": "جازان",
        "ID": 172,
        "REGION_ENG": "Jazan",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2016",
        "VALUE": "1192"
    }, {
        "REGION_ARB": "نجران",
        "ID": 173,
        "REGION_ENG": "Najran",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2016",
        "VALUE": "1371"
    }, {
        "REGION_ARB": "الباحة",
        "ID": 174,
        "REGION_ENG": "Al-Baha",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2016",
        "VALUE": "629"
    }, {
        "REGION_ARB": "الجوف",
        "ID": 175,
        "REGION_ENG": "Al-Jouf",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2016",
        "VALUE": "473"
    }, {
        "REGION_ARB": "المجموع",
        "ID": 176,
        "REGION_ENG": "Total",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2016",
        "VALUE": "47073"
    }, {
        "REGION_ARB": "الرياض",
        "ID": 177,
        "REGION_ENG": "Al-Riyadh",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2015",
        "VALUE": "103540"
    }, {
        "REGION_ARB": "مكة المكرمة",
        "ID": 178,
        "REGION_ENG": "Makkah Al-Mokarramah",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2015",
        "VALUE": "85244"
    }, {
        "REGION_ARB": "المدينة المنورة",
        "ID": 179,
        "REGION_ENG": "Al-Madinah Al-Monawarah",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2015",
        "VALUE": "19193"
    }, {
        "REGION_ARB": "القصيم",
        "ID": 180,
        "REGION_ENG": "Al-Qaseem",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2015",
        "VALUE": "20449"
    }, {
        "REGION_ARB": "الشرقية",
        "ID": 181,
        "REGION_ENG": "Eastern Region",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2015",
        "VALUE": "56799"
    }, {
        "REGION_ARB": "عسير",
        "ID": 182,
        "REGION_ENG": "Aseer",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2015",
        "VALUE": "13838"
    }, {
        "REGION_ARB": "تبوك",
        "ID": 183,
        "REGION_ENG": "Tabouk",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2015",
        "VALUE": "10621"
    }, {
        "REGION_ARB": "حائل",
        "ID": 184,
        "REGION_ENG": "Hail",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2015",
        "VALUE": "13295"
    }, {
        "REGION_ARB": "الحدود الشمالية",
        "ID": 185,
        "REGION_ENG": "Northern Borders",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2015",
        "VALUE": "8861"
    }, {
        "REGION_ARB": "جازان",
        "ID": 186,
        "REGION_ENG": "Jazan",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2015",
        "VALUE": "6568"
    }, {
        "REGION_ARB": "نجران",
        "ID": 187,
        "REGION_ENG": "Najran",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2015",
        "VALUE": "6150"
    }, {
        "REGION_ARB": "الباحة",
        "ID": 188,
        "REGION_ENG": "Al-Baha",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2015",
        "VALUE": "4172"
    }, {
        "REGION_ARB": "الجوف",
        "ID": 189,
        "REGION_ENG": "Al-Jouf",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2015",
        "VALUE": "5696"
    }, {
        "REGION_ARB": "المجموع",
        "ID": 190,
        "REGION_ENG": "Total",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2015",
        "VALUE": "354426"
    }, {
        "REGION_ARB": "الرياض",
        "ID": 191,
        "REGION_ENG": "Al-Riyadh",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2014",
        "VALUE": "28288"
    }, {
        "REGION_ARB": "مكة المكرمة",
        "ID": 192,
        "REGION_ENG": "Makkah Al-Mokarramah",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2014",
        "VALUE": "24343"
    }, {
        "REGION_ARB": "المدينة المنورة",
        "ID": 193,
        "REGION_ENG": "Al-Madinah Al-Monawarah",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2014",
        "VALUE": "8076"
    }, {
        "REGION_ARB": "القصيم",
        "ID": 194,
        "REGION_ENG": "Al-Qaseem",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2014",
        "VALUE": "11829"
    }, {
        "REGION_ARB": "الشرقية",
        "ID": 195,
        "REGION_ENG": "Eastern Region",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2014",
        "VALUE": "23903"
    }, {
        "REGION_ARB": "عسير",
        "ID": 196,
        "REGION_ENG": "Aseer",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2014",
        "VALUE": "5373"
    }, {
        "REGION_ARB": "تبوك",
        "ID": 197,
        "REGION_ENG": "Tabouk",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2014",
        "VALUE": "2799"
    }, {
        "REGION_ARB": "حائل",
        "ID": 198,
        "REGION_ENG": "Hail",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2014",
        "VALUE": "7707"
    }, {
        "REGION_ARB": "الحدود الشمالية",
        "ID": 199,
        "REGION_ENG": "Northern Borders",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2014",
        "VALUE": "3375"
    }, {
        "REGION_ARB": "جازان",
        "ID": 200,
        "REGION_ENG": "Jazan",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2014",
        "VALUE": "2182"
    }, {
        "REGION_ARB": "نجران",
        "ID": 201,
        "REGION_ENG": "Najran",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2014",
        "VALUE": "3904"
    }, {
        "REGION_ARB": "الباحة",
        "ID": 202,
        "REGION_ENG": "Al-Baha",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2014",
        "VALUE": "1192"
    }, {
        "REGION_ARB": "الجوف",
        "ID": 203,
        "REGION_ENG": "Al-Jouf",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2014",
        "VALUE": "1937"
    }, {
        "REGION_ARB": "المجموع",
        "ID": 204,
        "REGION_ENG": "Total",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2014",
        "VALUE": "124908"
    }, {
        "REGION_ARB": "الرياض",
        "ID": 205,
        "REGION_ENG": "Al-Riyadh",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2013",
        "VALUE": "16129"
    }, {
        "REGION_ARB": "مكة المكرمة",
        "ID": 206,
        "REGION_ENG": "Makkah Al-Mokarramah",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2013",
        "VALUE": "14197"
    }, {
        "REGION_ARB": "المدينة المنورة",
        "ID": 207,
        "REGION_ENG": "Al-Madinah Al-Monawarah",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2013",
        "VALUE": "4927"
    }, {
        "REGION_ARB": "القصيم",
        "ID": 208,
        "REGION_ENG": "Al-Qaseem",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2013",
        "VALUE": "7392"
    }, {
        "REGION_ARB": "الشرقية",
        "ID": 209,
        "REGION_ENG": "Eastern Region",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2013",
        "VALUE": "14399"
    }, {
        "REGION_ARB": "عسير",
        "ID": 210,
        "REGION_ENG": "Aseer",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2013",
        "VALUE": "2742"
    }, {
        "REGION_ARB": "تبوك",
        "ID": 211,
        "REGION_ENG": "Tabouk",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2013",
        "VALUE": "1728"
    }, {
        "REGION_ARB": "حائل",
        "ID": 212,
        "REGION_ENG": "Hail",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2013",
        "VALUE": "4003"
    }, {
        "REGION_ARB": "الحدود الشمالية",
        "ID": 213,
        "REGION_ENG": "Northern Borders",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2013",
        "VALUE": "1500"
    }, {
        "REGION_ARB": "جازان",
        "ID": 214,
        "REGION_ENG": "Jazan",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2013",
        "VALUE": "715"
    }, {
        "REGION_ARB": "نجران",
        "ID": 215,
        "REGION_ENG": "Najran",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2013",
        "VALUE": "2293"
    }, {
        "REGION_ARB": "الباحة",
        "ID": 216,
        "REGION_ENG": "Al-Baha",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2013",
        "VALUE": "723"
    }, {
        "REGION_ARB": "الجوف",
        "ID": 217,
        "REGION_ENG": "Al-Jouf",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2013",
        "VALUE": "859"
    }, {
        "REGION_ARB": "المجموع",
        "ID": 218,
        "REGION_ENG": "Total",
        "INDI_ARB": "السجلات التجارية المشطوبة",
        "INDI_ENG": "Abolished Records",
        "YEAR": "2013",
        "VALUE": "71607"
    }, {
        "REGION_ARB": "الرياض",
        "ID": 219,
        "REGION_ENG": "Al-Riyadh",
        "INDI_ARB": "عدد السكان",
        "INDI_ENG": "Population",
        "YEAR": "2017",
        "VALUE": "8216284"
    }, {
        "REGION_ARB": "مكة المكرمة",
        "ID": 220,
        "REGION_ENG": "Makkah Al-Mokarramah",
        "INDI_ARB": "عدد السكان",
        "INDI_ENG": "Population",
        "YEAR": "2017",
        "VALUE": "8557766"
    }, {
        "REGION_ARB": "المدينة المنورة",
        "ID": 221,
        "REGION_ENG": "Al-Madinah Al-Monawarah",
        "INDI_ARB": "عدد السكان",
        "INDI_ENG": "Population",
        "YEAR": "2017",
        "VALUE": "2132679"
    }, {
        "REGION_ARB": "القصيم",
        "ID": 222,
        "REGION_ENG": "Al-Qaseem",
        "INDI_ARB": "عدد السكان",
        "INDI_ENG": "Population",
        "YEAR": "2017",
        "VALUE": "1423935"
    }, {
        "REGION_ARB": "الشرقية",
        "ID": 223,
        "REGION_ENG": "Eastern Region",
        "INDI_ARB": "عدد السكان",
        "INDI_ENG": "Population",
        "YEAR": "2017",
        "VALUE": "4900325"
    }, {
        "REGION_ARB": "عسير",
        "ID": 224,
        "REGION_ENG": "Aseer",
        "INDI_ARB": "عدد السكان",
        "INDI_ENG": "Population",
        "YEAR": "2017",
        "VALUE": "2211875"
    }, {
        "REGION_ARB": "تبوك",
        "ID": 225,
        "REGION_ENG": "Tabouk",
        "INDI_ARB": "عدد السكان",
        "INDI_ENG": "Population",
        "YEAR": "2017",
        "VALUE": "910030"
    }, {
        "REGION_ARB": "حائل",
        "ID": 226,
        "REGION_ENG": "Hail",
        "INDI_ARB": "عدد السكان",
        "INDI_ENG": "Population",
        "YEAR": "2017",
        "VALUE": "699774"
    }, {
        "REGION_ARB": "الحدود الشمالية",
        "ID": 227,
        "REGION_ENG": "Northern Borders",
        "INDI_ARB": "عدد السكان",
        "INDI_ENG": "Population",
        "YEAR": "2017",
        "VALUE": "365231"
    }, {
        "REGION_ARB": "جازان",
        "ID": 228,
        "REGION_ENG": "Jazan",
        "INDI_ARB": "عدد السكان",
        "INDI_ENG": "Population",
        "YEAR": "2017",
        "VALUE": "1567547"
    }, {
        "REGION_ARB": "نجران",
        "ID": 229,
        "REGION_ENG": "Najran",
        "INDI_ARB": "عدد السكان",
        "INDI_ENG": "Population",
        "YEAR": "2017",
        "VALUE": "582243"
    }, {
        "REGION_ARB": "الباحة",
        "ID": 230,
        "REGION_ENG": "Al-Baha",
        "INDI_ARB": "عدد السكان",
        "INDI_ENG": "Population",
        "YEAR": "2017",
        "VALUE": "476172"
    }, {
        "REGION_ARB": "الجوف",
        "ID": 231,
        "REGION_ENG": "Al-Jouf",
        "INDI_ARB": "عدد السكان",
        "INDI_ENG": "Population",
        "YEAR": "2017",
        "VALUE": "508475"
    }, {
        "REGION_ARB": "المجموع",
        "ID": 232,
        "REGION_ENG": "Total",
        "INDI_ARB": "عدد السكان",
        "INDI_ENG": "Population",
        "YEAR": "2017",
        "VALUE": "32552336"
    }, {
        "REGION_ARB": "الرياض",
        "ID": 233,
        "REGION_ENG": "Al-Riyadh",
        "INDI_ARB": "عدد السكان",
        "INDI_ENG": "Population",
        "YEAR": "2016",
        "VALUE": "8002100"
    }, {
        "REGION_ARB": "مكة المكرمة",
        "ID": 234,
        "REGION_ENG": "Makkah Al-Mokarramah",
        "INDI_ARB": "عدد السكان",
        "INDI_ENG": "Population",
        "YEAR": "2016",
        "VALUE": "8325304"
    }, {
        "REGION_ARB": "المدينة المنورة",
        "ID": 235,
        "REGION_ENG": "Al-Madinah Al-Monawarah",
        "INDI_ARB": "عدد السكان",
        "INDI_ENG": "Population",
        "YEAR": "2016",
        "VALUE": "2080436"
    }, {
        "REGION_ARB": "القصيم",
        "ID": 236,
        "REGION_ENG": "Al-Qaseem",
        "INDI_ARB": "عدد السكان",
        "INDI_ENG": "Population",
        "YEAR": "2016",
        "VALUE": "1387996"
    }, {
        "REGION_ARB": "الشرقية",
        "ID": 237,
        "REGION_ENG": "Eastern Region",
        "INDI_ARB": "عدد السكان",
        "INDI_ENG": "Population",
        "YEAR": "2016",
        "VALUE": "4780619"
    }, {
        "REGION_ARB": "عسير",
        "ID": 238,
        "REGION_ENG": "Aseer",
        "INDI_ARB": "عدد السكان",
        "INDI_ENG": "Population",
        "YEAR": "2016",
        "VALUE": "2164172"
    }, {
        "REGION_ARB": "تبوك",
        "ID": 239,
        "REGION_ENG": "Tabouk",
        "INDI_ARB": "عدد السكان",
        "INDI_ENG": "Population",
        "YEAR": "2016",
        "VALUE": "890922"
    }, {
        "REGION_ARB": "حائل",
        "ID": 240,
        "REGION_ENG": "Hail",
        "INDI_ARB": "عدد السكان",
        "INDI_ENG": "Population",
        "YEAR": "2016",
        "VALUE": "684619"
    }, {
        "REGION_ARB": "الحدود الشمالية",
        "ID": 241,
        "REGION_ENG": "Northern Borders",
        "INDI_ARB": "عدد السكان",
        "INDI_ENG": "Population",
        "YEAR": "2016",
        "VALUE": "359235"
    }, {
        "REGION_ARB": "جازان",
        "ID": 242,
        "REGION_ENG": "Jazan",
        "INDI_ARB": "عدد السكان",
        "INDI_ENG": "Population",
        "YEAR": "2016",
        "VALUE": "1533680"
    }, {
        "REGION_ARB": "نجران",
        "ID": 243,
        "REGION_ENG": "Najran",
        "INDI_ARB": "عدد السكان",
        "INDI_ENG": "Population",
        "YEAR": "2016",
        "VALUE": "569332"
    }, {
        "REGION_ARB": "الباحة",
        "ID": 244,
        "REGION_ENG": "Al-Baha",
        "INDI_ARB": "عدد السكان",
        "INDI_ENG": "Population",
        "YEAR": "2016",
        "VALUE": "466384"
    }, {
        "REGION_ARB": "الجوف",
        "ID": 245,
        "REGION_ENG": "Al-Jouf",
        "INDI_ARB": "عدد السكان",
        "INDI_ENG": "Population",
        "YEAR": "2016",
        "VALUE": "497509"
    }, {
        "REGION_ARB": "المجموع",
        "ID": 246,
        "REGION_ENG": "Total",
        "INDI_ARB": "عدد السكان",
        "INDI_ENG": "Population",
        "YEAR": "2016",
        "VALUE": "31742308"
    }, {
        "REGION_ARB": "الرياض",
        "ID": 247,
        "REGION_ENG": "Al-Riyadh",
        "INDI_ARB": "السكان السعوديين",
        "INDI_ENG": "Number of Saudi",
        "YEAR": "2017",
        "VALUE": "4658322"
    }, {
        "REGION_ARB": "مكة المكرمة",
        "ID": 248,
        "REGION_ENG": "Makkah Al-Mokarramah",
        "INDI_ARB": "السكان السعوديين",
        "INDI_ENG": "Number of Saudi",
        "YEAR": "2017",
        "VALUE": "4516577"
    }, {
        "REGION_ARB": "المدينة المنورة",
        "ID": 249,
        "REGION_ENG": "Al-Madinah Al-Monawarah",
        "INDI_ARB": "السكان السعوديين",
        "INDI_ENG": "Number of Saudi",
        "YEAR": "2017",
        "VALUE": "1376244"
    }, {
        "REGION_ARB": "القصيم",
        "ID": 250,
        "REGION_ENG": "Al-Qaseem",
        "INDI_ARB": "السكان السعوديين",
        "INDI_ENG": "Number of Saudi",
        "YEAR": "2017",
        "VALUE": "1009543"
    }, {
        "REGION_ARB": "الشرقية",
        "ID": 251,
        "REGION_ENG": "Eastern Region",
        "INDI_ARB": "السكان السعوديين",
        "INDI_ENG": "Number of Saudi",
        "YEAR": "2017",
        "VALUE": "3140362"
    }, {
        "REGION_ARB": "عسير",
        "ID": 252,
        "REGION_ENG": "Aseer",
        "INDI_ARB": "السكان السعوديين",
        "INDI_ENG": "Number of Saudi",
        "YEAR": "2017",
        "VALUE": "1750131"
    }, {
        "REGION_ARB": "تبوك",
        "ID": 253,
        "REGION_ENG": "Tabouk",
        "INDI_ARB": "السكان السعوديين",
        "INDI_ENG": "Number of Saudi",
        "YEAR": "2017",
        "VALUE": "722664"
    }, {
        "REGION_ARB": "حائل",
        "ID": 254,
        "REGION_ENG": "Hail",
        "INDI_ARB": "السكان السعوديين",
        "INDI_ENG": "Number of Saudi",
        "YEAR": "2017",
        "VALUE": "538099"
    }, {
        "REGION_ARB": "الحدود الشمالية",
        "ID": 255,
        "REGION_ENG": "Northern Borders",
        "INDI_ARB": "السكان السعوديين",
        "INDI_ENG": "Number of Saudi",
        "YEAR": "2017",
        "VALUE": "288921"
    }, {
        "REGION_ARB": "جازان",
        "ID": 256,
        "REGION_ENG": "Jazan",
        "INDI_ARB": "السكان السعوديين",
        "INDI_ENG": "Number of Saudi",
        "YEAR": "2017",
        "VALUE": "1207269"
    }, {
        "REGION_ARB": "نجران",
        "ID": 257,
        "REGION_ENG": "Najran",
        "INDI_ARB": "السكان السعوديين",
        "INDI_ENG": "Number of Saudi",
        "YEAR": "2017",
        "VALUE": "438041"
    }, {
        "REGION_ARB": "الباحة",
        "ID": 258,
        "REGION_ENG": "Al-Baha",
        "INDI_ARB": "السكان السعوديين",
        "INDI_ENG": "Number of Saudi",
        "YEAR": "2017",
        "VALUE": "382438"
    }, {
        "REGION_ARB": "الجوف",
        "ID": 259,
        "REGION_ENG": "Al-Jouf",
        "INDI_ARB": "السكان السعوديين",
        "INDI_ENG": "Number of Saudi",
        "YEAR": "2017",
        "VALUE": "379751"
    }, {
        "REGION_ARB": "المجموع",
        "ID": 260,
        "REGION_ENG": "Total",
        "INDI_ARB": "السكان السعوديين",
        "INDI_ENG": "Number of Saudi",
        "YEAR": "2017",
        "VALUE": "20408362"
    }, {
        "REGION_ARB": "الرياض",
        "ID": 261,
        "REGION_ENG": "Al-Riyadh",
        "INDI_ARB": "السكان السعوديين",
        "INDI_ENG": "Number of Saudi",
        "YEAR": "2016",
        "VALUE": "4579570"
    }, {
        "REGION_ARB": "مكة المكرمة",
        "ID": 262,
        "REGION_ENG": "Makkah Al-Mokarramah",
        "INDI_ARB": "السكان السعوديين",
        "INDI_ENG": "Number of Saudi",
        "YEAR": "2016",
        "VALUE": "4440571"
    }, {
        "REGION_ARB": "المدينة المنورة",
        "ID": 263,
        "REGION_ENG": "Al-Madinah Al-Monawarah",
        "INDI_ARB": "السكان السعوديين",
        "INDI_ENG": "Number of Saudi",
        "YEAR": "2016",
        "VALUE": "1353102"
    }, {
        "REGION_ARB": "القصيم",
        "ID": 264,
        "REGION_ENG": "Al-Qaseem",
        "INDI_ARB": "السكان السعوديين",
        "INDI_ENG": "Number of Saudi",
        "YEAR": "2016",
        "VALUE": "991032"
    }, {
        "REGION_ARB": "الشرقية",
        "ID": 265,
        "REGION_ENG": "Eastern Region",
        "INDI_ARB": "السكان السعوديين",
        "INDI_ENG": "Number of Saudi",
        "YEAR": "2016",
        "VALUE": "3087687"
    }, {
        "REGION_ARB": "عسير",
        "ID": 266,
        "REGION_ENG": "Aseer",
        "INDI_ARB": "السكان السعوديين",
        "INDI_ENG": "Number of Saudi",
        "YEAR": "2016",
        "VALUE": "1719950"
    }, {
        "REGION_ARB": "تبوك",
        "ID": 267,
        "REGION_ENG": "Tabouk",
        "INDI_ARB": "السكان السعوديين",
        "INDI_ENG": "Number of Saudi",
        "YEAR": "2016",
        "VALUE": "710699"
    }, {
        "REGION_ARB": "حائل",
        "ID": 268,
        "REGION_ENG": "Hail",
        "INDI_ARB": "السكان السعوديين",
        "INDI_ENG": "Number of Saudi",
        "YEAR": "2016",
        "VALUE": "529012"
    }, {
        "REGION_ARB": "الحدود الشمالية",
        "ID": 269,
        "REGION_ENG": "Northern Borders",
        "INDI_ARB": "السكان السعوديين",
        "INDI_ENG": "Number of Saudi",
        "YEAR": "2016",
        "VALUE": "285486"
    }, {
        "REGION_ARB": "جازان",
        "ID": 270,
        "REGION_ENG": "Jazan",
        "INDI_ARB": "السكان السعوديين",
        "INDI_ENG": "Number of Saudi",
        "YEAR": "2016",
        "VALUE": "1187284"
    }, {
        "REGION_ARB": "نجران",
        "ID": 271,
        "REGION_ENG": "Najran",
        "INDI_ARB": "السكان السعوديين",
        "INDI_ENG": "Number of Saudi",
        "YEAR": "2016",
        "VALUE": "430711"
    }, {
        "REGION_ARB": "الباحة",
        "ID": 272,
        "REGION_ENG": "Al-Baha",
        "INDI_ARB": "السكان السعوديين",
        "INDI_ENG": "Number of Saudi",
        "YEAR": "2016",
        "VALUE": "376204"
    }, {
        "REGION_ARB": "الجوف",
        "ID": 273,
        "REGION_ENG": "Al-Jouf",
        "INDI_ARB": "السكان السعوديين",
        "INDI_ENG": "Number of Saudi",
        "YEAR": "2016",
        "VALUE": "373662"
    }, {
        "REGION_ARB": "المجموع",
        "ID": 274,
        "REGION_ENG": "Total",
        "INDI_ARB": "السكان السعوديين",
        "INDI_ENG": "Number of Saudi",
        "YEAR": "2016",
        "VALUE": "20064970"
    }, {
        "REGION_ARB": "الرياض",
        "ID": 275,
        "REGION_ENG": "Al-Riyadh",
        "INDI_ARB": "السكان غير السعودين",
        "INDI_ENG": "Number of Non-Saudi",
        "YEAR": "2017",
        "VALUE": "3557962"
    }, {
        "REGION_ARB": "مكة المكرمة",
        "ID": 276,
        "REGION_ENG": "Makkah Al-Mokarramah",
        "INDI_ARB": "السكان غير السعودين",
        "INDI_ENG": "Number of Non-Saudi",
        "YEAR": "2017",
        "VALUE": "4041189"
    }, {
        "REGION_ARB": "المدينة المنورة",
        "ID": 277,
        "REGION_ENG": "Al-Madinah Al-Monawarah",
        "INDI_ARB": "السكان غير السعودين",
        "INDI_ENG": "Number of Non-Saudi",
        "YEAR": "2017",
        "VALUE": "756435"
    }, {
        "REGION_ARB": "القصيم",
        "ID": 278,
        "REGION_ENG": "Al-Qaseem",
        "INDI_ARB": "السكان غير السعودين",
        "INDI_ENG": "Number of Non-Saudi",
        "YEAR": "2017",
        "VALUE": "414392"
    }, {
        "REGION_ARB": "الشرقية",
        "ID": 279,
        "REGION_ENG": "Eastern Region",
        "INDI_ARB": "السكان غير السعودين",
        "INDI_ENG": "Number of Non-Saudi",
        "YEAR": "2017",
        "VALUE": "1759963"
    }, {
        "REGION_ARB": "عسير",
        "ID": 280,
        "REGION_ENG": "Aseer",
        "INDI_ARB": "السكان غير السعودين",
        "INDI_ENG": "Number of Non-Saudi",
        "YEAR": "2017",
        "VALUE": "461744"
    }, {
        "REGION_ARB": "تبوك",
        "ID": 281,
        "REGION_ENG": "Tabouk",
        "INDI_ARB": "السكان غير السعودين",
        "INDI_ENG": "Number of Non-Saudi",
        "YEAR": "2017",
        "VALUE": "187366"
    }, {
        "REGION_ARB": "حائل",
        "ID": 282,
        "REGION_ENG": "Hail",
        "INDI_ARB": "السكان غير السعودين",
        "INDI_ENG": "Number of Non-Saudi",
        "YEAR": "2017",
        "VALUE": "161675"
    }, {
        "REGION_ARB": "الحدود الشمالية",
        "ID": 283,
        "REGION_ENG": "Northern Borders",
        "INDI_ARB": "السكان غير السعودين",
        "INDI_ENG": "Number of Non-Saudi",
        "YEAR": "2017",
        "VALUE": "76310"
    }, {
        "REGION_ARB": "جازان",
        "ID": 284,
        "REGION_ENG": "Jazan",
        "INDI_ARB": "السكان غير السعودين",
        "INDI_ENG": "Number of Non-Saudi",
        "YEAR": "2017",
        "VALUE": "360278"
    }, {
        "REGION_ARB": "نجران",
        "ID": 285,
        "REGION_ENG": "Najran",
        "INDI_ARB": "السكان غير السعودين",
        "INDI_ENG": "Number of Non-Saudi",
        "YEAR": "2017",
        "VALUE": "144202"
    }, {
        "REGION_ARB": "الباحة",
        "ID": 286,
        "REGION_ENG": "Al-Baha",
        "INDI_ARB": "السكان غير السعودين",
        "INDI_ENG": "Number of Non-Saudi",
        "YEAR": "2017",
        "VALUE": "93734"
    }, {
        "REGION_ARB": "الجوف",
        "ID": 287,
        "REGION_ENG": "Al-Jouf",
        "INDI_ARB": "السكان غير السعودين",
        "INDI_ENG": "Number of Non-Saudi",
        "YEAR": "2017",
        "VALUE": "128724"
    }, {
        "REGION_ARB": "المجموع",
        "ID": 288,
        "REGION_ENG": "Total",
        "INDI_ARB": "السكان غير السعودين",
        "INDI_ENG": "Number of Non-Saudi",
        "YEAR": "2017",
        "VALUE": "12143974"
    }, {
        "REGION_ARB": "الرياض",
        "ID": 289,
        "REGION_ENG": "Al-Riyadh",
        "INDI_ARB": "السكان غير السعودين",
        "INDI_ENG": "Number of Non-Saudi",
        "YEAR": "2016",
        "VALUE": "3422530"
    }, {
        "REGION_ARB": "مكة المكرمة",
        "ID": 290,
        "REGION_ENG": "Makkah Al-Mokarramah",
        "INDI_ARB": "السكان غير السعودين",
        "INDI_ENG": "Number of Non-Saudi",
        "YEAR": "2016",
        "VALUE": "3884733"
    }, {
        "REGION_ARB": "المدينة المنورة",
        "ID": 291,
        "REGION_ENG": "Al-Madinah Al-Monawarah",
        "INDI_ARB": "السكان غير السعودين",
        "INDI_ENG": "Number of Non-Saudi",
        "YEAR": "2016",
        "VALUE": "727334"
    }, {
        "REGION_ARB": "القصيم",
        "ID": 292,
        "REGION_ENG": "Al-Qaseem",
        "INDI_ARB": "السكان غير السعودين",
        "INDI_ENG": "Number of Non-Saudi",
        "YEAR": "2016",
        "VALUE": "396964"
    }, {
        "REGION_ARB": "الشرقية",
        "ID": 293,
        "REGION_ENG": "Eastern Region",
        "INDI_ARB": "السكان غير السعودين",
        "INDI_ENG": "Number of Non-Saudi",
        "YEAR": "2016",
        "VALUE": "1692932"
    }, {
        "REGION_ARB": "عسير",
        "ID": 294,
        "REGION_ENG": "Aseer",
        "INDI_ARB": "السكان غير السعودين",
        "INDI_ENG": "Number of Non-Saudi",
        "YEAR": "2016",
        "VALUE": "444222"
    }, {
        "REGION_ARB": "تبوك",
        "ID": 295,
        "REGION_ENG": "Tabouk",
        "INDI_ARB": "السكان غير السعودين",
        "INDI_ENG": "Number of Non-Saudi",
        "YEAR": "2016",
        "VALUE": "180223"
    }, {
        "REGION_ARB": "حائل",
        "ID": 296,
        "REGION_ENG": "Hail",
        "INDI_ARB": "السكان غير السعودين",
        "INDI_ENG": "Number of Non-Saudi",
        "YEAR": "2016",
        "VALUE": "155607"
    }, {
        "REGION_ARB": "الحدود الشمالية",
        "ID": 297,
        "REGION_ENG": "Northern Borders",
        "INDI_ARB": "السكان غير السعودين",
        "INDI_ENG": "Number of Non-Saudi",
        "YEAR": "2016",
        "VALUE": "73749"
    }, {
        "REGION_ARB": "جازان",
        "ID": 298,
        "REGION_ENG": "Jazan",
        "INDI_ARB": "السكان غير السعودين",
        "INDI_ENG": "Number of Non-Saudi",
        "YEAR": "2016",
        "VALUE": "346396"
    }, {
        "REGION_ARB": "نجران",
        "ID": 299,
        "REGION_ENG": "Najran",
        "INDI_ARB": "السكان غير السعودين",
        "INDI_ENG": "Number of Non-Saudi",
        "YEAR": "2016",
        "VALUE": "138621"
    }, {
        "REGION_ARB": "الباحة",
        "ID": 300,
        "REGION_ENG": "Al-Baha",
        "INDI_ARB": "السكان غير السعودين",
        "INDI_ENG": "Number of Non-Saudi",
        "YEAR": "2016",
        "VALUE": "90180"
    }, {
        "REGION_ARB": "الجوف",
        "ID": 301,
        "REGION_ENG": "Al-Jouf",
        "INDI_ARB": "السكان غير السعودين",
        "INDI_ENG": "Number of Non-Saudi",
        "YEAR": "2016",
        "VALUE": "123847"
    }, {
        "REGION_ARB": "المجموع",
        "ID": 302,
        "REGION_ENG": "Total",
        "INDI_ARB": "السكان غير السعودين",
        "INDI_ENG": "Number of Non-Saudi",
        "YEAR": "2016",
        "VALUE": "11677338"
    }, {
        "REGION_ARB": "الرياض",
        "ID": 303,
        "REGION_ENG": "Al-Riyadh",
        "INDI_ARB": "عدد السكان الذكور",
        "INDI_ENG": "Number of Males",
        "YEAR": "2017",
        "VALUE": "4864266"
    }, {
        "REGION_ARB": "مكة المكرمة",
        "ID": 304,
        "REGION_ENG": "Makkah Al-Mokarramah",
        "INDI_ARB": "عدد السكان الذكور",
        "INDI_ENG": "Number of Males",
        "YEAR": "2017",
        "VALUE": "4864584"
    }, {
        "REGION_ARB": "المدينة المنورة",
        "ID": 305,
        "REGION_ENG": "Al-Madinah Al-Monawarah",
        "INDI_ARB": "عدد السكان الذكور",
        "INDI_ENG": "Number of Males",
        "YEAR": "2017",
        "VALUE": "1194428"
    }, {
        "REGION_ARB": "القصيم",
        "ID": 306,
        "REGION_ENG": "Al-Qaseem",
        "INDI_ARB": "عدد السكان الذكور",
        "INDI_ENG": "Number of Males",
        "YEAR": "2017",
        "VALUE": "821310"
    }, {
        "REGION_ARB": "الشرقية",
        "ID": 307,
        "REGION_ENG": "Eastern Region",
        "INDI_ARB": "عدد السكان الذكور",
        "INDI_ENG": "Number of Males",
        "YEAR": "2017",
        "VALUE": "2941476"
    }, {
        "REGION_ARB": "عسير",
        "ID": 308,
        "REGION_ENG": "Aseer",
        "INDI_ARB": "عدد السكان الذكور",
        "INDI_ENG": "Number of Males",
        "YEAR": "2017",
        "VALUE": "1210136"
    }, {
        "REGION_ARB": "تبوك",
        "ID": 309,
        "REGION_ENG": "Tabouk",
        "INDI_ARB": "عدد السكان الذكور",
        "INDI_ENG": "Number of Males",
        "YEAR": "2017",
        "VALUE": "511413"
    }, {
        "REGION_ARB": "حائل",
        "ID": 310,
        "REGION_ENG": "Hail",
        "INDI_ARB": "عدد السكان الذكور",
        "INDI_ENG": "Number of Males",
        "YEAR": "2017",
        "VALUE": "386210"
    }, {
        "REGION_ARB": "الحدود الشمالية",
        "ID": 311,
        "REGION_ENG": "Northern Borders",
        "INDI_ARB": "عدد السكان الذكور",
        "INDI_ENG": "Number of Males",
        "YEAR": "2017",
        "VALUE": "201182"
    }, {
        "REGION_ARB": "جازان",
        "ID": 312,
        "REGION_ENG": "Jazan",
        "INDI_ARB": "عدد السكان الذكور",
        "INDI_ENG": "Number of Males",
        "YEAR": "2017",
        "VALUE": "848649"
    }, {
        "REGION_ARB": "نجران",
        "ID": 313,
        "REGION_ENG": "Najran",
        "INDI_ARB": "عدد السكان الذكور",
        "INDI_ENG": "Number of Males",
        "YEAR": "2017",
        "VALUE": "322878"
    }, {
        "REGION_ARB": "الباحة",
        "ID": 314,
        "REGION_ENG": "Al-Baha",
        "INDI_ARB": "عدد السكان الذكور",
        "INDI_ENG": "Number of Males",
        "YEAR": "2017",
        "VALUE": "254527"
    }, {
        "REGION_ARB": "الجوف",
        "ID": 315,
        "REGION_ENG": "Al-Jouf",
        "INDI_ARB": "عدد السكان الذكور",
        "INDI_ENG": "Number of Males",
        "YEAR": "2017",
        "VALUE": "289414"
    }, {
        "REGION_ARB": "المجموع",
        "ID": 316,
        "REGION_ENG": "Total",
        "INDI_ARB": "عدد السكان الذكور",
        "INDI_ENG": "Number of Males",
        "YEAR": "2017",
        "VALUE": "18710473"
    }, {
        "REGION_ARB": "الرياض",
        "ID": 317,
        "REGION_ENG": "Al-Riyadh",
        "INDI_ARB": "عدد السكان الذكور",
        "INDI_ENG": "Number of Males",
        "YEAR": "2016",
        "VALUE": "4735009"
    }, {
        "REGION_ARB": "مكة المكرمة",
        "ID": 318,
        "REGION_ENG": "Makkah Al-Mokarramah",
        "INDI_ARB": "عدد السكان الذكور",
        "INDI_ENG": "Number of Males",
        "YEAR": "2016",
        "VALUE": "4731921"
    }, {
        "REGION_ARB": "المدينة المنورة",
        "ID": 319,
        "REGION_ENG": "Al-Madinah Al-Monawarah",
        "INDI_ARB": "عدد السكان الذكور",
        "INDI_ENG": "Number of Males",
        "YEAR": "2016",
        "VALUE": "1164623"
    }, {
        "REGION_ARB": "القصيم",
        "ID": 320,
        "REGION_ENG": "Al-Qaseem",
        "INDI_ARB": "عدد السكان الذكور",
        "INDI_ENG": "Number of Males",
        "YEAR": "2016",
        "VALUE": "799904"
    }, {
        "REGION_ARB": "الشرقية",
        "ID": 321,
        "REGION_ENG": "Eastern Region",
        "INDI_ARB": "عدد السكان الذكور",
        "INDI_ENG": "Number of Males",
        "YEAR": "2016",
        "VALUE": "2866750"
    }, {
        "REGION_ARB": "عسير",
        "ID": 322,
        "REGION_ENG": "Aseer",
        "INDI_ARB": "عدد السكان الذكور",
        "INDI_ENG": "Number of Males",
        "YEAR": "2016",
        "VALUE": "1183152"
    }, {
        "REGION_ARB": "تبوك",
        "ID": 323,
        "REGION_ENG": "Tabouk",
        "INDI_ARB": "عدد السكان الذكور",
        "INDI_ENG": "Number of Males",
        "YEAR": "2016",
        "VALUE": "500255"
    }, {
        "REGION_ARB": "حائل",
        "ID": 324,
        "REGION_ENG": "Hail",
        "INDI_ARB": "عدد السكان الذكور",
        "INDI_ENG": "Number of Males",
        "YEAR": "2016",
        "VALUE": "377553"
    }, {
        "REGION_ARB": "الحدود الشمالية",
        "ID": 325,
        "REGION_ENG": "Northern Borders",
        "INDI_ARB": "عدد السكان الذكور",
        "INDI_ENG": "Number of Males",
        "YEAR": "2016",
        "VALUE": "197550"
    }, {
        "REGION_ARB": "جازان",
        "ID": 326,
        "REGION_ENG": "Jazan",
        "INDI_ARB": "عدد السكان الذكور",
        "INDI_ENG": "Number of Males",
        "YEAR": "2016",
        "VALUE": "829878"
    }, {
        "REGION_ARB": "نجران",
        "ID": 327,
        "REGION_ENG": "Najran",
        "INDI_ARB": "عدد السكان الذكور",
        "INDI_ENG": "Number of Males",
        "YEAR": "2016",
        "VALUE": "315464"
    }, {
        "REGION_ARB": "الباحة",
        "ID": 328,
        "REGION_ENG": "Al-Baha",
        "INDI_ARB": "عدد السكان الذكور",
        "INDI_ENG": "Number of Males",
        "YEAR": "2016",
        "VALUE": "249018"
    }, {
        "REGION_ARB": "الجوف",
        "ID": 329,
        "REGION_ENG": "Al-Jouf",
        "INDI_ARB": "عدد السكان الذكور",
        "INDI_ENG": "Number of Males",
        "YEAR": "2016",
        "VALUE": "282887"
    }, {
        "REGION_ARB": "المجموع",
        "ID": 330,
        "REGION_ENG": "Total",
        "INDI_ARB": "عدد السكان الذكور",
        "INDI_ENG": "Number of Males",
        "YEAR": "2016",
        "VALUE": "18233964"
    }, {
        "REGION_ARB": "الرياض",
        "ID": 331,
        "REGION_ENG": "Al-Riyadh",
        "INDI_ARB": "عدد السكان الإناث",
        "INDI_ENG": "Number of Females",
        "YEAR": "2017",
        "VALUE": "3352018"
    }, {
        "REGION_ARB": "مكة المكرمة",
        "ID": 332,
        "REGION_ENG": "Makkah Al-Mokarramah",
        "INDI_ARB": "عدد السكان الإناث",
        "INDI_ENG": "Number of Females",
        "YEAR": "2017",
        "VALUE": "3693182"
    }, {
        "REGION_ARB": "المدينة المنورة",
        "ID": 333,
        "REGION_ENG": "Al-Madinah Al-Monawarah",
        "INDI_ARB": "عدد السكان الإناث",
        "INDI_ENG": "Number of Females",
        "YEAR": "2017",
        "VALUE": "938251"
    }, {
        "REGION_ARB": "القصيم",
        "ID": 334,
        "REGION_ENG": "Al-Qaseem",
        "INDI_ARB": "عدد السكان الإناث",
        "INDI_ENG": "Number of Females",
        "YEAR": "2017",
        "VALUE": "602625"
    }, {
        "REGION_ARB": "الشرقية",
        "ID": 335,
        "REGION_ENG": "Eastern Region",
        "INDI_ARB": "عدد السكان الإناث",
        "INDI_ENG": "Number of Females",
        "YEAR": "2017",
        "VALUE": "1958849"
    }, {
        "REGION_ARB": "عسير",
        "ID": 336,
        "REGION_ENG": "Aseer",
        "INDI_ARB": "عدد السكان الإناث",
        "INDI_ENG": "Number of Females",
        "YEAR": "2017",
        "VALUE": "1001739"
    }, {
        "REGION_ARB": "تبوك",
        "ID": 337,
        "REGION_ENG": "Tabouk",
        "INDI_ARB": "عدد السكان الإناث",
        "INDI_ENG": "Number of Females",
        "YEAR": "2017",
        "VALUE": "398617"
    }, {
        "REGION_ARB": "حائل",
        "ID": 338,
        "REGION_ENG": "Hail",
        "INDI_ARB": "عدد السكان الإناث",
        "INDI_ENG": "Number of Females",
        "YEAR": "2017",
        "VALUE": "313564"
    }, {
        "REGION_ARB": "الحدود الشمالية",
        "ID": 339,
        "REGION_ENG": "Northern Borders",
        "INDI_ARB": "عدد السكان الإناث",
        "INDI_ENG": "Number of Females",
        "YEAR": "2017",
        "VALUE": "164049"
    }, {
        "REGION_ARB": "جازان",
        "ID": 340,
        "REGION_ENG": "Jazan",
        "INDI_ARB": "عدد السكان الإناث",
        "INDI_ENG": "Number of Females",
        "YEAR": "2017",
        "VALUE": "718898"
    }, {
        "REGION_ARB": "نجران",
        "ID": 341,
        "REGION_ENG": "Najran",
        "INDI_ARB": "عدد السكان الإناث",
        "INDI_ENG": "Number of Females",
        "YEAR": "2017",
        "VALUE": "259365"
    }, {
        "REGION_ARB": "الباحة",
        "ID": 342,
        "REGION_ENG": "Al-Baha",
        "INDI_ARB": "عدد السكان الإناث",
        "INDI_ENG": "Number of Females",
        "YEAR": "2017",
        "VALUE": "221645"
    }, {
        "REGION_ARB": "الجوف",
        "ID": 343,
        "REGION_ENG": "Al-Jouf",
        "INDI_ARB": "عدد السكان الإناث",
        "INDI_ENG": "Number of Females",
        "YEAR": "2017",
        "VALUE": "219061"
    }, {
        "REGION_ARB": "المجموع",
        "ID": 344,
        "REGION_ENG": "Total",
        "INDI_ARB": "عدد السكان الإناث",
        "INDI_ENG": "Number of Females",
        "YEAR": "2017",
        "VALUE": "13841863"
    }, {
        "REGION_ARB": "الرياض",
        "ID": 345,
        "REGION_ENG": "Al-Riyadh",
        "INDI_ARB": "عدد السكان الإناث",
        "INDI_ENG": "Number of Females",
        "YEAR": "2016",
        "VALUE": "3267091"
    }, {
        "REGION_ARB": "مكة المكرمة",
        "ID": 346,
        "REGION_ENG": "Makkah Al-Mokarramah",
        "INDI_ARB": "عدد السكان الإناث",
        "INDI_ENG": "Number of Females",
        "YEAR": "2016",
        "VALUE": "3593383"
    }, {
        "REGION_ARB": "المدينة المنورة",
        "ID": 347,
        "REGION_ENG": "Al-Madinah Al-Monawarah",
        "INDI_ARB": "عدد السكان الإناث",
        "INDI_ENG": "Number of Females",
        "YEAR": "2016",
        "VALUE": "915813"
    }, {
        "REGION_ARB": "القصيم",
        "ID": 348,
        "REGION_ENG": "Al-Qaseem",
        "INDI_ARB": "عدد السكان الإناث",
        "INDI_ENG": "Number of Females",
        "YEAR": "2016",
        "VALUE": "588092"
    }, {
        "REGION_ARB": "الشرقية",
        "ID": 349,
        "REGION_ENG": "Eastern Region",
        "INDI_ARB": "عدد السكان الإناث",
        "INDI_ENG": "Number of Females",
        "YEAR": "2016",
        "VALUE": "1913869"
    }, {
        "REGION_ARB": "عسير",
        "ID": 350,
        "REGION_ENG": "Aseer",
        "INDI_ARB": "عدد السكان الإناث",
        "INDI_ENG": "Number of Females",
        "YEAR": "2016",
        "VALUE": "981020"
    }, {
        "REGION_ARB": "تبوك",
        "ID": 351,
        "REGION_ENG": "Tabouk",
        "INDI_ARB": "عدد السكان الإناث",
        "INDI_ENG": "Number of Females",
        "YEAR": "2016",
        "VALUE": "390667"
    }, {
        "REGION_ARB": "حائل",
        "ID": 352,
        "REGION_ENG": "Hail",
        "INDI_ARB": "عدد السكان الإناث",
        "INDI_ENG": "Number of Females",
        "YEAR": "2016",
        "VALUE": "307066"
    }, {
        "REGION_ARB": "الحدود الشمالية",
        "ID": 353,
        "REGION_ENG": "Northern Borders",
        "INDI_ARB": "عدد السكان الإناث",
        "INDI_ENG": "Number of Females",
        "YEAR": "2016",
        "VALUE": "161685"
    }, {
        "REGION_ARB": "جازان",
        "ID": 354,
        "REGION_ENG": "Jazan",
        "INDI_ARB": "عدد السكان الإناث",
        "INDI_ENG": "Number of Females",
        "YEAR": "2016",
        "VALUE": "703802"
    }, {
        "REGION_ARB": "نجران",
        "ID": 355,
        "REGION_ENG": "Najran",
        "INDI_ARB": "عدد السكان الإناث",
        "INDI_ENG": "Number of Females",
        "YEAR": "2016",
        "VALUE": "253868"
    }, {
        "REGION_ARB": "الباحة",
        "ID": 356,
        "REGION_ENG": "Al-Baha",
        "INDI_ARB": "عدد السكان الإناث",
        "INDI_ENG": "Number of Females",
        "YEAR": "2016",
        "VALUE": "217366"
    }, {
        "REGION_ARB": "الجوف",
        "ID": 357,
        "REGION_ENG": "Al-Jouf",
        "INDI_ARB": "عدد السكان الإناث",
        "INDI_ENG": "Number of Females",
        "YEAR": "2016",
        "VALUE": "214622"
    }, {
        "REGION_ARB": "المجموع",
        "ID": 358,
        "REGION_ENG": "Total",
        "INDI_ARB": "عدد السكان الإناث",
        "INDI_ENG": "Number of Females",
        "YEAR": "2016",
        "VALUE": "13508344"
    }];
                

//                console.log(JSON.stringify(suudData));
                
                self.indi_options =ko.observableArray([]);
                self.year_options= ko.observableArray([]);
                
                self.indi_options(getLovFromResult(model.lang()?'INDI_ENG':'INDI_ARB',suudData)) ;
                self.year_options(getLovFromResult('YEAR',suudData)) ;
//                self.selYear =ko.observable(self.year_options()[0]);
                self.selIn =ko.observable(self.indi_options()[0].value);

                function updateMap(map) {
                    var geo;
                    switch (map) {
                        case 'africa':
                            geo = JSON.parse(africa);
                            break;
                        case 'asia':
                            geo = JSON.parse(asia);
                            break;
                        case 'australia':
                            geo = JSON.parse(australia);
                            break;
                        case 'europe':
                            geo = JSON.parse(europe);
                            break;
                        case 'northAmerica':
                            geo = JSON.parse(nAmerica);
                            break;
                        case 'southAmerica':
                            geo = JSON.parse(sAmerica);
                            break;
                        case 'world':
                            defaut:
                                    geo = JSON.parse(world);
                    }
                    self.mapProvider({
                        geo: geo,
                        propertiesKeys: {
                            id: 'name',
                            shortLabel: 'iso_a3',
                            longLabel: model.lang()?'name_eng':'name_arb'
                        }
                    });

                    self.areaData.removeAll();
                    var data = geo["features"];
                    // For demo purposes, use the color attribute group handler to give
                    // areas different colors based on a non important data dimension.
                    // In a real application, the color attribute group handler should be
                    // passed a meaningful data dimension.
                    var handler = new oj.ColorAttributeGroupHandler();
                    for (var i = 0; i < data.length; i++) {
                        var id = data[i]["properties"]["name"];
                        var disp_name = data[i]["properties"][model.lang()?'name_eng':'name_arb'];
                        self.areaData.push({id: i,
                            color: handler.getValue(id), displayName:disp_name ,
                            location: id});
                    }
                }

                self.statArray = ko.observableArray(doFilterSearch(['REGION_ENG','YEAR'],['Total','2017'],suudData));


                self.selectionValue = ko.observable(['']);
                
                self.IsYr = ko.computed(function(){
                    return self.ViewType() === "Year";
                }) ;
                
                  if(self.IsYr())
                                    self.statArray(
                                            doFilterSearch(['REGION_ENG','YEAR'],['Total',self.selYear()],suudData,true)
                                            );     
                                    else
                                         self.statArray(
                                            doFilterSearch(['REGION_ENG',model.lang()?'INDI_ENG':'INDI_ARB'],['Total',self.selIn()],suudData,true)
                                            ); 
                
                self.dataprovider = new oj.ArrayDataProvider(self.statArray, {idAttribute: 'ID'});
                self.columnArray = ko.observableArray([{"headerText": self.IsYr() ? model.indicator(): model.year(),  
                                            "sortable": "disabled", "field": self.IsYr() ? model.lang()?'INDI_ENG':'INDI_ARB':"YEAR"},
                                        {"headerRenderer": oj.KnockoutTemplateUtils.getRenderer(
                                            self.IsYr() ? "yr_hdr_tmpl":"in_hdr_tmpl", true),
                                            "sortable": "disabled", "field": "VALUE"  }]);

                self.selectionText = ko.computed(function () {
                    var items = '';
                    self.region('');
                    var selection = self.selectionValue();
                    if (selection && selection.length >0 && selection[0] !== '' ) {
                        for (var i = 0; i < selection.length; i++) {
                            // Find matching area from data model and grab info
                            for (var j = 0; j < self.areaData().length; j++) {
                                var area = self.areaData()[j];
                                if (area['id'] === selection[i]) {
                                    
                                    if(self.IsYr())
                                    self.statArray(
                                            doFilterSearch(['REGION_ENG','YEAR'],[area['location'],self.selYear()],suudData,true)
                                            );     
                                    else
                                         self.statArray(
                                            doFilterSearch(['REGION_ENG',model.lang()?'INDI_ENG':'INDI_ARB'],[area['location'],self.selIn()],suudData,true)
                                            ); 
                                    items=j;
                                    self.region(area['displayName']);

//                                    self.statArray([{id: 'Population', name: area['id'] + 1 * 1020237},
//                                        {id: 'Imports', name: area['id'] + 1 * 3426},
//                                        {id: 'Exports', name: area['id'] + 1 * 2314}]);
                                }
                            }
                        }
                    } 
                    else{
                        
                         if(self.IsYr())
                                    self.statArray(
                                            doFilterSearch(['REGION_ENG','YEAR'],['Total',self.selYear()],suudData,true)
                                            );     
                                    else
                                         self.statArray(
                                            doFilterSearch(['REGION_ENG',model.lang()?'INDI_ENG':'INDI_ARB'],['Total',self.selIn()],suudData,true)
                                            ); 
                        
                    }
                           self.columnArray([{"headerText": self.IsYr() ? model.indicator(): model.year(), 
                                            "sortable": "disabled", "field": self.IsYr() ? model.lang()?'INDI_ENG':'INDI_ARB':"YEAR"},
                                        {"headerRenderer": oj.KnockoutTemplateUtils.getRenderer(
                                            self.IsYr() ? "yr_hdr_tmpl":"in_hdr_tmpl", true),
                                            "sortable": "disabled", "field": "VALUE"  }]);
                    return items;
                });

                this.mapListener = function (event) {
                    updateMap(event.detail.value);
                };
            }




            return statmapContentViewModel;
        });