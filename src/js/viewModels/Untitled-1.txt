/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * build-report module
 */
define(['ojs/ojcore', 'knockout', 'jquery', '../libs/html2canvas/html2canvas', 'utils/dataBaseUtil', 'utils/queryUtil', 'ojs/ojknockout', 'ojs/ojtrain',
        'ojs/ojbutton', 'ojs/ojselectcombobox', 'ojs/ojchart', 'ojs/ojinputtext', 'ojs/ojlabel', 'ojs/ojarraydataprovider', 'ojs/ojdialog', 'ojs/ojinputtext'
    ],
    function(oj, ko, $, html2canvas, dbUtil, queryUtil) {

        function TrainData() {
            var self = this;


            self.subjectAreas = [{
                    subjectAreaId: 1,
                    name: model.commercialRegister,
                    css: 'icon icon-graph',
                    url: 'img/avg-pricing.jpg',
                    indicators: [{
                            indicatorId: 1,
                            name: model.transactionCommercialRegisters,
                            css: 'icon icon-dashboard',
                            url: 'img/import.jpg',
                            param: {
                                path: '/shared/Mobile/Trans_Registers',
                                filter: [{ key: 'Column28 > 0 and Column1', value: '2017' }, { key: 'Column24', value: 'مجدد' },
                                    { key: 'Column12', value: 'توزيع عددي-حركات السجل التجاري-شركات' }
                                ],
                                group: 'Column16',
                                value: 'Column28',
                                rows: '5',
                                type: 'bar',
                                label: model.transactionCommercialRegisters
                            },
                            filtrationValues: [{
                                name: model.type,
                                key: 'Column12',
                                value: 'توزيع عددي-حركات السجل التجاري-شركات',
                                values: ['توزيع عددي-حركات السجل التجاري-شركات',
                                    'توزيع عددي-حركات السجل التجاري-مؤسسات'
                                ]
                            }],
                            dimensions: {
                                group: [{ label: 'City', value: 'Column16' }],
                                value: [{ label: 'Observation Value', value: 'Column28' }]
                            },
                            metadata: 'السجل التجاري: وهو سجل يقوم بموجبه صاحب العمل بتقييد بياناته الشخصية لدى وزارة التجارة والاستثمار كالاسم، مكان وتاريخ الميلاد، والجنسية، بالإضافة إلى البيانات الأساسية الخاصة بمؤسسته مثل الاسم التجاري إن وجد، نوع النشاط التجاري، رأس المال، اسم المدير، وجنسية وتاريخ ميلاده، كما يتم تدوين عنوان المركز الرئيسي والفروع التابعة له، وتعتبر وزارة التجارة والاستثمار هي الجهة المسؤولة والناظمة لهذا السجل.'

                        },
                        {
                            indicatorId: 2,
                            name: model.activeCommerce,
                            css: 'icon icon-graph',
                            url: 'img/trade-exchange.jpg',
                            param: {
                                path: '/shared/Mobile/Active_Registers',
                                filter: [{ key: 'Column28 > 0 and Column1', value: '2016' }],
                                group: 'Column16',
                                value: 'Column28',
                                type: 'bar',
                                rows: '5',
                                label: model.activeCommerce
                            },
                            filtrationValues: [{
                                name: model.year,
                                key: 'Column28 > 0 and Column1',
                                value: '2014',
                                values: ['2013', '2014', '2015', '2016']
                            }],
                            dimensions: {
                                group: [{ label: 'City', value: 'Column16' }],
                                value: [{ label: 'Observation Value', value: 'Column28' }]
                            },
                            metadata: 'السجل التجاري: وهو سجل يقوم بموجبه صاحب العمل بتقييد بياناته الشخصية لدى وزارة التجارة والاستثمار كالاسم، مكان وتاريخ الميلاد، والجنسية، بالإضافة إلى البيانات الأساسية الخاصة بمؤسسته/ شركته مثل الاسم التجاري إن وجد، نوع النشاط التجاري، رأس المال، اسم المدير، وجنسية وتاريخ ميلاده، كما يتم تدوين عنوان المركز الرئيسي والفروع التابعة له، وتعتبر وزارة التجارة والاستثمار هي الجهة المسؤولة والناظمة لهذا السجل. السجل التجاري القائم: هو سجل تجاري لمؤسسة أو شركة تمارس نشاطها / أنشطتها الاقتصادية في الوقت الراهن'
                        }
                    ]

                },
                {
                    subjectAreaId: 2,
                    name: model.establishments,
                    css: 'icon icon-dashboard',
                    url: 'img/import.jpg',
                    indicators: [{
                            indicatorId: 1,
                            name: model.economicEstablishments,
                            css: 'icon icon-graph',
                            url: 'img/avg-pricing.jpg',
                            param: {
                                path: '/shared/Mobile/Economic_Establishments',
                                filter: [{ key: 'Column3', value: '2011' },
                                    { key: 'Column7', value: 'أبحاث الإعلان والسوق' }
                                ],
                                group: 'Column4',
                                value: 'Column27',
                                type: 'bar',
                                rows: '0',
                                label: model.economicEstablishments
                            },
                            filtrationValues: [{
                                name: model.year,
                                key: 'Column3',
                                value: '2011',
                                values: ['2010', '2011']
                            }],
                            dimensions: {
                                group: [{ label: 'Type', value: 'Column4' }],
                                value: [{ label: 'Observation Value', value: 'Column27' }]
                            },
                            metadata: 'المنشأة: هي وحدة عمل اقتصادية ذات كيان قانوني لها موقع ثابت يزاول فيها نشاط اقتصادي معين يملكها شخص أو مجموعة أشخاص أو شـركة أو قطاع شبه حكومي أو مؤسسة، وهي أصغر وحدة اقتصادية يمكن أن تتوفر لديها بيانات عن المشتغلين وتعويضاتهم المالية بالإضافة إلى النفقات والإيرادات والتكوينات الرأسمالية.'

                        },
                        {
                            indicatorId: 2,
                            name: model.operatingExpenditure,
                            css: 'icon icon-dashboard',
                            url: 'img/import.jpg',
                            param: {
                                path: '/shared/Mobile/Operating_Ex',
                                filter: [{ key: 'Column7', value: '2010' },
                                    { key: 'Column11', value: 'البحث والتطوير في المجال العلمي' },
                                    { key: 'Column21', value: 'Operating Expenditures' }
                                ],
                                group: 'Column8',
                                value: 'Column31',
                                rows: '0',
                                type: 'bar',
                                label: model.operatingExpenditure
                            },
                            filtrationValues: [{
                                    name: model.year,
                                    key: 'Column7',
                                    value: '2010',
                                    values: ['2010', '2011', '2012', '2013', '2014', '2015']
                                },
                                {
                                    name: model.type,
                                    key: 'Column11',
                                    value: 'البحث والتطوير في المجال العلمي',
                                    values: ['التعليم', 'التخزين وأنشطة الدعم للنقل', 'الاتصالات', 'البحث والتطوير في المجال العلمي']
                                }
                            ],
                            dimensions: {
                                group: [{ label: 'Type', value: 'Column8' }],
                                value: [{ label: 'Observation Value', value: 'Column31' }]
                            },

                            metadata: 'تشمل تعويضات المشتغلين بالاضافة إلى ما تنفقه المؤسسة من السلع والخدمات خلال السنة المالية نتيجة مزاولة نشاطها الاقتصادي سواء كانت مشتراه في نفس العام أو مسحولة من مخزون تم شرتؤه في سنوات سابقة، بالاضافة إلى المبالغ المستحقة الدفع من قبل المؤسسة خلال العام والتي لا تدخل ضمن المستلزمات السلعية أو الخدمية ولكنها مرتبطة بالنشاط الجاري.'
                        }
                    ]
                },
                {
                    subjectAreaId: 3,
                    name: model.foreignTrade,
                    css: 'icon icon-graph',
                    url: 'img/trade-exchange.jpg',
                    indicators: [{
                            indicatorId: 1,
                            name: model.exports,
                            css: 'icon icon-dashboard',
                            url: 'img/import.jpg',
                            param: {
                                path: '/shared/Economic Statistics/International Trade and Balance of Payment/FT/OBIEE FT/Trade Exchange',
                                filter: [{ key: 'Column3', value: '2016' }, { key: 'Column2', value: 'Exports' }],

                                group: 'Column1',
                                value: 'Column4',
                                rows: '5',
                                type: 'bar',
                                label: model.exports
                            },
                            filtrationValues: [{
                                name: model.year,
                                key: 'Column3',
                                value: '',
                                values: ['2015', '2016']
                            }],
                            dimensions: {
                                group: [{ label: 'Country', value: 'Column1' }],
                                value: [{ label: 'Observation Value', value: 'Column4' }]
                            },

                            metadata: 'قيمة الصادرات (F.O.B): تحدد أقيام السلع المصدرة على أساس القيمة (فوب) (تسليم ظهر السفينة أو الطائرة) أي قيمة البضاعة مضافا إليها التكاليف الأخرى حتى تسليمها على ظهر وسيلة الشحن أو تتضمن قيمة البضائع بما فيها كافة المصاريف حتى مكتب التصدير .'
                        },
                        {
                            indicatorId: 2,
                            name: model.imports,
                            css: 'icon icon-dashboard',
                            url: 'img/import.jpg',
                            param: {
                                path: '/shared/Economic Statistics/International Trade and Balance of Payment/FT/OBIEE FT/Trade Exchange',
                                filter: [{ key: 'Column3', value: '2016' }, { key: 'Column2', value: 'Imports' }],
                                group: 'Column1',
                                value: 'Column4',
                                rows: '5',
                                type: 'bar',
                                label: model.imports
                            },
                            filtrationValues: [{
                                name: model.year,
                                key: 'Column3',
                                value: '',
                                values: ['2015', '2016']
                            }],
                            dimensions: {
                                group: [{ label: 'Country', value: 'Column1' }],
                                value: [{ label: 'Observation Value', value: 'Column4' }]
                            },

                            metadata: 'قيمة الواردات (C.I.F): تحدد أقيام السلع المستوردة على أساس القيمة (فوب) (تسليم ظهر السفينة أو الطائرة) أي قيمة البضاعة مضافا إليها التكاليف الأخرى حتى تسليمها على ظهر وسيلة الشحن أو تتضمن قيمة البضائع بما فيها كافة المصاريف حتى مكتب التصدير .'
                        }
                    ]
                }

            ];

            self.subjectAreasOptions = ko.observable();
            self.indicators = ko.observable();
            self.indicatorsOptions = ko.observable([]);
            self.valueOptions = ko.observable([]);
            self.groupOptions = ko.observable([]);
            self.selectedIndicator = ko.observable();
            self.selectedGroup = ko.observable();
            self.selectedValue = ko.observable();
            self.filtration = ko.observableArray();
            self.selectedStp4 = ko.observable();

            self.dispSA = ko.observable('');
            self.dispIn = ko.observable('');
            self.dispDim = ko.observable('');
            self.dispFil = ko.observable('');
            self.selectedFavouriteReport = ko.observable();
            self.favouriteReportName = ko.observable('');




            self.saveFavouriteReport = function() {

                // console.log(self.filtration());
                // console.log(self.selectedIndicator());
                // console.log(self.favouriteReportName());

                console.log(self.filtration());
                console.log(self.selectedIndicator());

                var temp1 = JSON.stringify(self.filtration());
                var temp2 = JSON.stringify(self.selectedIndicator());

                console.log(JSON.parse(temp1));
                console.log(JSON.parse(temp2));

                // console.log(JSON.stringify(self.selectedIndicator()));

                var tempsubjectAreaId = self.indicators();

                var tempselectedIndicatorId = self.selectedIndicator().indicatorId;

                var filters = self.selectedIndicator().param.filter;

                dbUtil.insertData(queryUtil.insertReport, [self.favouriteReportName(), tempsubjectAreaId, tempselectedIndicatorId, JSON.stringify(filters)]).then(function(response) {
                    console.log('Report has been saved into DB');
                });

                // var filtrationValues = [];
                // for (var i = 0; i < self.filtration().length; i++) {
                //     filtrationValues[i] = self.filtration()[i].value;
                // }

                // self.indicators('');
                // self.selectedIndicator('');
                // self.selectedGroup('');
                // self.selectedValue('');
                // self.filterSelectionVal();
                document.querySelector('#modalDialog1').close();
            };

            self.fetchReportNames = function() {
                self.favouriteReports = ko.observable();
                self.tempReportNames = ko.observableArray([]);

                dbUtil.queryData(queryUtil.getReportNames, []).then(function(response) {
                    console.log('Report names fetched from DB');
                    console.log(response);
                    console.log(response.rows.item(0).REPORT_NAME);
                    console.log(response.rows.item(0).REPORT_ID);
                    console.log(response.rows.length);
                    if (response.rows.length > 0) {
                        for (var i = 0; i < response.rows.length; i++) {
                            console.log('Report Name-->' + response.rows.item(i).REPORT_NAME);
                            var temp = {};
                            temp.value = response.rows.item(i).REPORT_ID;
                            temp.label = response.rows.item(i).REPORT_NAME;
                            self.tempReportNames().push(temp);
                        }

                        console.log(self.tempReportNames());
                    }
                });

                self.favouriteReports = new oj.ArrayDataProvider(self.tempReportNames, { idAttribute: 'value' });
                console.log(self.tempReportNames());

                // self.favouriteReports = new oj.ArrayDataProvider(self.tempReportNames, { idAttribute: 'value' });

            };




            self.generateReport = function() {

                console.log(self.filtration());
                for (var i = 0; i < self.filtration().length; i++) {
                    for (var j = 0; j < self.selectedIndicator().param.filter.length; j++) {
                        //console.log(parameter().filter[j].key + " - " + filtrationValues()[i].key);

                        self.selectedStp4(self.selectedIndicator().param);
                        if (self.selectedIndicator().param.filter[j].key == self.filtration()[i].key) {
                            if (self.filtration()[i].value.length == 1) {
                                self.selectedIndicator().param.filter[j].value = self.filtration()[i].value[0];
                            } else {
                                self.selectedIndicator().param.filter[j].value = self.filtration()[i].value;
                            }
                        }
                    }
                }
                console.log(self.selectedIndicator().metadata);
                self.metadata(self.selectedIndicator().metadata);
                callRestService();
                hideall();
                document.getElementById("5").style.display = "block";
                this.selectedStepValue("stp5");
            };

            self.currentRawName = ko.observable('');
            self.savebuttonDisabled = ko.observable(true);

            self.saveRawValueChangeCallback = function(event) {
                if (self.currentRawName()) {
                    self.savebuttonDisabled(false);
                } else {
                    self.savebuttonDisabled(true);
                }
            };

            self.filterSelectionVal = ko.pureComputed(function() {

                var returnVal = 'No Selection'

                if (self.indicators()) {

                    for (k = 0; k < self.subjectAreas.length; k++) {

                        if (self.subjectAreas[k]['indicators'][0] === self.indicators()[0]) {
                            self.dispSA(self.subjectAreas[k]['name']());
                            returnVal = self.subjectAreas[k]['name']();
                        }
                    }
                }
                if (self.selectedIndicator()) {
                    self.dispIn(self.selectedIndicator()['name']());
                    returnVal = returnVal + ' >> ' + self.selectedIndicator()['name']();
                }
                if (self.selectedGroup()) {
                    var groupList = self.selectedIndicator()['dimensions']['group'];
                    for (k = 0; k < groupList.length; k++) {

                        if (groupList[k]['value'] === self.selectedGroup()) {
                            self.dispDim(groupList[k]['label']);
                            returnVal = returnVal = returnVal + ' >> ' + groupList[k]['label'];
                        }
                    }
                }

                if (self.selectedValue()) {
                    var valList = self.selectedIndicator()['dimensions']['value'];
                    for (k = 0; k < valList.length; k++) {

                        if (valList[k]['value'] === self.selectedValue()) {
                            self.dispDim(self.dispDim() + ' / ' + valList[k]['label']);
                            returnVal = returnVal = returnVal + ' >> ' + valList[k]['label'];
                        }
                    }

                    if (self.selectedStp4() || (self.filtration() && self.selectedIndicator()['param'])) {
                        self.dispFil('');
                        for (var i = 0; i < self.filtration().length; i++) {
                            for (var j = 0; j < self.selectedIndicator().param.filter.length; j++) {
                                //console.log(parameter().filter[j].key + " - " + filtrationValues()[i].key);
                                if (self.selectedIndicator().param.filter[j].key === self.filtration()[i].key) {
                                    if (self.filtration()[i].value.length === 1) {
                                        self.dispFil(self.dispFil() + ' / ' + self.filtration()[i].value[0]);
                                        returnVal = returnVal = returnVal + ' >> ' + self.filtration()[i].value[0];
                                    } else {
                                        self.dispFil(self.filtration()[i].value);
                                        returnVal = returnVal = returnVal + ' >> ' + self.filtration()[i].value;
                                    }
                                }
                            }
                        }

                    }



                }

                return returnVal;

            });

            this.selectedStepValue = ko.observable('stp1');
            this.selectedStepLabel = ko.observable('Step One');

            this.stepArray =
                ko.observableArray(
                    [{ label: 'Step 1', id: 'stp1' },
                        { label: 'Step 2', id: 'stp2' },
                        { label: 'Step 3', id: 'stp3' },
                        { label: 'Step 4', id: 'stp4' },
                        { label: 'Step 5', id: 'stp5' }
                    ]);

            this.stepArrayAr =
                ko.observableArray(
                    [{ label: 'خطوة 1', id: 'stp1' },
                        { label: 'خطوة 2', id: 'stp2' },
                        { label: 'خطوة 3', id: 'stp3' },
                        { label: 'خطوة 4', id: 'stp4' },
                        { label: 'خطوة 5', id: 'stp5' }
                    ]);

            function hideall() {
                for (var i = 0; i < 5; i++) {
                    document.getElementById((i + 1) + "").style.display = "none";
                }
            }

            self.setSubjectAreaOptions = function() {
                var subjectAreasOptions = [];
                for (var i = 0; i < self.subjectAreas.length; i++) {
                    subjectAreasOptions.push({ value: self.subjectAreas[i].subjectAreaId, label: self.subjectAreas[i].name() })
                }
                self.subjectAreasOptions(subjectAreasOptions);
                self.indicators('');
                hideall();
                document.getElementById("1").style.display = "block";
                this.selectedStepValue("stp1");
            };

            $(document).ready(function() {
                self.setSubjectAreaOptions();
            });


            self.selectSubjectArea = function() {
                var indicatorsOptions = [];
                //indicatorsOptions.push({});
                //  console.log('Indicator Option:--->' + self.indicators());
                console.log('Subject Area');
                console.log(self.subjectAreas[self.indicators() - 1]);
                var tempSubjectArea = self.subjectAreas[self.indicators() - 1];
                console.log(tempSubjectArea);
                console.log(tempSubjectArea.indicators[1].name());

                for (var i = 0; i < tempSubjectArea.indicators.length; i++) {
                    indicatorsOptions.push({ value: tempSubjectArea.indicators[i], label: tempSubjectArea.indicators[i].name() })

                }
                self.indicatorsOptions(indicatorsOptions);

                self.selectedIndicator('');
                self.filtration([]);
                //document.getElementById("filter").innerHTML = "";
                console.log(self.indicatorsOptions());
                hideall();
                document.getElementById("2").style.display = "block";
                this.selectedStepValue("stp2");
            };

            self.selectIndicator = function() {
                //console.log(self.selectedIndicator());
                var groupOptions = [];
                //indicatorsOptions.push({});
                for (var i = 0; i < self.selectedIndicator().dimensions.group.length; i++) {
                    groupOptions.push(self.selectedIndicator().dimensions.group[i]);
                }
                var valueOptions = [];
                //indicatorsOptions.push({});
                for (var i = 0; i < self.selectedIndicator().dimensions.value.length; i++) {
                    valueOptions.push(self.selectedIndicator().dimensions.value[i]);
                }


                self.valueOptions(valueOptions);
                self.groupOptions(groupOptions);
                self.selectedGroup('');
                self.selectedValue('');
                self.selectedStp4('');

                self.filtration(self.selectedIndicator().filtrationValues);
                //console.log(self.selectedIndicator().filtrationValues);
                hideall();
                document.getElementById("3").style.display = "block";
                this.selectedStepValue("stp3");
            };

            self.selectDimensions = function() {
                //                    console.log(self.selectedGroup());
                //                    console.log(self.selectedValue());
                hideall();
                document.getElementById("4").style.display = "block";
                this.selectedStepValue("stp4");
                console.log(self.filtration());
                // console.log(self.filtration().value);

            };



            this.updateLabelText = function(event) {
                var train = document.getElementById("train");
                self.selectedStepLabel(train.getStep(event.detail.value).label);
                //self.selectedStepLabel(model.step() + ' - ' + event.detail.value);

                hideall();
                document.getElementById(event.detail.value.substring(3)).style.display = "block";
            };

            self.stackValue = ko.observable('off');
            self.orientationValue = ko.observable('vertical');
            self.barSeriesValue = ko.observableArray();
            self.barGroupsValue = ko.observableArray();
            self.metadata = ko.observable();


            function callRestService() {
                console.log(self.selectedIndicator());

                $.ajax({
                    type: "POST",
                    url: "http://192.168.155.115:7001/BIRestService/resources/gastat/genericReport",
                    dataType: 'json',
                    data: JSON.stringify(self.selectedIndicator().param),
                    contentType: 'application/json',
                    success: function(response) {
                        var barSeries = [{ name: self.selectedIndicator().param.label(), items: response['value'] }];
                        var barGroups = response['group'];
                        self.barSeriesValue(barSeries);
                        self.barGroupsValue(barGroups);
                        try {
                            //callRestService();
                            document.getElementById("barChart").refresh();
                        } catch (err) {
                            console.log(err);
                        }
                    },
                    failure: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus);
                    }
                });
            };

            self.openMetadataURL = function() {
                cordova.InAppBrowser.open('http://192.168.155.113:8888/assets/imgs/bi-report.png', '_self', 'location=no,hardwareback=no');

            };


            self.makeFavourite = function() {
                document.querySelector('#modalDialog1').open();
            };

            self.prepareCanvas = function() {

                //context.fillRect(0, 0, canvas.width, canvas.height);
                //                     
                //                    context.fillRect(0, 0, 500, 500);
                //                    context.fillStyle = "#FF0000";
                //                    context.font = "30px Arial";
                //                    context.fillText("Hello World",0,600);
                //                    context.fillText("akjhs hda",0,660);



                html2canvas(document.getElementById("canvasMeta")).then(canvas => {

                    var destCanvas = document.getElementById("can3");

                    var destCtx = destCanvas.getContext("2d");
                    //                            var svg = document.querySelector("svg");
                    //                            canvg(destCanvas, svg.outerHTML);
                    destCanvas.height = canvas.height + 10;
                    destCanvas.width = $(window).width();
                    destCtx.drawImage(canvas, 0, 0);


                });

                html2canvas(document.getElementById("canvasDiv")).then(canvas => {

                    var destCanvas = document.getElementById("can2");
                    destCanvas.width = $(window).width();
                    destCanvas.height = canvas.height + 10;
                    var destCtx = destCanvas.getContext("2d");
                    destCtx.drawImage(destCanvas, 0, canvas.height);
                    destCtx.drawImage(canvas, 0, 0);

                    var can1 = document.getElementById("can1");
                    var context = can1.getContext("2d");
                    context.clearRect(0, 0, can1.width, can1.height);
                    can1.height = 0;
                    //
                    var svg = document.querySelector("svg");
                    ////                    //canvas.setAttribute("style","background-image: url(css/images/GaStat.png);");
                    //                     svg.setAttribute("width", "500px");
                    svg.setAttribute("height", "300px");
                    //////                    //console.log(svg.outerHTML);
                    canvg(can1, svg.outerHTML);
                    context.globalCompositeOperation = "destination-over";
                    context.fillStyle = '#fff'; // <- background color
                    var backgroundImage = new Image();
                    backgroundImage.src = 'css/images/masdar-watermark.png';
                    backgroundImage.setAttribute("syle", "background-position: center center;");
                    context.drawImage(backgroundImage, 0, 0);

                    var can4 = document.getElementById("can4");
                    var can3 = document.getElementById("can3");

                    can4.width = $(window).width();
                    can4.height = destCanvas.height + can1.height + can3.height + 10;
                    var drawContext = can4.getContext("2d");
                    drawContext.drawImage(destCanvas, 0, 0);
                    drawContext.drawImage(can1, 0, destCanvas.height);
                    drawContext.drawImage(can3, 0, destCanvas.height + can1.height);

                });
            };

            self.shareImg = function() {
                self.prepareCanvas();

                var canvas = document.getElementById("can4");

                window.plugins.socialsharing.share(
                    self.selectedIndicator().param.label(),
                    self.selectedIndicator().param.label(), canvas.toDataURL(), null);

                //document.removeChild(canvas)

            };
            self.saveImg = function() {
                self.prepareCanvas();
                var canvas = document.getElementById("can4");
                var params = { data: canvas.toDataURL(), prefix: 'GaStat_', format: 'PNG', quality: 100, mediaScanner: true };
                window.imageSaver.saveBase64Image(params,
                    function(filePath) {
                        alert('File saved on ' + filePath);
                    },
                    function(msg) {
                        alert(msg);
                    }
                );
            };
            self.loadFavoriteReport = function() {
                var id = self.selectedFavouriteReport();
                dbUtil.queryData(queryUtil.fetchReport, [id]).then(function(response) {
                    console.log('Report data has been fetched');
                    console.log(response);

                    var tempIndicatorId = response.rows.item(0).INDICATOR_ID;
                    var tempSAId = response.rows.item(0).SUBJECTAREA_ID;
                    var tempFilter = JSON.parse(response.rows.item(0).FILTER)


                    self.indicators(self.subjectAreas[tempSAId - 1].indicators);
                    self.selectedIndicator(self.subjectAreas[tempSAId - 1].indicators[tempIndicatorId - 1]);
                    self.filtration(self.selectedIndicator().filtrationValues);

                    self.dispSA = self.subjectAreas[tempSAId - 1].name;
                    self.dispIn = self.subjectAreas[tempSAId - 1].indicators[tempIndicatorId - 1].name;
                    self.dispDim = ko.observable('');
                    self.dispFil = ko.observable('');

                    console.log(self.filtration());

                    for (var i = 0; i < self.filtration().length; i++) {
                        for (var j = 0; j < self.selectedIndicator().param.filter.length; j++) {


                            if (self.selectedIndicator().param.filter[j].key == self.filtration()[i].key) {

                                if (self.filtration()[i].key == tempFilter[i].key) {
                                    self.selectedIndicator().param.filter[j].value = tempFilter[i].value;
                                }
                            }
                        }
                    }
                    self.metadata(self.selectedIndicator().metadata);
                    callRestService();
                    hideall();
                    document.getElementById("5").style.display = "block";
                    // this.selectedStepValue("stp5");

                });

            };

            self.handleAttached = function() {
                self.fetchReportNames();
            }

        };
        var trainModel = new TrainData();
        return TrainData;
    });