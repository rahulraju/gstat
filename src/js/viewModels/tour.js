/**
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */

// Tour page viewModel holding the tour launch page and tour content

'use strict';
define(['knockout', 'jquery', 'appController',
    'ojs/ojknockout'
], function(ko, $, app) {
    function tourViewModel() {
        var self = this;
        self.tourPage = ko.observable('tourLaunchPage');

        self.step = ko.observable(0);

        self.skipOrSignIn = ko.computed(function() {
            if (self.step() === 2) {
                return 'Dashboard';
            }
            return 'Skip';
        });

        self.startTour = function() {
            self.tourPage('tourContent');
        };

        self.filmStripOptionChange = function(event) {
            self.step(event.detail.value['index']);
        };


        self.handleActivated = function(info) {
            var firstTimeUser = true;
            if (firstTimeUser === 'false') {
                app.router.go('dashboard');
            }

        }

    }
    return tourViewModel;
});