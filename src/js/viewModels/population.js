/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * population module
 */
define(['ojs/ojcore', 'knockout', 'jquery','text!../properties/Indicators_Population', 'ojs/ojchart', 'ojs/ojmodel', 'ojs/ojbutton','ojs/ojfilmstrip','ojs/ojlabel',
    'ojs/ojtoolbar', 'ojs/ojselectcombobox','jet-composites/demo-chart-three-d-effect-control/1.0.0/loader','jet-composites/orientation/1.0.0/loader',
    'ojs/ojtable', 'ojs/ojarraytabledatasource', 'ojs/ojgauge' ,'masdar_util'
], function (oj, ko, $,pop_prop) {
    /**
     * The view model for the main content view template
     */
    function populationContentViewModel() {
        var self = this;
        
        
         self.stackValue = ko.observable('off');
        self.orientationValue = ko.observable('vertical');
        
        /* chart data */
        var barSeries = [{name: "Population", items: [31742308, 32552336]} ];
    
        var barGroups = ["2017", "2016"];
   
        self.barSeriesValue = ko.observableArray(barSeries);
        self.barGroupsValue = ko.observableArray(barGroups);
  
        //Slider page code
        self.indicators = ko.observableArray([]);
        
       self.indicators(getIndicatorJson(pop_prop));        
//       console.log(getIndicatorJson());
        
          self.pageContext = ko.computed(function(){
              
              var indi_Array = self.indicators();
              var p_in_code =sessionStorage.getItem('pop_indi_code');
              var index =0;
//              
              for(i=0;i<indi_Array.length;i++){
                  indi_Array[i].name(model.lang()?indi_Array[i].name_eng:indi_Array[i].name_arb); 
                   indi_Array[i].meta(model.lang()?indi_Array[i].meta_eng:indi_Array[i].meta_arb); 
                   if(indi_Array[i].in_code === p_in_code){
                      index =i; 
                    }
              }

           self.currDetailPage = ko.observable({"index": index});
           if(model.lang())
                    return indi_Array[0].name_eng;
                else
                    return 'indi_Array[0].name_arb';
            
            return ;
            
        });
        
        self.refresh = function(){
//            postbipCall('MobileIndicator_RPT',null,function(data){self.indicators(getIndicatorJson(data));

            self.reports = ko.observable(self.indicators()[0].indicators);     
            
            
            
//            });            
        };
        
        self.refresh();
        
//        self.reports = self.indicators()[0]? ko.observable(self.indicators()[0].indicators): ko.observable([]);
//        self.currDetailPage = ko.observable({"index": 0});
        
       
       


        handleDetailCreate = function ()
        {
            var filmStrip = document.getElementById('detailFilmStrip');
            var pagingModel = filmStrip.getPagingModel();
            pagingModel.on("beforePage", handleDetailBeforePage);
            handleDetailBeforePage();
        };

        handleDetailBeforePage = function (event)
        {

            var filmStrip = document.getElementById('detailFilmStrip');
            var pagingModel = filmStrip.getPagingModel();
            var pageCount = pagingModel.getPageCount();
            var pageIndex = event ? event.page : 0;

            //wrap in try..catch block because the detail filmstrip is 
            //initialized first and the master filmstrip is not yet 
            //available
            try
            {
                var masterFilmStrip = document.getElementById('masterFilmStrip');
                var masterPagingModel = masterFilmStrip.getPagingModel();
                var masterPage = masterPagingModel.getPage();
                var masterItemsPerPage = masterFilmStrip.getItemsPerPage();
                var newMasterPage =
                        Math.floor(pageIndex / masterItemsPerPage);
                //change master page in detail's beforePage event so that 
                //the master page change transitions at the same time as 
                //the detail page
                if (newMasterPage !== masterPage)
                    masterPagingModel.setPage(newMasterPage);
            } catch (e)
            {
                //do nothing
            }
        };

        masterClick = function (data, event)
        {
            //get the ko binding context for the item DOM element
            var context = ko.contextFor(event.target);
            //get the index of the item from the binding context
            var index = context.$index();
            
            //set currDetailPage in a timeout so that the page change 
            //transitions smoothly, otherwise it seems that there's some 
            //processing going on that interferes (maybe due to knockout)
            setTimeout(function () {
                self.currDetailPage({"index": index});
            }, 50);
        };

        getDetailItemInitialDisplay = function (index)
        {
            return index < 1 ? '' : 'none';
        };

        getMasterItemInitialDisplay = function (index)
        {
            return index < 3 ? '' : 'none';
        };

        getMasterItemLabelId = function (index)
        {
            return 'masterLabel' + index;
        };

        isMasterItemSelected = function (index)
        {
            
            return self.currDetailPage().index === index;
        };

        getMasterItemLabelledBy = function (index)
        {
            var labelledBy = getMasterItemLabelId(index);
            
            if (isMasterItemSelected(index)){
                
//                alert(index);
     sessionStorage.setItem('pop_indi_code',self.indicators()[index].in_code); 

    var vals = self.indicators()[index].seq_no;
    var series = model.lang()? self.indicators()[index].meta_eng : self.indicators()[index].meta_arb;
    
    
    var rs = series.split(";");
    var vs = vals.split(":");
    barSeries=[];
            
            for(i=0;i<rs.length;i++){
               barSeries.push( {name: rs[i], items: vs[i].split(";")} );
            }
  
        self.barSeriesValue(barSeries);
    
                self.reports(self.indicators()[index].indicators);
                labelledBy += " masterItemSelectedLabel";
                
                
            }
            return labelledBy;
        };
        
    }
    
    
    return populationContentViewModel;
});
