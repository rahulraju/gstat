/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * rating module
 */
define(['ojs/ojcore', 'knockout', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojgauge', 'masdar_util', 'ojs/ojanimation',
    'ojs/ojchart', 'ojs/ojselectcombobox', 'ojs/ojtabs'], function (oj, ko) {
    /**
     * The view model for the main content view template
     */
    function ratingContentViewModel() {
        var self = this;
        self.value = ko.observable();
        self.rating = ko.observable(0);
        self.readOnly = ko.observable(true);
        self.averageRt = ko.observable();
        self.transientValue = ko.observable();
        self.pieSeriesValue = ko.observableArray();
        self.pieGroupsValue = ko.observableArray();
        self.DeviceIp =  sessionStorage.getItem('DeviceIp');
        self.rat_update = false;
        self.thresholdValues = [
            {max: 0, shortDesc: 'No Rating'},
            {max: 1, shortDesc: 'Poor'},
            {max: 2, shortDesc: 'Needs Improvement'},
            {max: 3, shortDesc: 'Satisfactory'},
            {max: 4, shortDesc: 'Exceeds Expectations'},
            {max: 5, shortDesc: 'Outstanding'}];

//             localStorage.removeItem("RatingValue");
//             localStorage.removeItem("RatingComment");

        self.submit = function () {
            alert('Thanks for your feedback!');

            sessionStorage.setItem("RatingValue", self.rating());
            sessionStorage.setItem("RatingComment", self.value());
            self.averageRt((self.averageRt() + self.rating()) / 2);
            self.readOnly(true);
            jsonUrl =osbUrl+'MasdarRatingService/insert';
            var ratingComments = self.value();
            if(ratingComments)
            ratingComments = ratingComments.replace(/(\r\n|\n|\r|,|;)/gm,"");
            var insertRating = {
                "MasdarRating": [{
                        "userId": self.DeviceIp,
                        "rating": self.rating() ,
                        "ratingComments": ratingComments,
                        "creationDate": "3",
                        "updatedDate": "3"
                    }]};
            if(self.rat_update){
                delete insertRating.MasdarRating[0].creationDate;
                delete insertRating.MasdarRating[0].updatedDate;
                jsonUrl =osbUrl+'MasdarRatingService/update';
            }          
           
            doRestCall(jsonUrl, insertRating, self.rat_update?'put':'post', function(data){  self.refresh();}) ;

        };

        self.getDesc = ko.pureComputed(function () {
            if (self.transientValue())
                return  self.thresholdValues[Math.ceil(self.transientValue())].shortDesc;
            else
                return 'No Rating';
        });

        self.averageRating = ko.pureComputed(function () {
            if (self.averageRt())
                return  self.thresholdValues[Math.ceil((self.averageRt() ))].shortDesc;
            else
                return 'No Rating';
        });

        self.ratingChange = function (event) {

        };

        self.editRating = function () {
            self.readOnly(false);
        };
        self.back = function () {
            self.readOnly(true);
        };





        self.handleAttached = function (info) {

        };
        
        self.showingFront = true;
    
    self.animate = function() {
      var elem = document.getElementById('animateCont');
      
      // Determine startAngle and endAngle
      var startAngle = self.showingFront ? '0deg' : '180deg';
      var endAngle = self.showingFront ? '180deg' : '0deg';

      // Animate the element
      oj.AnimationUtils['flipOut'](elem, {'flipTarget': 'children',
                                          'persist': 'all',
                                          'startAngle': startAngle,
                                          'endAngle': endAngle});

      self.showingFront = !self.showingFront;
    };
        

        self.refresh = function () {
            var ratingVal = sessionStorage.getItem("RatingValue");
            var ratingCom = sessionStorage.getItem("RatingComment");
            if (ratingVal) {
                self.rating(Number.parseInt(ratingVal));
                self.readOnly(true);
            }
            if (ratingCom)
                self.value(ratingCom);

//        var cnt =getTotal(series,'items');
//        var total =0;
//        
//        var arrayVal = findItemFromResult(series,'items','$$$');

            var report = 'MasdarRating_RPT';
            var params = {"name": "P_USER_ID", "values": self.DeviceIp};

            postbipCall(report, params, function (data) {
//             alert(data);
                var ratingArray = getBipJson(data, 2);
                var usr_rating = Number(ratingArray[0].RATING);
                if(usr_rating < 0)
                  self.rat_update = false;
              else
                  self.rat_update = true;
                self.averageRt(Number(ratingArray[0].AVG_RATING));
                
                 self.rating(usr_rating < 0?0:usr_rating);
                ratingArray.splice(0, 1);
                var series = [];
                for (i = 0; i < ratingArray.length; i++) {
                    series.push({name: self.thresholdValues[Number(ratingArray[i].RATING)].shortDesc, items: [Number(ratingArray[i].AVG_RATING)]});
                } ;
                self.pieSeriesValue(series);
                document.getElementById("ratChart").refresh();
            });

//         for(i=0;i<arrayVal.length;i++)
//             total = total + arrayVal[i]*(i+1);
//         
////         alert(total);
////         alert(total/cnt);
//         self.averageRt(total/cnt);

        };


//         this.pieGroupsValue = ko.observableArray(["Group A","Group B"]);




        /* chart style defaults */
        self.styleDefaults = ko.pureComputed(function () {
            return {
                dataItemGaps: '100%',
                pieInnerRadius: 1,
                threeDEffect: 'on'
            };
        });

        self.refresh();

    }

    return ratingContentViewModel;
});