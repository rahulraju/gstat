/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your incidents ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojbutton', 'ojs/ojinputtext','ojs/ojmodel'],
        function (oj, ko, $) {

            function IncidentsViewModel() {
                var self = this;
                self.username = ko.observable('gastat');
                self.password = ko.observable('gastat');

                self.buttonClick = function (event, ui) {

//                        $( ".selector" ).ojPopup();
//                    if(self.username()==""){
//                        $( ".oj-hybrid-padding" ).ojPopup();
//                        return;
//                    }
                    var Member = oj.Model.extend({
                        idAttribute: "memberId"
                    });

                    var Members = oj.Collection.extend({
                        url: "http://192.168.1.5:7101/GaStatBackend/resources/gastat/staticLogin/"+self.username()+"?password="+self.password(),
                        type: 'GET',
                        contentType: "application/json",
//model: Member,
//    //used to generate authentication and config headers
//    customURL: rootViewModel.getHeaders,
                        // The parse function modifies the response object, extracting and returning the members array.
                        parse: function (members) {
                            return members.items;
                        }
                    });

                    var membersColl = new Members();

                    var emp = membersColl.fetch({
                        success: function (model, response, options) {
                            // model = fetched object  
                            //self.username(response['origin']);
                            $('.result').text(response['payload']);
                            console.log(response['payload']);
                        },
                        error: function (model, xhr, options) {
                            console.log("employee.fetch error");
                        }});

                    console.log("Create Clicked ");

                };
                // Below are a subset of the ViewModel methods invoked by the ojModule binding
                // Please reference the ojModule jsDoc for additional available methods.

                /**
                 * Optional ViewModel method invoked when this ViewModel is about to be
                 * used for the View transition.  The application can put data fetch logic
                 * here that can return a Promise which will delay the handleAttached function
                 * call below until the Promise is resolved.
                 * @param {Object} info - An object with the following key-value pairs:
                 * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
                 * @param {Function} info.valueAccessor - The binding's value accessor.
                 * @return {Promise|undefined} - If the callback returns a Promise, the next phase (attaching DOM) will be delayed until
                 * the promise is resolved
                 */
                self.handleActivated = function (info) {
                    // Implement if needed
                };

                /**
                 * Optional ViewModel method invoked after the View is inserted into the
                 * document DOM.  The application can put logic that requires the DOM being
                 * attached here.
                 * @param {Object} info - An object with the following key-value pairs:
                 * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
                 * @param {Function} info.valueAccessor - The binding's value accessor.
                 * @param {boolean} info.fromCache - A boolean indicating whether the module was retrieved from cache.
                 */
                self.handleAttached = function (info) {
                    // Implement if needed
                };


                /**
                 * Optional ViewModel method invoked after the bindings are applied on this View. 
                 * If the current View is retrieved from cache, the bindings will not be re-applied
                 * and this callback will not be invoked.
                 * @param {Object} info - An object with the following key-value pairs:
                 * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
                 * @param {Function} info.valueAccessor - The binding's value accessor.
                 */
                self.handleBindingsApplied = function (info) {
                    // Implement if needed
                };

                /*
                 * Optional ViewModel method invoked after the View is removed from the
                 * document DOM.
                 * @param {Object} info - An object with the following key-value pairs:
                 * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
                 * @param {Function} info.valueAccessor - The binding's value accessor.
                 * @param {Array} info.cachedNodes - An Array containing cached nodes for the View if the cache is enabled.
                 */
                self.handleDetached = function (info) {
                    // Implement if needed
                };
            }

            /*
             * Returns a constructor for the ViewModel so that the ViewModel is constructed
             * each time the view is displayed.  Return an instance of the ViewModel if
             * only one instance of the ViewModel is needed.
             */
            return new IncidentsViewModel();
        }
);
