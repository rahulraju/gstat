/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your about ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojknockout', 'ojs/ojbutton','ojs/ojfilmstrip','ojs/ojpopup',
    'ojs/ojaccordion', 'ojs/ojpagingcontrol'],
        function (oj, ko, $) {


            function InforgraphModel() {
        var self = this;
        
        self.curIndex = ko.observable('1');
        self.ftPagingModel = ko.observable(null);
         self.piPagingModel = ko.observable(null);
          self.popPagingModel = ko.observable(null);
           self.genPagingModel = ko.observable(null);
        self.StripOrient  = ko.observable('horizontal');      
//        self.StripOrient( $(window).width() > $(window).height() ? 'horizontal':'vertical');
        
        self.popImg = ko.pureComputed(function(){            
            return 'img/infographics/'+self.curIndex()+'.jpg';
        });
                
         self.ftImgStrip = [
          { name: 'Exports and imports of goods A' },
          { name: 'Exports and imports of goods B' },
          { name: 'Foreign Trade Customs Ports' },
           { name: 'Trade French AR' },
          { name: 'Trade Spain Ar' },
          { name: 'Trade UK Ar' },
          { name: 'Trade USA AR' }    
      ];
      
            self.piImgStrip = [
          { name: 'Average prices of goods and services' },
          { name: 'General Cost of Living Index (CPI)' },
          { name: 'General Index of Real Estate Prices' }    
      ];
      
       self.popImgStrip = [
          { name: 'Demographics of the Saudi Arabia' }    
      ];
      
     self.genImgStrip = [
          { name: '1' },
          { name: '2' },
          { name: '3' }    
      ];
      
       self.handleBindingsApplied = function()
      {
        var filmStrip_ft = document.getElementById('ftFilmStrip');
        var filmStrip_pi = document.getElementById('piFilmStrip');
        var filmStrip_pop = document.getElementById('popFilmStrip');
        var filmStrip_gen = document.getElementById('genFilmStrip');
        var busyContext = oj.Context.getContext(filmStrip_pi).getBusyContext();
        busyContext.whenReady().then(function ()
        {
          // Set the Paging Control pagingModel
          self.ftPagingModel(filmStrip_ft.getPagingModel());
          self.piPagingModel(filmStrip_pi.getPagingModel());
          self.popPagingModel(filmStrip_pop.getPagingModel());
          self.genPagingModel(filmStrip_gen.getPagingModel());
//                     var wHt = $(window).height();
//          $('#filmStrip').height(wHt - 115);
        });
      };
      

      
      getItemInitialDisplay = function(index)
      {

          return index < 2 ? '' : 'none';
      };
      
      
      
      zoomImg = function(index){
//          alert(index);
          self.curIndex(index.name);
         var popup = document.querySelector("#popup1");
        popup.open();
        var wHt = $(window).height();
        var pHt = $(popup).height();
       $(popup).css('top',(wHt-pHt)/2);
       //  alert(wHt);
      };

      

                    //  $("#accordion").accordion({  active: false,collapsible: true});

            }
            return new InforgraphModel();
            
            
        }
                

);
