/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * biDashboard module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojchart', 'ojs/ojmodel', 'ojs/ojbutton', 'ojs/ojanimation','ojs/ojdefer', 'ojs/ojpopup',
    'ojs/ojtoolbar', 'ojs/ojselectcombobox', 'jet-composites/demo-chart-three-d-effect-control/1.0.0/loader', 'jet-composites/orientation/1.0.0/loader',
    'ojs/ojtable', 'ojs/ojarraytabledatasource', 'ojs/ojgauge', 'ojs/ojfilmstrip', 'ojs/ojinputtext','masdar_util'
], function (oj, ko, $) {
    /**
     * The view model for the main content view template
     */

    function biDashboardContentViewModel() {
        var self = this;
        self.stackValue = ko.observable('off');
        self.DeviceIp = sessionStorage.getItem('DeviceIp');
        self.orientationValue = ko.observable('vertical');
        self.threeDValue = ko.observable('off');
        self.viewCnt = ko.observable('');
        self.myViews = 0;
        self.ratSeriesValue = ko.observableArray([]);
        var resultArray = [];
        var allResults = [];
        self.barSeriesValue = ko.observableArray();
        self.barGroupsValue = ko.observableArray();
        self.avgRating = ko.observable(0);
        self.myRating = ko.observable(0);
        self.avgRatingValue = ko.observable();
        self.myRatingValue = ko.observable();
        self.myRatingComments = ko.observable();
        self.rat_update = false;

        self.drillingOption = ko.observable('off');
        self.drillY = ko.observable();
        self.drillH = ko.observable();
        self.drillQ = ko.observable();


        self.thresholdValues = [
            {max: 0, shortDesc: 'No Rating'},
            {max: 1, shortDesc: 'Poor'},
            {max: 2, shortDesc: 'Needs Improvement'},
            {max: 3, shortDesc: 'Satisfactory'},
            {max: 4, shortDesc: 'Exceeds Expectations'},
            {max: 5, shortDesc: 'Outstanding'}];

        self.type = ko.observable('bar');


        self.compareValue = ko.observable('5');
        self.compareOptions = ko.observableArray();

        self.checkCompare = function (event) {
            if (event.detail.value.length > 0) {
                self.type('bar');
            }

        };
        self.routing = function () {
            oj.Router.rootInstance.go('indicator-pro');
        };
        self.dispFil = ko.observable('');

        self.isCompare = ko.pureComputed(function () {
            return self.compareValue() && self.compareValue().length > 1;
        });

        self.compareYear = ko.observableArray([
            {label: model.compare(),
                children: [
                    {value: "2", label: model.yrs2()},
                    {value: "3", label: model.yrs3()},
                    {value: "5", label: model.yrs5()}
                ]},
            {label: model.year(),
                children: [
                    {value: "2017", label: "2017"},
                    {value: "2016", label: "2016"},
                    {value: "2015", label: "2015"},
                    {value: "2014", label: "2014"},
                    {value: "2013", label: "2013"},
                    {value: "2012", label: "2012"},
                    {value: "2011", label: "2011"},
                    {value: "2010", label: "2010"},
                    {value: "2009", label: "2009"}

                ]}
        ]);

        self.grpOpt = ko.observableArray([
            {label: "Year",
                children: [
                    {value: "M", label: "Monthly"},
                    {value: "Q", label: "Quarterly"},
                    {value: "H", label: "Half Yearly"},
                    {value: "Y", label: "Annual"}
                ]}
        ]);

        self.grpValue = ko.observable('Q');

        self.uomOpt = ko.observableArray([
            {label: "Units",
                children: [
                    {value: "Ton", label: "Ton"},
                    {value: "Saudi riyal", label: "Saudi riyal"}

                ]}
        ]);

        self.uomValue = ko.observable('Saudi riyal');

        self.reportParam = sessionStorage.getItem('report_param');

        if (self.reportParam)
            self.reportParam = JSON.parse(self.reportParam);

//        sessionStorage.setItem('report_param',data);




        var indi_group_code = self.reportParam.group_code;
        var indi_group_eng = self.reportParam.group_eng;
        var indi_group_arb = self.reportParam.group_arb;

        self.indi_groups = [];
        self.indi_group_vis = ko.observable(true);
        if (indi_group_code && indi_group_code.length > 1) {
            indi_group_code = indi_group_code.split(";");
            indi_group_eng = indi_group_eng.split(";");
            indi_group_arb = indi_group_arb.split(";");
            for (i = 0; i < indi_group_eng.length; i++)
                self.indi_groups.push({name: model.lang() ? indi_group_eng[i] : indi_group_arb[i]});

        } else
            self.indi_group_vis(false);

        self.currentNavArrowPlacement = ko.observable("adjacent");
        self.currentNavArrowVisibility = ko.observable("auto");

        getItemInitialDisplay = function (index)
        {
            return index < 3 ? '' : 'none';
        };

        selIndicator = function (data, event) {
            self.currDetailPage({"index": data});
            self.applyFilter();
        };
        self.currDetailPage = ko.observable({"index": 0});

        isItemSelected = function (index)
        {
            return self.currDetailPage().index === index;
        };




        function callRestService() {
            var p_indi_code = localStorage.getItem('indicatorCode');
            if (indi_group_code && indi_group_code.length > 1)
                p_indi_code = indi_group_code[self.currDetailPage().index];
            var report = 'IndicatorReport_RPT';
            var params = [{name: "P_INDICATOR", values: p_indi_code}, {name: "P_YEAR", values: "1"}, {name: "P_COMP_YEAR", values: self.compareValue()}];
            postbipCall(report, params,
                    function (response) {

                        resultArray = getBipJson(response, 3);
                        var filterArray = resultArray;
//                        filterArray = groupData(filterArray, self.grpValue());
                        var groupCol = model.lang() ? 'G1_ENG' : 'G1_ARB';
                        self.drillingOption("groupsOnly");
//                        if(p_indi_code.includes('MOF_')){
//                            groupCol='indicator';
//                            self.drillingOption('off');
//                            }
                        var result = getBarGraph(filterArray, groupCol);

                        self.barSeriesValue(result.seriesVal);
                        self.barGroupsValue(result.groupVal);
                        try {
                            //callRestService();
                            document.getElementById("barChart").refresh();
                        } catch (err) {
                            console.log(err);
                        }
                    });

        }

        self.chartDrillYear = function (event) {
            var filterArray;
            if (event) {
                var detail = event.detail;
                if (detail['group']) {
                    var drillSel = detail['group'];
                    if (drillSel.length > 2) {
                        self.drillY(drillSel);
                        filterArray = doFilterSearch(['YEAR'], [drillSel], resultArray);
                    }
                }

            }
            try {
                var groupCol = model.lang() ? 'G1_ENG' : 'G1_ARB';
                var result = getBarGraph(filterArray, groupCol);
                self.barSeriesValue(result.seriesVal);
                self.barGroupsValue(result.groupVal);
                self.drillingOption("off");
                document.getElementById("barChart").refresh();
            } catch (err) {
                console.log(err);
            }

        };

        self.chartDrill = function (event) {
            var filterArray = doFilterSearch(['uom'], [self.uomValue()], resultArray);
            self.drillingOption("on");
            if (event) {
                var detail = event.detail;
                if (detail['group']) {
                    var drillSel = detail['group'];
                    if (drillSel.length > 2) {
                        self.drillY(drillSel);
                        filterArray = doFilterSearch(['year'], [drillSel], filterArray);
                        if (!detail['series'])
                            filterArray = groupData(filterArray, 'H');
                    }
                }
                if (detail['series'])
                {
                    var drillSel = detail['series'];

                    self.drillH(drillSel);
                    filterArray = doFilterSearch(['year'], [self.drillY()], filterArray);
                    if (drillSel.charAt(0) === 'H') {
                        filterArray = groupData(filterArray, 'Q');
                        if (drillSel.charAt(1) === '1')
                            filterArray = doFilterSearch(['group', 'group'], ['Q1', 'Q2'], filterArray, false, true);
                        else
                            filterArray = doFilterSearch(['group', 'group'], ['Q3', 'Q4'], filterArray, false, true);
                    } else if (drillSel.charAt(0) === 'Q') {
                        var num = drillSel.charAt(1);
                        self.drillingOption("groupsOnly");

                        filterArray = groupData(filterArray, 'M');
                        filterArray = doFilterSearch(['group', 'group', 'group'], [num * 3 - 2, num * 3 - 1, num * 3], filterArray, true, true);
                    }
//                filterArray = doFilterSearch(['group'],[drillSel],filterArray);  


                } else {
//                       filterArray = groupData(filterArray,self.grpValue());
                }
            }
            try {
                var result = getGraphData(filterArray);
                self.barSeriesValue(result.seriesVal);
                self.barGroupsValue(result.groupVal);

                //callRestService();
                document.getElementById("barChart").refresh();
            } catch (err) {
                console.log(err);
            }

        };

        function callRestService1(compare) {

            self.reportParam.type = self.type();
            if (self.reportParam.type.length == 1) {
                parameter().type = parameter().type[0];
            }
            if (!compare)
                if (parameter().filter[0].value === self.compareValue())
                    self.compareValue('');
            //console.log(JSON.stringify(parameter()));

            doRestCall('http://192.168.154.148:7001/BIRestService/resources/gastat/genericReport', parameter(), 'post', function (response) {

                var barSeries = [{name: compare ? parameter().filter[0].value : parameter().label(), items: response['value']}];
                if (compare) {
                    barSeries.push(compare);
                    parameter().filter[0].value = compare.name;
                } else {
                    if (self.compareValue() && self.compareValue().length > 1) {
                        var oldYear = parameter().filter[0].value;
                        parameter().filter[0].value = self.compareValue();
                        callRestService({name: oldYear, items: response['value']});
                    }
                }

                var barGroups = response['group'];
                //var colorHandler = new oj.ColorAttributeGroupHandler();
                //console.log(parameter().type);
                if (parameter().type == "pyramid" || parameter().type == "pie") {
                    console.log(response);
                    self.barSeriesValue(response['value']);
                } else {
                    //console.log(response['value']);
                    self.barSeriesValue(barSeries);
                    //self.barSeriesValue(getBarSeries(colorHandler));
                }
                self.type(parameter().type);
                self.barGroupsValue(barGroups);
                try {
                    //callRestService();
                    document.getElementById("barChart").refresh();
                } catch (err) {
                    console.log(err);
                }
            });

//            $.ajax({
//                type: "POST",
//                cache: false,
//                url: "http://192.168.155.115:7001/BIRestService/resources/gastat/genericReport",
//                dataType: 'json',
//                data: JSON.stringify(parameter()),
//                contentType: 'application/json',
//                success: function(response) {
//                    var barSeries = [{ name: parameter().label(), items: response['value'] }];
//                    var barGroups = response['group'];
//                    //var colorHandler = new oj.ColorAttributeGroupHandler();
//                    //console.log(parameter().type);
//                    if (parameter().type == "pyramid" || parameter().type == "pie") {
//                        console.log(response);
//                        self.barSeriesValue(response['value']);
//                    } else {
//                        //console.log(response['value']);
//                        self.barSeriesValue(barSeries);
//                        //self.barSeriesValue(getBarSeries(colorHandler));
//                    }
//                    self.type(parameter().type);
//                    self.barGroupsValue(barGroups);
//                    try {
//                        //callRestService();
//                        document.getElementById("barChart").refresh();
//                    } catch (err) {
//                        console.log(err);
//                    }
//                },
//                failure: function(jqXHR, textStatus, errorThrown) {
//                    console.log(textStatus);
//                }
//            });
        }
        ;
//        callRestService();


        //        var metadataArray = ko.observableArray();
        //        self.datasource = ko.observable(); //new oj.ArrayTableDataSource(metadataArray(),{idAttribute: 'Column4'});
        //        var metadata = oj.Collection.extend({
        //            url: "http://192.168.155.17:8888/BIRestService/resources/gastat/getMetadata?path=/shared/Economic%20Statistics/Prices/MD/MD_FULL"
        //        });
        //        var metadaColl = new metadata();
        //        metadaColl.fetch({
        //            success: function (model, response, options) {
        //                metadataArray(response['Row']);
        //                //console.log(metadataArray());
        //                self.datasource(new oj.ArrayTableDataSource(metadataArray(), {idAttribute: 'Column4'}));
        //
        //            },
        //            error: function (model, xhr, options) {
        //                alert("Can't Connect Server");
        //            }});

        self.applyFilter = function () {
//            alert(self.currDetailPage());
            callRestService();
//	  self.dispFil('');
//            for (var i = 0; i < filtrationValues().length; i++) {
//                for (var j = 0; j < parameter().filter.length; j++) {
//                    //console.log(parameter().filter[j].key + " - " + filtrationValues()[i].key);
//                    if (parameter().filter[j].key == filtrationValues()[i].key) {
//                        if (filtrationValues()[i].value.length == 1) {
//                            parameter().filter[j].value = filtrationValues()[i].value[0];
//                            self.dispFil(self.dispFil() + ' / ' + filtrationValues()[i].value[0]);
//                        } else {
//                            parameter().filter[j].value = filtrationValues()[i].value;
//                            self.dispFil(filtrationValues()[i].value);
//                        }
//                    }
//                }
//            }
//            callRestService();
            //console.log(parameter());

        };

        self.openMetadataURL = function () {
                                    var my_rat= self.myRating();
            self.myRating(3);
            self.avgRating(3);
//            self.myRating(my_rat);

            window.open(getContextUrl());
//            cordova.InAppBrowser.open('http://192.168.155.113:8888/assets/imgs/bi-report.png', '_self', 'location=no,hardwareback=no');

        };

        self.showingFront = true;

        self.saveRating = function () {
            self.updateRating();
            self.animateRating();
        };
        
        self.ratingPop = function(){
                 var popup = document.querySelector("#ratingPop");
        popup.open('#ratD');
    };

        self.animateRating = function () {
//
            $("#ratingBack").toggle();
            $("#ratingFront").toggle();


            var elem = document.getElementById('animateCont');

            // Determine startAngle and endAngle
            var startAngle = self.showingFront ? '0deg' : '180deg';
            var endAngle = self.showingFront ? '180deg' : '0deg';

            // Animate the element
            oj.AnimationUtils['flipOut'](elem, {'flipTarget': 'children',
                'persist': 'all',
                'startAngle': startAngle,
                'endAngle': endAngle});

            self.showingFront = !self.showingFront;
                        var my_rat= self.myRating();
            self.myRating(3);
            self.myRating(my_rat);

        };






//        self.shareImg = function() {
//            var canvas = document.querySelector("canvas");
//            var svg = document.querySelector("svg");
//            svg.setAttribute("height", "300px");
//            canvg(canvas, svg.outerHTML);
//
//
//            //console.log(canvas.toDataURL());
//
//            var context = canvas.getContext("2d");
//
//            context.globalCompositeOperation = "destination-over";
//            context.fillStyle = '#fff'; // <- background color
//            var backgroundImage = new Image();
//            backgroundImage.src = 'css/images/masdar-watermark.png';
//            backgroundImage.setAttribute("syle", "background-position: center center;");
//            context.drawImage(backgroundImage, 0, 0);
//            //context.fillRect(0, 0, canvas.width, canvas.height);
//            context.fillRect(0, 0, 500, 500);
//
//            console.log(canvas.toDataURL());
//
//            window.plugins.socialsharing.share(parameter().label(), parameter().label(), canvas.toDataURL(), null);
//            callRestService();
//            //document.removeChild(canvas)
//
//        };

        self.updateRating = function () {

            jsonUrl = osbUrl+'MasdarAudit/insert';
            var insertRating = {
                "MasdarAuditLog": [{
                        "userId": self.DeviceIp,
                        "indicator": localStorage.getItem('indicatorCode'),
                        "viewCount": self.myViews + 1,
                        "rating": self.myRating(),
                        "ratingComments": self.myRatingComments(),
                        "attribute1": "string",
                        "attribute2": "string",
                        "creationDate": "2008-09-29T04:49:45",
                        "updatedDate": "2014-09-19T02:18:33"
                    }]
            };
            if (self.rat_update) {
                delete insertRating.MasdarAuditLog[0].creationDate;
                delete insertRating.MasdarAuditLog[0].updatedDate;
                jsonUrl = osbUrl+'MasdarAudit/update';
            }

            doRestCall(jsonUrl, insertRating, self.rat_update ? 'put' : 'post', function (data) {
                self.rat_update=true;

                ;
            });

        };

        self.refresh = function () {

            var report = 'MasdarRating_RPT';

            var params = [{"name": "P_INDICATOR", "values": localStorage.getItem('indicatorCode')}, {"name": "P_USER_ID", "values": self.DeviceIp}];

            postbipCall(report, params, function (data) {
                var ratingArray = getBipJson(data, 2);
                self.viewCnt(Number(ratingArray[0].TOTAL_VIEWS));
                self.avgRating(Number(ratingArray[0].AVG_RATING));
                self.myViews =Number(ratingArray[0].MY_VIEWS);
                self.myRatingComments(ratingArray[0].RATING_COMMENTS);

                    self.rat_update = ratingArray[0].RATING > -1;
                self.myRating(self.rat_update ? Number(ratingArray[0].RATING):0);
                
                ratingArray.splice(0, 1);
                var series = [];
                for (i = 0; i < ratingArray.length; i++) {
                    series.push({name: self.thresholdValues[Number(ratingArray[i].RATING)].shortDesc, items: [Number(ratingArray[i].AVG_RATING)]});
                } ;
                self.ratSeriesValue(series);
  
                self.updateRating('load');
            });
        };
        self.refresh();
        self.applyFilter();



//        self.saveImg = function() {
//            var canvas = document.querySelector("canvas");
//
//            var svg = document.querySelector("svg");
//            //canvas.setAttribute("style","background-image: url(css/images/GaStat.png);");
//
//            svg.setAttribute("height", "300px");
//            //console.log(svg.outerHTML);
//            canvg(canvas, svg.outerHTML);
//
//
//            var context = canvas.getContext("2d");
//
//            context.globalCompositeOperation = "destination-over";
//            context.fillStyle = '#fff'; // <- background color
//            var backgroundImage = new Image();
//            backgroundImage.src = 'css/images/masdar-watermark.png';
//            backgroundImage.setAttribute("syle", "background-position: center center;");
//            context.drawImage(backgroundImage, 0, 0);
//            //context.fillRect(0, 0, canvas.width, canvas.height);
//            context.fillRect(0, 0, 500, 500);
//
//
//            var params = { data: canvas.toDataURL(), prefix: 'GaStat_', format: 'PNG', quality: 100, mediaScanner: true };
//            window.imageSaver.saveBase64Image(params,
//                function(filePath) {
//                    alert('File saved on ' + filePath);
//                },
//                function(msg) {
//                    alert(msg);
//                }
//            );
//            callRestService();
//
//        };




        self.prepareCanvas = function () {

            //context.fillRect(0, 0, canvas.width, canvas.height);
//                     
//                    context.fillRect(0, 0, 500, 500);
//                    context.fillStyle = "#FF0000";
//                    context.font = "30px Arial";
//                    context.fillText("Hello World",0,600);
//                    context.fillText("akjhs hda",0,660);





//     document.getElementById("canvasMeta").style.fontSize='6px';
            var can1 = document.getElementById("can1");
            var context = can1.getContext("2d");
            context.clearRect(0, 0, can1.width, can1.height);
            can1.height = 0;
            var svg = document.querySelector("svg");

            svg.setAttribute("height", "400px");
//////                    //console.log(svg.outerHTML);
            canvg(can1, svg.outerHTML);
            context.globalCompositeOperation = "destination-over";
            context.fillStyle = '#fff'; // <- background color
            var backgroundImage = new Image();
            backgroundImage.src = 'css/images/masdar-watermark.png';
            backgroundImage.setAttribute("syle", "background-position: center center;");
            context.drawImage(backgroundImage, 0, 0);
            document.getElementById("can2").width = can1.width;
            document.getElementById("can3").width = can1.width;
            wrapText('can3', '   ' + model.lang() ? self.reportParam.meta_eng : self.reportParam.meta_arb, 16);
            var text2 = '' + model.lang() ? self.reportParam.name_eng : self.reportParam.name_arb + ' <br> ' + model.filtration() + ':' + self.dispFil();
            wrapText('can2', text2, 16);
            var canVAAS = clubCanvas(['can2', 'can1', 'can3'], 'can4');
            console.log(canVAAS.toDataURL());


//                    html2canvas(document.getElementById("canvasMeta"),useWidth,useHeight).then(canvas => {
//                        
//
//                        var destCanvas = document.getElementById("can3");
//
//                        var destCtx = destCanvas.getContext("2d");
////                            var svg = document.querySelector("svg");
////                            canvg(destCanvas, svg.outerHTML);
////                           destCtx.font = "6px Arial";
//                        destCanvas.height = canvas.height;
//                        destCanvas.width = 500;
//                        destCtx.drawImage(canvas, 0, 0);
//                        document.getElementById("canvasMeta").style.fontSize='';
//
//
//                    });


// document.getElementById("canvasDiv").style.fontSize='6px';
//                    html2canvas(document.getElementById("canvasDiv")).then(canvas => {      
//                         document.getElementById("canvasDiv").style.fontSize='';
//                        var destCanvas = document.getElementById("can2");
//                        destCanvas.width = $(window).width();
//                        destCanvas.height = canvas.height + 10;
//                        var destCtx = destCanvas.getContext("2d");
//                        destCtx.drawImage(destCanvas, 0, canvas.height);
//                        destCtx.drawImage(canvas, 0, 0);

//                        var can1 = document.getElementById("can1");
//                        var context = can1.getContext("2d");
//                        context.clearRect(0, 0, can1.width, can1.height);
//                        can1.height = 0;
////
//                        var svg = document.querySelector("svg");
//////                    //canvas.setAttribute("style","background-image: url(css/images/GaStat.png);");
////                     svg.setAttribute("width", "500px");
//                        svg.setAttribute("height", "300px");
////////                    //console.log(svg.outerHTML);
//                        canvg(can1, svg.outerHTML);
//                        context.globalCompositeOperation = "destination-over";
//                        context.fillStyle = '#fff'; // <- background color
//                        var backgroundImage = new Image();
//                        backgroundImage.src = 'css/images/masdar-watermark.png';
//                        backgroundImage.setAttribute("syle", "background-position: center center;");
//                        context.drawImage(backgroundImage, 0, 0);



//                        var can4 = document.getElementById("can4");
//                        var can3 = document.getElementById("can3");
//
//                        can4.width = $(window).width();
//                        can4.height = destCanvas.height + can1.height + can3.height;
//                        var drawContext = can4.getContext("2d");
//                        drawContext.drawImage(destCanvas, 0, 0);
//                        drawContext.drawImage(can1, 0, destCanvas.height);
//                        drawContext.drawImage(can3, 0, destCanvas.height + can1.height);

//                    });
        };




        self.shareImg = function () {
            self.prepareCanvas();

            var canvas = document.getElementById("can4");
            try {
                window.plugins.socialsharing.share(
                        self.reportParam.name_eng,
                        self.reportParam.name_eng, canvas.toDataURL(), null);
            } catch (err) {
                alert(err);
            }

            //document.removeChild(canvas)

        };
        self.saveImg = function () {
            self.prepareCanvas();
            var canvas = document.getElementById("can4");

            var params = {data: canvas.toDataURL(), prefix: 'GaStat_', format: 'PNG', quality: 100, mediaScanner: true};
            window.imageSaver.saveBase64Image(params,
                    function (filePath) {
                        alert('File saved on ' + filePath);
                    },
                    function (msg) {
                        alert("Please go to the mobile settings and give store permission of the Masadar App");
                    }
            );

        };

        $(function ()
        {
            $( "#ratingBack" ).delay( 800 ).hide(400 );
                 
//        $("#ratingBack").hide();
        });
    }

    return biDashboardContentViewModel;

});