/**
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */

// view model for the tour content with filmstrip
'use strict';
define(['knockout', 'jquery', 'ojs/ojfilmstrip', 'ojs/ojpagingcontrol'], function(ko, $) {
    function tourContentViewModel() {
        var self = this;

        self.pagingModel = ko.observable(null);

        // todo: need to fix the animation so that the paging model is set before the transition occurs
        self.handleAttached = function() {
            var filmStrip = document.getElementById("filmStrip");
            oj.Context.getContext(filmStrip).getBusyContext().whenReady().then(function() {
                self.pagingModel(filmStrip.getPagingModel());
            });
        };

        self.steps = [{
                'title': model.commercialRegister(),
                'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                'imgSrc': 'icon icon-graph',
                'color': '#4493cd'
            },
            {
                'title': model.establishments(),
                'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                'imgSrc': 'icon icon-dashboard',
                'color': '#FFD603'
            },
            {
                'title': model.foreignTrade(),
                'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                'imgSrc': 'icon icon-graph',
                'color': '#E5003E'
            }
        ];

        self.getItemInitialDisplay = function(index) {
            return index < 1 ? '' : 'none';
        };

    }
    return tourContentViewModel;
});