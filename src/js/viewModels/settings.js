/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * settings module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojbutton', 'ojs/ojinputtext',
    'ojs/ojslider', 'ojs/ojselectcombobox', 'translation'], function (oj, ko) {
    /**
     * The view model for the main content view template
     */
    function settingsContentViewModel() {
        var self = this;
        self.theme = ko.observable();

        self.langtxt = ko.observable(oj.Translations.getTranslatedString('lang'));
        self.themetxt = ko.observable(oj.Translations.getTranslatedString('theme'));
        self.fontSizetxt = ko.observable(oj.Translations.getTranslatedString('fontSize'));



        self.lang = ko.observable();
        self.fontSize = ko.observable();
        self.max = ko.observable(50);
        self.min = ko.observable(10);
        self.value = ko.observable(20);
        self.step = ko.observable(1);

        $(document).ready(function () {
            console.log('hello');
            $("#span1").css('font-size', self.value() + 'px');
        });


        self.valueChangedListener = function (event, ui) {
            console.log("Create Clicked ");
            $("#span1").css('font-size', self.value() + 'px');
        };

        self.stateOptionChangedHandler = function (event, ui) {
            console.log("Option Selected");
            $(".oj-web-applayout-header").css('background-color', self.theme());
        };

        

        self.allowNotification = function () {


//            alert(pushNoti);
//            pushNoti.register(
//                    successHandler,
//                    errorHandler,
//                    {
//                        "senderID": "1032138992519",
//                        "ecb": "onNotification"
//                    });
//            alert('end');
        };
    }

    return settingsContentViewModel;
});
