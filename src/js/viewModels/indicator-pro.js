/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * indicator-pro module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojknockout', 'ojs/ojfilmstrip', 'ojs/ojlabel','masdar_util'
], function (oj, ko, $) {

    /**
     * The view model for the main content view template
     */
    function indicatorproContentViewModel() {
        var self = this;
        
        //Slider page code
        self.indicators = ko.observableArray([]);
        
       self.indicators(getKeyIndicatorJson());        
        
          self.pageContext = ko.computed(function(){
              
              var indi_Array = self.indicators();
              var p_in_code =localStorage.getItem('indicatorCode');
              var index =0;
//              
              for(i=0;i<indi_Array.length;i++){
                  indi_Array[i].name(model.lang()?indi_Array[i].name_eng:indi_Array[i].name_arb); 
                   indi_Array[i].meta(model.lang()?indi_Array[i].meta_eng:indi_Array[i].meta_arb); 
                   if(indi_Array[i].in_code === p_in_code){
                      index =i; 
                    }
                  
                  if(indi_Array[i].indicators.length >0){
                      var sub_indi = indi_Array[i].indicators;
                      for(j=0;j<sub_indi.length;j++)
                      {
                          sub_indi[j].name(model.lang()?sub_indi[j].name_eng:sub_indi[j].name_arb); 
                          sub_indi[j].meta(model.lang()?sub_indi[j].meta_eng:sub_indi[j].meta_arb); 
                           if(sub_indi[j].in_code === p_in_code){
                             index =i; 
                    }
                      }
                  }
              }
              

            self.currDetailPage = ko.observable({"index": index});
           if(model.lang())
                    return indi_Array[0].name_eng;
                else
                    return 'indi_Array[0].name_arb';
            
            return ;
            
        });
        
        self.refresh = function(){
//            postbipCall('MobileIndicator_RPT',null,function(data){self.indicators(getIndicatorJson(data));

            self.reports = ko.observable(self.indicators()[0].indicators);       
//            });            
        };
        
        self.refresh();
        
//        self.reports = self.indicators()[0]? ko.observable(self.indicators()[0].indicators): ko.observable([]);
//        self.currDetailPage = ko.observable({"index": 0});
        
       
        self.routing = function (data) {
            oj.Router.rootInstance.go('dynamicReport');
            parameter(data);
            sessionStorage.setItem('report_param',JSON.stringify(data));
            localStorage.setItem('indicatorCode',data.in_code); 
        };
        


        handleDetailCreate = function ()
        {
            var filmStrip = document.getElementById('detailFilmStrip');
            var pagingModel = filmStrip.getPagingModel();
            pagingModel.on("beforePage", handleDetailBeforePage);
            handleDetailBeforePage();
        };

        handleDetailBeforePage = function (event)
        {

            var filmStrip = document.getElementById('detailFilmStrip');
            var pagingModel = filmStrip.getPagingModel();
            var pageCount = pagingModel.getPageCount();
            var pageIndex = event ? event.page : 0;

            //wrap in try..catch block because the detail filmstrip is 
            //initialized first and the master filmstrip is not yet 
            //available
            try
            {
                var masterFilmStrip = document.getElementById('masterFilmStrip');
                var masterPagingModel = masterFilmStrip.getPagingModel();
                var masterPage = masterPagingModel.getPage();
                var masterItemsPerPage = masterFilmStrip.getItemsPerPage();
                var newMasterPage =
                        Math.floor(pageIndex / masterItemsPerPage);
                //change master page in detail's beforePage event so that 
                //the master page change transitions at the same time as 
                //the detail page
                if (newMasterPage !== masterPage)
                    masterPagingModel.setPage(newMasterPage);
            } catch (e)
            {
                //do nothing
            }
        };

        masterClick = function (data, event)
        {
            //get the ko binding context for the item DOM element
            var context = ko.contextFor(event.target);
            //get the index of the item from the binding context
            var index = context.$index();
            
            //set currDetailPage in a timeout so that the page change 
            //transitions smoothly, otherwise it seems that there's some 
            //processing going on that interferes (maybe due to knockout)
            setTimeout(function () {
                self.currDetailPage({"index": index});
            }, 50);
        };

        getDetailItemInitialDisplay = function (index)
        {
            return index < 1 ? '' : 'none';
        };

        getMasterItemInitialDisplay = function (index)
        {
            return index < 3 ? '' : 'none';
        };

        getMasterItemLabelId = function (index)
        {
            return 'masterLabel' + index;
        };

        isMasterItemSelected = function (index)
        {
            
            return self.currDetailPage().index === index;
        };

        getMasterItemLabelledBy = function (index)
        {
            var labelledBy = getMasterItemLabelId(index);
            
            if (isMasterItemSelected(index)){
                
                self.reports(self.indicators()[index].indicators);
            sessionStorage.setItem('SA_EN',self.indicators()[index].name_eng);
            sessionStorage.setItem('SA_AR',self.indicators()[index].name_arb);
            this.text()['dynamicReport'](model.lang()? self.indicators()[index].name_eng : self.indicators()[index].name_arb);
                labelledBy += " masterItemSelectedLabel";
                
            }
            return labelledBy;
        };
        
        

    } ;





    return indicatorproContentViewModel;

});