/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * biDashboard module
 */
define(['ojs/ojcore', 'knockout', 'ojs/ojchart', 'ojs/ojmodel', 'ojs/ojbutton'
], function (oj, ko) {
    /**
     * The view model for the main content view template
     */
    function biDashboardContentViewModel() {
        var self = this;
        var self = this;
        self.threeDValue = ko.observable('on');
        
        
        /* chart data */
        self.pyramidSeries = ko.observable();
        self.pyramidSeriesValue = ko.observableArray(self.pyramidSeries());


            var Report = oj.Collection.extend({
                url: "http://192.168.1.136:7101/BIRestService/resources/gastat/exportBySections"
            });
            var membersColl = new Report();

            membersColl.fetch({
                success: function (model, response, options) {
                    self.pyramidSeries(response);
        self.pyramidSeriesValue(self.pyramidSeries()['2016']);
                },
                error: function (model, xhr, options) {
                    alert("employee.fetch error - " + model);
                }});
    }

    return biDashboardContentViewModel;
});
