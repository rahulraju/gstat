
SELECT
    saw_0 AS year,
    saw_1 AS month,
    saw_2 AS g1_eng,
    saw_3 AS g1_arb,
    CAST(saw_4 AS double) AS value
FROM
    (
        ( SELECT
            "Time"."Year" saw_0,
            "Time"."Qtr Number" saw_1,
            "Indicators"."L6 English Short Desc" saw_2,
            "Indicators"."L6 Arabic Short Desc" saw_3,
            "Observation Value"."Observation Value" * 10000000 saw_4
          FROM
            "EF - International Investment Position"
          WHERE
            ( "Indicators"."L6 Code" IN (
                'EfBoPINV0201',
                'EfBoPINV0202',
                'EfBoPINV0203'
            ) )
            AND (
                ( 'II_II' =:p_indicator )
                AND (
                    (
                        ( "Time"."Year" =:p_year )
                        OR (
                            :p_comp_year > 6
                            AND "Time"."Year" =:p_comp_year
                        )
                    )
                    OR (
                        (:p_comp_year < 6 )
                        AND ( "Time"."Year" <= year(current_date) )
                        AND (:p_comp_year >= ( year(current_date) - "Time"."Year" ) )
                    )
                )
            )
        )
        UNION
        ( SELECT
            "Time"."Year" saw_0,
            "Time"."Qtr Number" saw_1,
            "Indicators"."L8 English Short Desc" saw_2,
            "Indicators"."L8 Arabic Short Desc" saw_3,
            "Observation Value"."Observation Value" * 10000000 saw_4
          FROM
            "EF - Balance of Payment Statistics"
          WHERE
            ( "Indicators"."L8 Code" IN (
                'EfBoPINV01010101',
                'EfBoPINV01010102',
                'EfBoPINV010102',
                'EfBoPINV010103',
                'EfBoPINV0101'
            ) )
            AND (
                ( 'II_BOP_CA' =:p_indicator )
                AND (
                    (
                        ( "Time"."Year" =:p_year )
                        OR (
                            :p_comp_year > 6
                            AND "Time"."Year" =:p_comp_year
                        )
                    )
                    OR (
                        (:p_comp_year < 6 )
                        AND ( "Time"."Year" <= year(current_date) )
                        AND (:p_comp_year >= ( year(current_date) - "Time"."Year" ) )
                    )
                )
            )
        )
        UNION
        ( SELECT
            "Time"."Year" saw_0,
            "Time"."Qtr Number" saw_1,
            "Indicators"."L8 English Short Desc" saw_2,
            "Indicators"."L8 Arabic Short Desc" saw_3,
            "Observation Value"."Observation Value" * 10000000 saw_4
          FROM
            "EF - Balance of Payment Statistics"
          WHERE
            ( "Indicators"."L8 Code" IN (
                'EfBoPINV010301',
                'EfBoPINV010302',
                'EfBoPINV010303',
                'EfBoPINV010304',
                'EfBoPINV0103'
            ) )
            AND (
                ( 'II_BOP_FA' =:p_indicator )
                AND (
                    (
                        ( "Time"."Year" =:p_year )
                        OR (
                            :p_comp_year > 6
                            AND "Time"."Year" =:p_comp_year
                        )
                    )
                    OR (
                        (:p_comp_year < 6 )
                        AND ( "Time"."Year" <= year(current_date) )
                        AND (:p_comp_year >= ( year(current_date) - "Time"."Year" ) )
                    )
                )
            )
        )
        UNION
        ( SELECT
            "Time"."Year" saw_0,
            "Time"."Month Number" saw_1,
            "Indicators"."L6 English Short Desc" saw_2,
            "Indicators"."L6 Arabic Short Desc" saw_3,
            "Observation Value"."Observation Value" * 10000000 saw_4
          FROM
            "EF - Monetary Base"
          WHERE
            ( "Indicators"."L6 Code" IN (
                'EfMBSI0103',
                'EfMBSI010201',
                'EfMBSI0102',
                'EfMBSI0101',
                'EfMBSI01',
                'EfMBSI010202'
            ) )
            AND (
                ( 'MOF_MB' =:p_indicator )
                AND (
                    (
                        ( "Time"."Year" =:p_year )
                        OR (
                            :p_comp_year > 6
                            AND "Time"."Year" =:p_comp_year
                        )
                    )
                    OR (
                        (:p_comp_year < 6 )
                        AND ( "Time"."Year" <= year(current_date) )
                        AND (:p_comp_year >= ( year(current_date) - "Time"."Year" ) )
                    )
                )
            )
        )
        UNION
        ( SELECT
            "Time"."Year" saw_0,
            "Time"."Month Number" saw_1,
            "Indicators"."L6 English Short Desc" saw_2,
            "Indicators"."L6 Arabic Short Desc" saw_3,
            "Observation Value"."Observation Value" * 10000000 saw_4
          FROM
            "EF - Money Supply"
          WHERE
            ( "Indicators"."L6 Code" IN (
                'EfMBSI02010201',
                'EfMBSI020101',
                'EfMBSI0201020201',
                'EfMBSI0201020202'
            ) )
            AND (
                ( 'MOF_MS' =:p_indicator )
                AND (
                    (
                        ( "Time"."Year" =:p_year )
                        OR (
                            :p_comp_year > 6
                            AND "Time"."Year" =:p_comp_year
                        )
                    )
                    OR (
                        (:p_comp_year < 6 )
                        AND ( "Time"."Year" <= year(current_date) )
                        AND (:p_comp_year >= ( year(current_date) - "Time"."Year" ) )
                    )
                )
            )
        )
        UNION
        ( SELECT
            "Time"."Year" saw_0,
            "Time"."Qtr Number" saw_1,
            "Indicators"."L6 English Short Desc" saw_2,
            "Indicators"."L6 Arabic Short Desc" saw_3,
            "Observation Value"."Observation Value" saw_4
          FROM
            "EF - Financial Soundness"
          WHERE
            ( "Indicators"."L6 Code" IN (
                'EfMBSI0706',
                'EfMBSI0705',
                'EfMBSI0701'
            ) )
            AND (
                ( 'MOF_FS' =:p_indicator )
                AND (
                    (
                        ( "Time"."Year" =:p_year )
                        OR (
                            :p_comp_year > 6
                            AND "Time"."Year" =:p_comp_year
                        )
                    )
                    OR (
                        (:p_comp_year < 6 )
                        AND ( "Time"."Year" <= year(current_date) )
                        AND (:p_comp_year >= ( year(current_date) - "Time"."Year" ) )
                    )
                )
            )
        )
        UNION
        ( SELECT
            "Time"."Year" saw_0,
            "Time"."Month Number" saw_1,
            'Export Value' saw_2,
            'قيمة الصادرات' saw_3,
            "Exports Value"."Export Value" saw_4
          FROM
            "Foreign Trade - Exports - Monthly"
          WHERE
            (
                ( 'FT_EXP_EV' =:p_indicator )
                AND (
                    (
                        ( "Time"."Year" =:p_year )
                        OR (
                            :p_comp_year > 6
                            AND "Time"."Year" =:p_comp_year
                        )
                    )
                    OR (
                        (:p_comp_year < 6 )
                        AND ( "Time"."Year" <= year(current_date) )
                        AND (:p_comp_year >= ( year(current_date) - "Time"."Year" ) )
                    )
                )
            )
        )
        UNION
        ( SELECT
            "Time"."Year" saw_0,
            "Time"."Month Number" saw_1,
            'Import Value' saw_2,
            'قيمة الواردات' saw_3,
            "Imports Value"."Import Value" saw_4
          FROM
            "Foreign Trade - Imports - Monthly"
          WHERE
            (
                ( 'FT_IMP' =:p_indicator )
                AND (
                    (
                        ( "Time"."Year" =:p_year )
                        OR (
                            :p_comp_year > 6
                            AND "Time"."Year" =:p_comp_year
                        )
                    )
                    OR (
                        (:p_comp_year < 6 )
                        AND ( "Time"."Year" <= year(current_date) )
                        AND (:p_comp_year >= ( year(current_date) - "Time"."Year" ) )
                    )
                )
            )
        )
        UNION
        ( SELECT
            "Time"."Year" saw_0,
            '12' saw_1,
            'Export Value' saw_2,
            'قيمة الصادرات' saw_3,
            "Export Value"."Index for Export Value" saw_4
          FROM
            "Foreign Trade - Indices"
          WHERE
            (
                ( 'FT_INDEX_EAU' =:p_indicator )
                AND (
                    (
                        ( "Time"."Year" =:p_year )
                        OR (
                            :p_comp_year > 6
                            AND "Time"."Year" =:p_comp_year
                        )
                    )
                    OR (
                        (:p_comp_year < 6 )
                        AND ( "Time"."Year" <= year(current_date) )
                        AND (:p_comp_year >= ( year(current_date) - "Time"."Year" ) )
                    )
                )
            )
        )
        UNION
        ( SELECT
            "Time"."Year" saw_0,
            '12' saw_1,
            'Import Value' saw_2,
            'قيمة الواردات' saw_3,
            "Import Average Value"."Index for Import Average Value" saw_4
          FROM
            "Foreign Trade - Indices"
          WHERE
            (
                ( 'FT_INDEX_IAU' =:p_indicator )
                AND (
                    (
                        ( "Time"."Year" =:p_year )
                        OR (
                            :p_comp_year > 6
                            AND "Time"."Year" =:p_comp_year
                        )
                    )
                    OR (
                        (:p_comp_year < 6 )
                        AND ( "Time"."Year" <= year(current_date) )
                        AND (:p_comp_year >= ( year(current_date) - "Time"."Year" ) )
                    )
                )
            )
        )
        UNION
        ( SELECT
            "Time"."Year" saw_0,
            '12' saw_1,
            'Export Non-Oil Value' saw_2,
            'قيمة الصادرات' saw_3,
            "Exports Value"."Export Value - Non Oil" saw_4
          FROM
            "Foreign Trade - Exports - Yearly"
          WHERE
            (
                ( 'FT_EXP_MR' =:p_indicator )
                AND (
                    (
                        ( "Time"."Year" =:p_year )
                        OR (
                            :p_comp_year > 6
                            AND "Time"."Year" =:p_comp_year
                        )
                    )
                    OR (
                        (:p_comp_year < 6 )
                        AND ( "Time"."Year" <= year(current_date) )
                        AND (:p_comp_year >= ( year(current_date) - "Time"."Year" ) )
                    )
                )
            )
        )
        UNION
        ( SELECT
            "Time"."Year" saw_0,
            '12' saw_1,
            'Trade Balance Value' saw_2,
            'قيمة الواردات' saw_3,
            "Value of Trade Balance"."Value of trade balance" saw_4
          FROM
            "Foreign Trade - Trade Exchange"
          WHERE
            (
                ( 'FT_TE_BD' =:p_indicator )
                AND (
                    (
                        ( "Time"."Year" =:p_year )
                        OR (
                            :p_comp_year > 6
                            AND "Time"."Year" =:p_comp_year
                        )
                    )
                    OR (
                        (:p_comp_year < 6 )
                        AND ( "Time"."Year" <= year(current_date) )
                        AND (:p_comp_year >= ( year(current_date) - "Time"."Year" ) )
                    )
                )
            )
        )
        UNION
        ( SELECT
            "Time"."Year" saw_0,
            '12' saw_1,
            "Country"."Country English Short Desc" saw_2,
            "Country"."Country Arabic Short Desc" saw_3,
            "Value of Volume of Trade Exchange"."Rank countries- Value of of volume of trade exchange" saw_4
          FROM
            "Foreign Trade - Trade Exchange"
          WHERE
            (
                ( 'FT_TE_CR' =:p_indicator )
                AND (
                    (
                        ( "Time"."Year" =:p_year )
                        OR (
                            :p_comp_year > 6
                            AND "Time"."Year" =:p_comp_year
                        )
                    )
                    OR (
                        (:p_comp_year < 6 )
                        AND ( "Time"."Year" <= year(current_date) )
                        AND (:p_comp_year >= ( year(current_date) - "Time"."Year" ) )
                    )
                )
            )
        )
        UNION
        ( SELECT
            "Time"."Year" saw_0,
            '12' saw_1,
            'Trade Exchange Volume' saw_2,
            'قيمة الواردات' saw_3,
            "Value of Volume of Trade Exchange"."Value of volume of trade exchange" saw_4
          FROM
            "Foreign Trade - Trade Exchange"
          WHERE
            (
                ( 'FT_TE_VD' =:p_indicator )
                AND (
                    (
                        ( "Time"."Year" =:p_year )
                        OR (
                            :p_comp_year > 6
                            AND "Time"."Year" =:p_comp_year
                        )
                    )
                    OR (
                        (:p_comp_year < 6 )
                        AND ( "Time"."Year" <= year(current_date) )
                        AND (:p_comp_year >= ( year(current_date) - "Time"."Year" ) )
                    )
                )
            )
        )
        UNION
        ( SELECT
            "Time"."Year" saw_0,
            '12' saw_1,
            "Basket Wholesale"."Section English Short Desc" saw_2,
            "Basket Wholesale"."Section English Short Desc" saw_3,
            "Wholesale  prices"."General Index After 2011" saw_4
          FROM
            "Wholesale Prices"
          WHERE
            (
                ( 'PI_WT_PI' =:p_indicator )
                AND ( "Indicator"."L4 Code" = 'EfPi0301' )
                AND (
                    (
                        ( "Time"."Year" =:p_year )
                        OR (
                            :p_comp_year > 6
                            AND "Time"."Year" =:p_comp_year
                        )
                    )
                    OR (
                        (:p_comp_year < 6 )
                        AND ( "Time"."Year" <= year(current_date) )
                        AND (:p_comp_year >= ( year(current_date) - "Time"."Year" ) )
                    )
                )
            )
        )
        UNION
        ( SELECT
            "Time"."Year" saw_0,
            '12' saw_1,
            "Basket Real Estate"."Section English Short Desc" saw_2,
            "Basket Real Estate"."Section Arabic Short Desc" saw_3,
            "Property prices"."General Index" saw_4
          FROM
            "Real Estate Prices"
          WHERE
            (
                ( 'PI_RS' =:p_indicator )
                AND ( "Indicator"."L4 Code" = 'EfPi0401' )
                AND (
                    (
                        ( "Time"."Year" =:p_year )
                        OR (
                            :p_comp_year > 6
                            AND "Time"."Year" =:p_comp_year
                        )
                    )
                    OR (
                        (:p_comp_year < 6 )
                        AND ( "Time"."Year" <= year(current_date) )
                        AND (:p_comp_year >= ( year(current_date) - "Time"."Year" ) )
                    )
                )
            )
        )
    ) t1
ORDER BY
    saw_0,
    saw_1,
    saw_2,
    saw_3